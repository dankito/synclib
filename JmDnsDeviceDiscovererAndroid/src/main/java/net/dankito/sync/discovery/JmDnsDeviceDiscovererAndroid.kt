package net.dankito.sync.discovery

import android.content.Context
import android.net.wifi.WifiManager


open class JmDnsDeviceDiscovererAndroid(protected val context: Context) : JmDnsDeviceDiscoverer() {

    companion object {
        private const val MULTICAST_LOCK_NAME = "JmDnsDeviceDiscovererAndroid"
    }


    private var multicastLock: WifiManager.MulticastLock? = null


    override fun start(config: DeviceDiscovererConfig): Boolean {
        acquireWifiLock()

        return super.start(config)
    }

    override fun stop() {
        super.stop()

        releaseWifiLock()
    }


    /**
     * To improve battery life, processing of multicast packets is disabled by default on Android.
     * We can and must reenable this for the service discovery to work.
     * This is done programmatically by acquiring a lock in our activity.
     * (Explanation copied from http://home.heeere.com/tech-androidjmdns.html)
     */
    private fun acquireWifiLock() {
        val wifiManager = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        multicastLock = wifiManager.createMulticastLock(MULTICAST_LOCK_NAME)

        multicastLock?.let { multicastLock ->
            multicastLock.setReferenceCounted(true)
            multicastLock.acquire()
        }
    }

    private fun releaseWifiLock() {
        multicastLock?.let { multicastLock ->
            if(multicastLock.isHeld) {
                multicastLock.release()
            }
        }

        multicastLock = null
    }

}