package net.dankito.sync.test.java.files

import net.dankito.sync.test.files.FileSystemWatcherTestBase
import net.dankito.sync.test.files.IFileSystemWatcher


class FileSystemWatcherJavaTest : FileSystemWatcherTestBase() {

    override fun createFileSystemWatcher(): IFileSystemWatcher {
        return FileSystemWatcherJava()
    }

}
