package net.dankito.sync.test.java.window.main.cell

import javafx.scene.text.Font
import net.dankito.sync.test.java.window.main.viewmodel.LogItemItemViewModel
import net.dankito.sync.test.ui.model.LogItem
import tornadofx.*


class LogItemListCellFragment : ListCellFragment<LogItem>() {

    val logItem = LogItemItemViewModel().bindTo(this)


    override val root = vbox {
        cellProperty.addListener { _, _, newValue ->
            // so that the graphic always has cell's width
            newValue?.let { prefWidthProperty().bind(it.widthProperty().subtract(16)) }
        }

        vbox {
            label(logItem.timestamp) {
                font = Font.font(16.0)
            }

            label(logItem.message) {
                font = Font.font(15.0)

                isWrapText = true

                vboxConstraints {
                    marginTop = 6.0
                    marginBottom = 6.0
                }
            }
        }
    }

}