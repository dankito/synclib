package net.dankito.sync.test.java.window.files

import javafx.collections.ObservableList
import net.dankito.sync.test.java.window.files.service.FileChooserDialogConfig
import net.dankito.sync.test.java.window.files.service.InternalFileChooserDialogConfig
import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.ListDirectory


open class OpenFileDialog : SelectFileDialogBase() {

    protected lateinit var filesSelectedCallback: (List<FileInfo>) -> Unit


    open fun show(config: FileChooserDialogConfig = FileChooserDialogConfig(),
                  filesSelectedCallback: (List<FileInfo>) -> Unit) {

        this.filesSelectedCallback = filesSelectedCallback

        show(InternalFileChooserDialogConfig.from(config, ListDirectory.DirectoriesAndFiles))
    }


    override fun getResultingSelectedFiles(selectedItems: ObservableList<FileInfo>): List<FileInfo> {
        return selectedItems.filter{ it.isDirectory == false }
    }

    override fun selectingFilesDone(selectedFiles: List<FileInfo>) {
        filesSelectedCallback(selectedFiles)
    }

}