package net.dankito.sync.test.java.window.files.viewmodel

import javafx.beans.property.SimpleStringProperty
import net.dankito.utils.FormatUtils
import net.dankito.utils.io.FileInfo
import tornadofx.*


class FileInfoViewModel : ItemViewModel<FileInfo>() {

    companion object {
        private val formatUtils = FormatUtils()
    }


    val name = bind { SimpleStringProperty(if (item?.name?.isEmpty() == true) item?.absolutePath else item?.name) }

    val size = bind { SimpleStringProperty(item?.takeIf { it.isDirectory == false }?.let {
                formatUtils.formatFileSize(it.size)
    }) }

}