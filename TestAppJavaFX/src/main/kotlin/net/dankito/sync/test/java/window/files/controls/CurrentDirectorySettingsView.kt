package net.dankito.sync.test.java.window.files.controls

import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Pos
import javafx.scene.layout.Priority
import tornadofx.*


class CurrentDirectorySettingsView(private val currentDirectoryPath: SimpleStringProperty) : View() {


    override val root = hbox {
        prefHeight = 30.0

        alignment = Pos.CENTER_LEFT

        textfield(currentDirectoryPath) {
            isDisable = true
            opacity = 1.0


            hboxConstraints {
                hGrow = Priority.ALWAYS
            }
        }

    }

}