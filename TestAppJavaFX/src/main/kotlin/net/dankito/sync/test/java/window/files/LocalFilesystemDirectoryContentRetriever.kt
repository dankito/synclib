package net.dankito.sync.test.java.window.files

import net.dankito.sync.test.files.FolderShortcutsService
import net.dankito.sync.test.files.IFolderShortcutsService
import net.dankito.utils.extensions.toFileInfo
import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.FileUtils
import net.dankito.utils.io.ListDirectory


class LocalFilesystemDirectoryContentRetriever(private val fileUtils: FileUtils = FileUtils(),
                                               private val folderShortcutsService: IFolderShortcutsService = FolderShortcutsService()
) : IDirectoryContentRetriever {


    override fun getFilesOfDirectorySorted(directory: FileInfo, listDirectory: ListDirectory, folderDepth: Int, extensionsFilters: List<String>, callback: (List<FileInfo>?) -> Unit) {
        val filesInDirectory = fileUtils.getFilesOfDirectorySorted(directory.toFile(), listDirectory, folderDepth, extensionsFilters)

        callback(filesInDirectory?.map { it.toFileInfo() })
    }

    override fun getFolderShortcuts(callback: (List<FileInfo>) -> Unit) {
        callback(folderShortcutsService.getFolderShortcuts())
    }

}