package net.dankito.sync.test.java.window.files

import javafx.collections.ObservableList
import net.dankito.sync.test.java.window.files.service.FileChooserDialogConfig
import net.dankito.sync.test.java.window.files.service.InternalFileChooserDialogConfig
import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.ListDirectory


open class SelectFolderDialog : SelectFileDialogBase() {

    protected lateinit var folderSelectedCallback: (FileInfo) -> Unit


    open fun show(config: FileChooserDialogConfig = FileChooserDialogConfig(),
                  folderSelectedCallback: (FileInfo) -> Unit) {

        this.folderSelectedCallback = folderSelectedCallback

        show(InternalFileChooserDialogConfig.from(config, ListDirectory.DirectoriesOnly, true))
    }


    override fun getResultingSelectedFiles(selectedItems: ObservableList<FileInfo>): List<FileInfo> {
        return selectedItems.filter { it.isDirectory }
    }

    override fun selectingFilesDone(selectedFiles: List<FileInfo>) {
        selectedFiles.takeIf { it.isNotEmpty() }?.get(0)?.let { selectedFolder ->
            folderSelectedCallback(selectedFolder)
        }
    }

}