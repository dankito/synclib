package net.dankito.sync.test.java.window.files.cell

import javafx.geometry.Pos
import javafx.scene.effect.ColorAdjust
import javafx.scene.text.Font
import net.dankito.mime.MimeTypeCategorizer
import net.dankito.mime.MimeTypeDetector
import net.dankito.sync.test.java.window.files.service.FileChooserDialogConfig
import net.dankito.sync.test.java.window.files.service.PreviewImageService
import net.dankito.sync.test.java.window.files.viewmodel.FileInfoViewModel
import net.dankito.utils.io.FileInfo
import tornadofx.*


class FileInfoListCellFragment : ListCellFragment<FileInfo>() {

    companion object {
        private const val PreviewIconHeight = 48.0
        private const val PreviewIconWidth = 48.0
        private const val PreviewIconMarginLeftAndRight = 6.0


        private val iconTintColor = ColorAdjust(0.374 / 2, 0.914, 0.0, 0.0) // from white to orange

        private val previewImageService = PreviewImageService(MimeTypeDetector(), MimeTypeCategorizer())
    }


    val file = FileInfoViewModel().bindTo(this)


    override val root = vbox {
        cellProperty.addListener { _, _, newValue ->
            // so that the graphic always has cell's width
            newValue?.let { prefWidthProperty().bind(it.widthProperty().subtract(16)) }
        }

        borderpane {
            alignment = Pos.CENTER_LEFT

            center {
                hbox {
                    imageview {
                        alignment = Pos.CENTER_LEFT

                        prefHeight = PreviewIconHeight
                        prefWidth = PreviewIconWidth

                        fitWidth = PreviewIconWidth
                        isPreserveRatio = true

                        effect = iconTintColor

                        itemProperty.addListener { _, _, newValue ->
                            if (newValue != null) {
                                previewImageService.setPreviewImage(this, newValue, FileChooserDialogConfig())
                            }
                            else {
                                image = null
                            }
                        }

                        hboxConstraints {
                            marginLeft = PreviewIconMarginLeftAndRight
                            marginRight = PreviewIconMarginLeftAndRight
                        }
                    }

                    label(file.name) {
                        font = Font.font(18.0)
                    }
                }
            }

            right {

                label(file.size) {
                    font = Font.font(15.0)

                    borderpaneConstraints {
                        this.alignment = Pos.CENTER_RIGHT
                    }
                }
            }
        }
    }

}