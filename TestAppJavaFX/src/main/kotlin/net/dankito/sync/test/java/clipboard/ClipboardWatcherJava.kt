package net.dankito.sync.test.java.clipboard

import net.dankito.sync.test.clipboard.ClipboardContent
import net.dankito.sync.test.clipboard.ClipboardWatcherBase
import org.slf4j.LoggerFactory
import java.awt.Toolkit
import java.awt.datatransfer.*
import java.io.File
import java.util.*
import kotlin.concurrent.schedule


open class ClipboardWatcherJava : ClipboardWatcherBase(), ClipboardOwner {

    companion object {
        private val log = LoggerFactory.getLogger(ClipboardWatcherJava::class.java)
    }


    protected val retakeOwnershipTimer = Timer()

    protected val clip = Toolkit.getDefaultToolkit().systemClipboard

    protected val flavorListener = FlavorListener {
        try {
            clipboardContentChanged(mapToClipboardContent())
        } catch (e: Exception) {
            log.error("Could not get current content from clipboard", e)
        }
    }

    protected open fun mapToClipboardContent(): ClipboardContent {
        val plainText = if (clip.isDataFlavorAvailable(DataFlavor.stringFlavor))
                            clip.getData(DataFlavor.stringFlavor) as? String
                        else null

        val html = if (clip.isDataFlavorAvailable(DataFlavor.allHtmlFlavor))
                        clip.getData(DataFlavor.allHtmlFlavor) as? String
                    else null

        val file = if (clip.isDataFlavorAvailable(DataFlavor.javaFileListFlavor))
                        clip.getData(DataFlavor.javaFileListFlavor) as List<File>
                    else listOf()

        return ClipboardContent(plainText, html, file.map { it.absolutePath })
    }


    override fun startListeningToClipboard() {
        clip.addFlavorListener(flavorListener)
    }

    override fun stopListeningToClipboard() {
        clip.removeFlavorListener(flavorListener)
    }


    override fun lostOwnership(clipboard: Clipboard?, contents: Transferable?) {
        retakeOwnershipAfter(250L, contents)
    }

    protected open fun retakeOwnershipAfter(millis: Long, contents: Transferable?) {
        retakeOwnershipTimer.schedule(millis) {
            retakeOwnership(contents)
        }
    }

    /**
     * If another program changes the clipboard, clipboard ownership gets lost and we are no further informed of
     * changes. Therefore we have to re-take ownership which can be done by pasting the same content again to clipboard.
     *
     * Code taken from https://stackoverflow.com/a/5999515
     */
    protected open fun retakeOwnership(contents: Transferable?) {
        clip.removeFlavorListener(flavorListener) // remove flavorListener to not get notified of change in next line

        clip.setContents(contents, this) // set the some content again and re-take ownership

        clip.addFlavorListener(flavorListener)
    }

}