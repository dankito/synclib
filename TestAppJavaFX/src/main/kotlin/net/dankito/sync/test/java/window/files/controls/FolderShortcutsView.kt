package net.dankito.sync.test.java.window.files.controls

import javafx.collections.ObservableList
import javafx.scene.input.MouseButton
import net.dankito.sync.test.java.window.files.cell.FileInfoListCellFragment
import net.dankito.utils.io.FileInfo
import tornadofx.*


class FolderShortcutsView(favoriteFolders: ObservableList<FileInfo>,
                          val folderSelectedListener: (FileInfo) -> Unit) : View() {

    override val root = listview(favoriteFolders) {
        prefWidth = 250.0

        cellFragment(FileInfoListCellFragment::class)

        setOnMouseClicked { event ->
            if (event.button == MouseButton.PRIMARY && event.clickCount == 1) {
                this.selectedItem?.let { folderSelectedListener(it) }
            }
        }
    }

}