package net.dankito.sync.test.java

import net.dankito.jpa.entitymanager.IEntityManager
import net.dankito.sync.discovery.IDeviceDiscoverer
import net.dankito.sync.security.ISecurityProvider
import net.dankito.sync.test.SyncTestClient
import net.dankito.sync.test.di.CommonComponent
import net.dankito.sync.test.files.IFolderShortcutsService
import net.dankito.sync.test.java.di.AppComponent
import net.dankito.sync.test.java.di.DaggerAppComponent
import net.dankito.sync.test.java.di.JavaFxModule
import javax.inject.Inject


fun main(args: Array<String>) {
    val app = TestAppJavaCommandline()

    app.start()

    println("Press Enter to exit ...")
    readLine()

    app.stop()
    System.exit(0)
}


class TestAppJavaCommandline {

    @Inject
    lateinit var entityManager: IEntityManager

    @Inject
    lateinit var deviceDiscoverer: IDeviceDiscoverer

    @Inject
    lateinit var securityProvider: ISecurityProvider

    @Inject
    lateinit var folderShortcutsService: IFolderShortcutsService


    private val syncTestClient: SyncTestClient


    init {
        setupDI()

        val deviceName = "${System.getProperty("os.name")} ${System.getProperty("os.version")}"

        syncTestClient = SyncTestClient(deviceName, entityManager, securityProvider, deviceDiscoverer, folderShortcutsService)
    }

    private fun setupDI() {
        val component = DaggerAppComponent.builder()
                .javaFxModule(JavaFxModule())
                .build()

        CommonComponent.component = component
        AppComponent.component = component

        component.inject(this)
    }


    fun start() {
        syncTestClient.start()
    }

    fun stop() {
        syncTestClient.stop()
    }

}