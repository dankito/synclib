package net.dankito.sync.test.java.window.files.service

import net.dankito.sync.test.java.window.files.IDirectoryContentRetriever
import net.dankito.sync.test.java.window.files.LocalFilesystemDirectoryContentRetriever
import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.ListDirectory


class InternalFileChooserDialogConfig(extensionsFilters: List<String> = listOf(),
                                      initialDirectory: FileInfo? = null,
                                      dialogTitle: String? = null,
                                      selectSingleFile: Boolean = false,
                                      showFolderShortcutsView: Boolean = true,
                                      tryToLoadThumbnailForImageFiles: Boolean = true,
                                      tryToLoadThumbnailForVideoFiles: Boolean = true,
                                      tryToLoadAlbumArtForAudioFiles: Boolean = true,
                                      directoryContentRetriever: IDirectoryContentRetriever = LocalFilesystemDirectoryContentRetriever(),
                                      val listDirectory: ListDirectory = ListDirectory.DirectoriesAndFiles

) : FileChooserDialogConfig(extensionsFilters, initialDirectory, dialogTitle, selectSingleFile,
        showFolderShortcutsView, tryToLoadThumbnailForImageFiles, tryToLoadThumbnailForVideoFiles,
        tryToLoadAlbumArtForAudioFiles, directoryContentRetriever) {

    companion object {

        fun from(config: FileChooserDialogConfig, listDirectory: ListDirectory,
                 selectSingleFile: Boolean = config.selectSingleFile): InternalFileChooserDialogConfig {

            return InternalFileChooserDialogConfig(config.extensionsFilters, config.initialDirectory,
                    config.dialogTitle, selectSingleFile, config.showFolderShortcutsView,
                    config.tryToLoadThumbnailForImageFiles, config.tryToLoadThumbnailForVideoFiles,
                    config.tryToLoadAlbumArtForAudioFiles, config.directoryContentRetriever,
                    listDirectory)
        }

    }

}