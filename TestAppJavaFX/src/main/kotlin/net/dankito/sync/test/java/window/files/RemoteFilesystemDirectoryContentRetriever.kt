package net.dankito.sync.test.java.window.files

import net.dankito.sync.Endpoint
import net.dankito.sync.messenger.IMessenger
import net.dankito.sync.test.messenger.http.HttpMessageBuilder
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentMessage
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentResponse
import net.dankito.sync.test.messenger.http.messages.GetFolderShortcutsResponse
import net.dankito.utils.IThreadPool
import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.ListDirectory
import org.slf4j.LoggerFactory
import tornadofx.*


open class RemoteFilesystemDirectoryContentRetriever(protected val messenger: IMessenger,
                                                     protected val messageBuilder: HttpMessageBuilder,
                                                     protected val endpoint: Endpoint,
                                                     protected val threadPool: IThreadPool)
    : IDirectoryContentRetriever {

    companion object {
        private val log = LoggerFactory.getLogger(RemoteFilesystemDirectoryContentRetriever::class.java)
    }


    override fun getFilesOfDirectorySorted(directory: FileInfo, listDirectory: ListDirectory, folderDepth: Int,
                                           extensionsFilters: List<String>, callback: (List<FileInfo>?) -> Unit) {

        threadPool.runAsync { // this method gets called on UI thread, so get off UI thread for network call
            val adjustedDirectory =
                    if (directory.absolutePath == "/" + GetDirectoryContentMessage.RootFolder)
                        GetDirectoryContentMessage.RootFolder
                    else
                        directory.absolutePath

            val response = messenger.sendMessage(endpoint,
                    messageBuilder.getContentDirectoryMessage(adjustedDirectory, listDirectory),
                    GetDirectoryContentResponse::class.java)

            log.info("Response: error = ${response.error}, files = ${response.body?.files}")

            runLater {
                callback(response.body?.files ?: listOf())
            }

            // TODO: show error

        }
    }


    override fun getFolderShortcuts(callback: (List<FileInfo>) -> Unit) {

        threadPool.runAsync { // this method gets called on UI thread, so get off UI thread for network call
            val response = messenger.sendMessage(endpoint,
                    messageBuilder.getGetFolderShortcutsMessage(),
                    GetFolderShortcutsResponse::class.java)

            log.info("Response: error = ${response.error}, files = ${response.body?.files}")

            runLater {
                callback(response.body?.files ?: listOf())
            }

            // TODO: show error

        }
    }

}