package net.dankito.sync.test.java.di

import dagger.Module
import dagger.Provides
import net.dankito.jpa.entitymanager.EntityManagerConfiguration
import net.dankito.jpa.entitymanager.IEntityManager
import net.dankito.sync.data.CouchbaseLiteEntityManagerJava
import net.dankito.sync.discovery.IDeviceDiscoverer
import net.dankito.sync.discovery.UdpDeviceDiscoverer
import net.dankito.sync.security.ConscryptSecurityProviderJava
import net.dankito.sync.security.ISecurityProvider
import net.dankito.sync.test.files.FolderShortcutsService
import net.dankito.sync.test.files.IFolderShortcutsService
import net.dankito.utils.IThreadPool
import net.dankito.utils.network.INetworkConnectivityManager
import net.dankito.utils.network.INetworkHelper
import net.dankito.utils.network.PeriodicCheckingNetworkConnectivityManager
import javax.inject.Singleton


@Module
class JavaFxModule {

    @Provides
    @Singleton
    fun provideEntityManager(configuration: EntityManagerConfiguration) : IEntityManager {
        return CouchbaseLiteEntityManagerJava(configuration)
    }


    @Provides
    @Singleton
    fun provideNetworkConnectivityManager(networkHelper: INetworkHelper) : INetworkConnectivityManager {
        return PeriodicCheckingNetworkConnectivityManager(networkHelper)
    }

    @Provides
    @Singleton
    fun provideDeviceDiscoverer(connectivityManager: INetworkConnectivityManager, threadPool: IThreadPool) : IDeviceDiscoverer {
        return UdpDeviceDiscoverer(connectivityManager, threadPool)
    }


    @Provides
    @Singleton
    fun provideSecurityProvider() : ISecurityProvider {
        return ConscryptSecurityProviderJava()
    }


    @Provides
    @Singleton
    fun provideFolderShortcutsService() : IFolderShortcutsService {
        return FolderShortcutsService()
    }

}