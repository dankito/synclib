package net.dankito.sync.test.java.window.files.service

import javafx.scene.image.Image
import javafx.scene.image.ImageView
import net.dankito.mime.MimeTypeCategorizer
import net.dankito.mime.MimeTypeDetector
import net.dankito.utils.io.FileInfo
import java.io.File




class PreviewImageService(/*private val thumbnailService: ThumbnailService,*/
        private val mimeTypeDetector: MimeTypeDetector,
        private val mimeTypeCategorizer: MimeTypeCategorizer) {

    companion object {

        private val IconsFolderName = "icons"


        private val DefaultFolderIconUrl = getIconUrl("file_chooser_dialog_ic_folder_default.png")

        private val DefaultFileIconUrl = getIconUrl("file_chooser_dialog_ic_file_default.png")

        private val ImageFileIconUrl = getIconUrl("file_chooser_dialog_ic_file_image.png")

        private val MusicFileIconUrl = getIconUrl("file_chooser_dialog_ic_file_music.png")

        private val VideotFileIconUrl = getIconUrl("file_chooser_dialog_ic_file_video.png")

        private val PdfFileIconUrl = getIconUrl("file_chooser_dialog_ic_file_pdf.png")

        private val WordDocumentFileIconUrl = getIconUrl("file_chooser_dialog_ic_file_word.png")

        private val ExcelDocumentFileIconUrl = getIconUrl("file_chooser_dialog_ic_file_excel.png")

        private val PowerPointDocumentFileIconUrl = getIconUrl("file_chooser_dialog_ic_file_powerpoint.png")

        private val MarkupFileIconUrl = getIconUrl("file_chooser_dialog_ic_file_xml.png")

        private val DocumentFileIconUrl = getIconUrl("file_chooser_dialog_ic_file_document.png")


        private fun getIconUrl(iconName: String): String {
            return PreviewImageService::class.java.classLoader.
                    getResource(File(IconsFolderName, iconName).path).toExternalForm()
        }
    }

//    private val previewImageCache = PreviewImageCache()


    fun setPreviewImage(imageView: ImageView, file: FileInfo, config: FileChooserDialogConfig) {
//        val cachedPreviewImage = previewImageCache.getCachedPreviewImage(file) // TODO: cache image
//
//        if(cachedPreviewImage != null) {
//            imageView.setImageBitmap(cachedPreviewImage)
//        }
//        else {
//            imageView.setImageBitmap(null) // reset preview image (don't wait till preview image is calculated to show it, as otherwise it may show previous file's preview image

        setPreviewImageForFile(imageView, file, config)
//        }
    }

    private fun setPreviewImageForFile(imageView: ImageView, file: FileInfo, config: FileChooserDialogConfig) {
        val mimeType = mimeTypeDetector.getBestPickForFile(file.toFile())

        if(mimeType == null) {
            if(file.isDirectory) {
                setPreviewImageForFolder(imageView, file)
            }
            else { // fallback
                setPreviewImageToResource(imageView, DefaultFileIconUrl)
            }
        }
        else {
            setPreviewImageForFile(imageView, file, mimeType, config)
        }
    }

    private fun setPreviewImageForFolder(imageView: ImageView, folder: FileInfo) {
        setPreviewImageToResource(imageView, DefaultFolderIconUrl)
    }

    private fun setPreviewImageForFile(imageView: ImageView, file: FileInfo, mimeType: String, config: FileChooserDialogConfig) {
        setPreviewImageToResource(imageView, getIconForFile(mimeType)) // first set default file icon ...

        if(shouldTryToLoadThumbnailForFile(mimeType, config)) {
//            LoadThumbnailTask(viewHolder, imageView, file, mimeType, thumbnailService, previewImageCache).execute() // ... then check if may a thumbnail can be loaded
        }
    }

    private fun getIconForFile(mimeType: String): String {
        return when {
            mimeTypeCategorizer.isImageFile(mimeType) -> ImageFileIconUrl
            mimeTypeCategorizer.isAudioFile(mimeType) -> MusicFileIconUrl
            mimeTypeCategorizer.isVideoFile(mimeType) -> VideotFileIconUrl
            mimeTypeCategorizer.isPdfFile(mimeType) -> PdfFileIconUrl
            mimeTypeCategorizer.isMicrosoftWordFile(mimeType) || mimeTypeCategorizer.isOpenOfficeWriterFile(mimeType)
                -> WordDocumentFileIconUrl
            mimeTypeCategorizer.isMicrosoftExcelFile(mimeType) || mimeTypeCategorizer.isOpenOfficeCalcFile(mimeType)
                -> ExcelDocumentFileIconUrl
            mimeTypeCategorizer.isMicrosoftPowerPointFile(mimeType) || mimeTypeCategorizer.isOpenOfficeImpressFile(mimeType)
                -> PowerPointDocumentFileIconUrl
            mimeTypeCategorizer.isMarkUpFile(mimeType) -> MarkupFileIconUrl
            mimeTypeCategorizer.isDocument(mimeType) -> DocumentFileIconUrl
            else -> DefaultFileIconUrl
        }
    }

    private fun setPreviewImageToResource(imageView: ImageView, imageUrl: String) {
        imageView.image = Image(imageUrl)
    }

    private fun shouldTryToLoadThumbnailForFile(mimeType: String, config: FileChooserDialogConfig): Boolean {
        return (config.tryToLoadThumbnailForImageFiles && mimeTypeCategorizer.isImageFile(mimeType))
                || (config.tryToLoadThumbnailForVideoFiles && mimeTypeCategorizer.isVideoFile(mimeType))
                || (config.tryToLoadAlbumArtForAudioFiles && mimeTypeCategorizer.isAudioFile(mimeType))
    }

}