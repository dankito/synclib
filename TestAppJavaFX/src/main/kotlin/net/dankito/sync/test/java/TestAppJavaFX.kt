package net.dankito.sync.test.java

import javafx.application.Application
import javafx.stage.Stage
import net.dankito.sync.test.di.CommonComponent
import net.dankito.sync.test.java.di.AppComponent
import net.dankito.sync.test.java.di.JavaFxModule
import net.dankito.sync.test.java.window.main.MainWindow
import net.dankito.sync.test.java.di.DaggerAppComponent
import net.dankito.utils.localization.UTF8ResourceBundleControl
import org.slf4j.LoggerFactory
import tornadofx.*
import java.util.*


class TestAppJavaFX : App(MainWindow::class) {

    companion object {
        private val log = LoggerFactory.getLogger(TestAppJavaFX::class.java)
    }


    override fun start(stage: Stage) {
        try {
            setupMessagesResources() // has to be done before creating / injecting first instances as some of them already rely on Messages (e.g. CalculatedTags)

            setupDI()

            super.start(stage)
        } catch (e: Exception) {
            log.error("Could not start MainWindow", e)

            // TODO: show dialog with error message

            System.exit(1)
        }
    }


    private fun setupMessagesResources() {
        ResourceBundle.clearCache() // at this point default ResourceBundles are already created and cached. In order that ResourceBundle created below takes effect cache has to be clearedbefore
        FX.messages = ResourceBundle.getBundle("SyncTest", UTF8ResourceBundleControl())
    }


    private fun setupDI() {
        val component = DaggerAppComponent.builder()
                .javaFxModule(JavaFxModule())
                .build()

        CommonComponent.component = component
        AppComponent.component = component
    }


    @Throws(Exception::class)
    override fun stop() {
        super.stop()
        System.exit(0) // otherwise Window would be closed but application still running in background
    }

}



fun main(args: Array<String>) {
    Application.launch(TestAppJavaFX::class.java, *args)
}