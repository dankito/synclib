package net.dankito.sync.test.java.window.files

import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.ListDirectory


interface IDirectoryContentRetriever {

    fun getFilesOfDirectorySorted(directory: FileInfo, listDirectory: ListDirectory, folderDepth: Int,
                                  extensionsFilters: List<String>, callback: (List<FileInfo>?) -> Unit)

    fun getFolderShortcuts(callback: (List<FileInfo>) -> Unit)

}