package net.dankito.sync.test.java.files

import net.dankito.sync.test.files.AbstractFileSystemWatcher
import net.dankito.sync.test.files.FileChange
import net.dankito.sync.test.files.FileChangeInfo
import net.dankito.utils.io.FileInfo
import org.slf4j.LoggerFactory
import java.io.File
import java.nio.file.Path
import java.nio.file.StandardWatchEventKinds
import java.nio.file.WatchEvent
import java.util.concurrent.ConcurrentHashMap


open class FileSystemWatcherJava : AbstractFileSystemWatcher() {

    companion object {
        private val log = LoggerFactory.getLogger(FileSystemWatcherJava::class.java)
    }


    protected val watchedFolders = ConcurrentHashMap<File, WatchDir>()


    override fun doWatchFolderRecursively(folder: File, changeListener: (FileChangeInfo) -> Unit) {
        val watcher = object : WatchDir(folder.toPath(), true) {
            override fun handleEvent(child: Path, event: WatchEvent<*>) {
                super.handleEvent(child, event)

                fileChanged(folder, child, event, changeListener)
            }
        }

        watchedFolders.put(folder, watcher)

        watcher.processEvents()
    }

    protected open fun fileChanged(folder: File, child: Path, event: WatchEvent<*>,
                                   changeListener: (FileChangeInfo) -> Unit) {

        mapChange(event)?.let { change ->
            val file = child.toFile()
            log.info("$change $file")
            changeListener(FileChangeInfo(FileInfo(file.name, file.absolutePath, file.isDirectory, file.length()),
                    file.toRelativeString(folder), change))
        }
    }

    protected open fun mapChange(event: WatchEvent<*>): FileChange? {
        when (event.kind()) {
            StandardWatchEventKinds.ENTRY_CREATE -> return FileChange.Created
            StandardWatchEventKinds.ENTRY_MODIFY -> return FileChange.Modified
            StandardWatchEventKinds.ENTRY_DELETE -> return FileChange.Deleted
            else -> return null
        }
    }

    override fun stopWatching(folder: File) {
        watchedFolders.remove(folder)?.let { watcher ->
            watcher.stopWatching()
        }
    }

    override fun stopAllWatchers() {
        ArrayList(watchedFolders.keys).forEach { file -> stopWatching(file)}
    }

}