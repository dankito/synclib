package net.dankito.sync.test.java.window.main.viewmodel

import javafx.beans.property.SimpleStringProperty
import net.dankito.sync.test.messenger.http.messages.DeviceInfo
import tornadofx.*


class DeviceInfoViewModel : ItemViewModel<DeviceInfo>() {

    val deviceName = bind { SimpleStringProperty(item?.deviceName) }

    val deviceAddress = bind { SimpleStringProperty("${item?.address} (Port ${item?.messagesPort})") }

}