package net.dankito.sync.test.java.window.main

import javafx.collections.FXCollections
import net.dankito.jpa.entitymanager.IEntityManager
import net.dankito.sync.discovery.IDeviceDiscoverer
import net.dankito.sync.security.ISecurityProvider
import net.dankito.sync.test.files.IFolderShortcutsService
import net.dankito.sync.test.java.clipboard.ClipboardWatcherJava
import net.dankito.sync.test.java.di.AppComponent
import net.dankito.sync.test.java.window.main.cell.DeviceInfoListCellFragment
import net.dankito.sync.test.java.window.main.cell.LogItemListCellFragment
import net.dankito.sync.test.messenger.http.messages.DeviceInfo
import net.dankito.sync.test.ui.model.LogItem
import net.dankito.sync.test.ui.presenter.MainWindowPresenter
import net.dankito.utils.IThreadPool
import net.dankito.utils.events.IEventBus
import org.slf4j.LoggerFactory
import tornadofx.*
import javax.inject.Inject


class MainWindow : View(FX.messages["application.title"]) {

    companion object {
        private val logger = LoggerFactory.getLogger(MainWindow::class.java)
    }


    @Inject
    lateinit var entityManager: IEntityManager

    @Inject
    lateinit var deviceDiscoverer: IDeviceDiscoverer

    @Inject
    lateinit var securityProvider: ISecurityProvider

    @Inject
    lateinit var folderShortcutsService: IFolderShortcutsService

    @Inject
    lateinit var eventBus: IEventBus

    @Inject
    lateinit var threadPool: IThreadPool


    private val presenter: MainWindowPresenter


    private val discoveredDevices = FXCollections.observableArrayList<DeviceInfo>()

    private val logItems = FXCollections.observableArrayList<LogItem>()


    init {
        AppComponent.component.inject(this)

        presenter = MainWindowPresenter(eventBus, threadPool) { addLogMessage(it) }

        setupLogic()
    }

    private fun setupLogic() {
        presenter.start("${System.getProperty("os.name")} ${System.getProperty("os.version")}",
                entityManager, deviceDiscoverer, securityProvider, folderShortcutsService)

        presenter.syncTestClient.addDeviceDiscoveredListener { handleDeviceDiscovered(it) }
        presenter.syncTestClient.addDeviceDisconnectedListener { handleDeviceDisconnected(it) }

        // TODO: use implementation from JavaFxUtils
        ClipboardWatcherJava().addClipboardContentChangedListener {
            log.info("Clipboard content changed to $it")
            entityManager.persistEntity(it)
        }
    }


    override val root = vbox {
        prefWidth = 800.0
        prefHeight = 450.0


        label(FX.messages["discovered.devices.label"]) {
            vboxConstraints {
                marginTop = 4.0
            }
        }

        listview(discoveredDevices) {
            userData = presenter // bad code design, but found no other way to pass MainWindowPresenter to DeviceInfoListCellFragment

            cellFragment(DeviceInfoListCellFragment::class)
        }

        label(FX.messages["log.items.label"]) {
            vboxConstraints {
                marginTop = 24.0
                marginBottom = 4.0
            }
        }

        listview(logItems) {
            cellFragment(LogItemListCellFragment::class)
        }

    }


    private fun handleDeviceDiscovered(deviceInfo: DeviceInfo) {
        runLater {
            discoveredDevices.add(deviceInfo)

            addLogMessageOnUiThread(LogItem("Device discovered: $deviceInfo"))
        }
    }

    private fun handleDeviceDisconnected(deviceInfo: DeviceInfo) {
        runLater {
            discoveredDevices.remove(deviceInfo)

            addLogMessageOnUiThread(LogItem("Device disconnected: $deviceInfo"))
        }
    }

    private fun addLogMessage(message: LogItem) {
        runLater {
            addLogMessageOnUiThread(message)
        }
    }

    private fun addLogMessageOnUiThread(message: LogItem) {
        logger.info(message.logMessage)

        logItems.add(message)
    }

}