package net.dankito.sync.test.java.window.main.cell

import javafx.scene.text.Font
import javafx.stage.DirectoryChooser
import net.dankito.sync.test.java.window.files.IDirectoryContentRetriever
import net.dankito.sync.test.java.window.files.OpenFileDialog
import net.dankito.sync.test.java.window.files.RemoteFilesystemDirectoryContentRetriever
import net.dankito.sync.test.java.window.files.SelectFolderDialog
import net.dankito.sync.test.java.window.files.service.FileChooserDialogConfig
import net.dankito.sync.test.java.window.main.viewmodel.DeviceInfoViewModel
import net.dankito.sync.test.messenger.http.messages.DeviceInfo
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentMessage
import net.dankito.sync.test.ui.presenter.MainWindowPresenter
import tornadofx.*
import java.io.File


class DeviceInfoListCellFragment : ListCellFragment<DeviceInfo>() {

    companion object {
        private const val ButtonsHeight = 34.0
        private const val ButtonsWidth = 175.0
    }


    val device = DeviceInfoViewModel().bindTo(this)

    private var presenter: MainWindowPresenter? = null


    override val root = vbox {
        cellProperty.addListener { _, _, newValue ->
            // so that the graphic always has cell's width
            newValue?.let { prefWidthProperty().bind(it.widthProperty().subtract(16)) }

            this@DeviceInfoListCellFragment.presenter = newValue?.listView?.userData as? MainWindowPresenter
        }

        vbox {
            label(device.deviceName) {
                font = Font.font(18.0)
            }

            label(device.deviceAddress) {
                font = Font.font(15.0)

                vboxConstraints {
                    marginTop = 6.0
                    marginBottom = 6.0
                }
            }

            hbox {
                button(FX.messages["download.files"]) {
                    minHeight = ButtonsHeight
                    maxHeight = minHeight

                    minWidth = ButtonsWidth
                    maxWidth = minWidth

                    action { downloadFiles(device.item) }
                }

                button(FX.messages["download.folder"]) {
                    minHeight = ButtonsHeight
                    maxHeight = minHeight

                    minWidth = ButtonsWidth
                    maxWidth = minWidth

                    hboxConstraints {
                        marginLeftRight(12.0)
                    }

                    action { downloadFolder(device.item) }
                }

                button(FX.messages["transfer.speed.test"]) {
                    minHeight = ButtonsHeight
                    maxHeight = minHeight

                    minWidth = ButtonsWidth
                    maxWidth = minWidth

                    action { presenter?.doTransferSpeedTest(device.item) }
                }
            }
        }
    }


    private fun downloadFiles(device: DeviceInfo) {
        presenter?.let { presenter ->
            val directoryContentRetriever = createRemoteFilesystemDirectoryContentRetriever(device, presenter)
            val config = FileChooserDialogConfig(listOf(), GetDirectoryContentMessage.RootFolderFileInfo,
                    "Select remote files to download", false, true, false, false, false,
                    directoryContentRetriever)

            find(OpenFileDialog::class).show(config) { selectedFiles ->

                val downloadDirectoryChooser = createDownloadDirectoryChooser()

                downloadDirectoryChooser.showDialog(this.primaryStage)?.let { downloadDirectory ->
                    presenter.downloadSelectedFilesAsync(device, downloadDirectory, selectedFiles)
                }
            }
        }
    }

    private fun createRemoteFilesystemDirectoryContentRetriever(device: DeviceInfo, presenter: MainWindowPresenter)
            : IDirectoryContentRetriever {

        val client = presenter.syncTestClient

        return RemoteFilesystemDirectoryContentRetriever(client.messenger, client.messageBuilder, device.endpoint,
                presenter.threadPool)
    }

    private fun createDownloadDirectoryChooser(): DirectoryChooser {
        val downloadDirectoryChooser = DirectoryChooser()

        val initialDownloadDirectory = File(System.getProperty("user.dir"), "downloads")
        initialDownloadDirectory.mkdirs()
        downloadDirectoryChooser.initialDirectory = initialDownloadDirectory
        downloadDirectoryChooser.title = "Select destination folder"

        return downloadDirectoryChooser
    }

    private fun downloadFolder(device: DeviceInfo) {
        presenter?.let { presenter ->
            val directoryContentRetriever = createRemoteFilesystemDirectoryContentRetriever(device, presenter)
            val config = FileChooserDialogConfig(listOf(), GetDirectoryContentMessage.RootFolderFileInfo,
                    "Select remote folder to download", true, true, false, false, false,
                    directoryContentRetriever)

            find(SelectFolderDialog::class).show(config) { remoteFolder ->

                val localFolderDialog = DirectoryChooser()
                localFolderDialog.title = "Select local destination folder"
                localFolderDialog.showDialog(null)?.let { localFolder ->

                    presenter.downloadFolderAsync(device, remoteFolder.toFile(), localFolder)
                }
            }
        }
    }

}