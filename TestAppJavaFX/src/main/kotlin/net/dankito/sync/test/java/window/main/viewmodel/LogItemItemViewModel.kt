package net.dankito.sync.test.java.window.main.viewmodel

import javafx.beans.property.SimpleStringProperty
import net.dankito.sync.test.ui.model.LogItem
import net.dankito.sync.test.ui.presenter.MainWindowPresenter
import tornadofx.*


class LogItemItemViewModel : ItemViewModel<LogItem>() {

    val message = bind { SimpleStringProperty(item?.logMessage) }

    val timestamp = bind { SimpleStringProperty(item?.let { MainWindowPresenter.LogItemTimestampFormat.format(it.timestamp) }) }

}