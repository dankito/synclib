package net.dankito.sync.test.java.di

import dagger.Component
import net.dankito.sync.test.di.CommonComponent
import net.dankito.sync.test.di.CommonUtilsModule
import net.dankito.sync.test.di.DaoModule
import net.dankito.sync.test.di.NetworkModule
import net.dankito.sync.test.java.TestAppJavaCommandline
import net.dankito.sync.test.java.window.main.MainWindow
import javax.inject.Singleton


@Singleton
@Component(modules = [
    JavaFxModule::class,
    CommonUtilsModule::class, NetworkModule::class, DaoModule::class
])
interface AppComponent : CommonComponent {

    companion object {
        lateinit var component: AppComponent
    }


    fun inject(mainWindow: MainWindow)

    fun inject(commandlineApp: TestAppJavaCommandline)

}