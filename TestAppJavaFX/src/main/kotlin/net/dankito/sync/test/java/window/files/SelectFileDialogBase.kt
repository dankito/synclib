package net.dankito.sync.test.java.window.files

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.ListChangeListener
import javafx.collections.ObservableList
import javafx.scene.control.ListView
import javafx.scene.control.SelectionMode
import javafx.scene.layout.Priority
import net.dankito.sync.test.java.window.DialogFragment
import net.dankito.sync.test.java.window.files.cell.FileInfoListCellFragment
import net.dankito.sync.test.java.window.files.controls.CurrentDirectorySettingsView
import net.dankito.sync.test.java.window.files.controls.FolderShortcutsView
import net.dankito.sync.test.java.window.files.service.InternalFileChooserDialogConfig
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentMessage
import net.dankito.utils.extensions.toFileInfo
import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.FileUtils
import net.dankito.utils.javafx.ui.extensions.ensureOnlyUsesSpaceIfVisible
import tornadofx.*


abstract class SelectFileDialogBase : DialogFragment() {

    companion object {
        val ParentDirectoryFileInfo = FileInfo("..", "", true, 0)

        private val DialogButtonsHeight = 36.0
        private val DialogButtonsWidth = 150.0
    }


    abstract fun getResultingSelectedFiles(selectedItems: ObservableList<FileInfo>): List<FileInfo>

    abstract fun selectingFilesDone(selectedFiles: List<FileInfo>)


    protected val fileUtils = FileUtils()

    protected var internalConfig: InternalFileChooserDialogConfig = InternalFileChooserDialogConfig()

    protected val folderShortcuts = FXCollections.observableArrayList<FileInfo>()

    protected val showFolderShortcutsView = SimpleBooleanProperty(internalConfig.showFolderShortcutsView)

    protected var currentDirectoryProperty: FileInfo = ParentDirectoryFileInfo

    protected val currentDirectoryPath = SimpleStringProperty()

    protected val currentDirectoryContent = FXCollections.observableArrayList<FileInfo>()

    protected val selectedFiles = FXCollections.observableArrayList<FileInfo>()


    protected lateinit var lstvwCurrentDirectoryContent: ListView<FileInfo>


    override val root = hbox {

        prefHeight = 500.0
        prefWidth = 750.0

        add(FolderShortcutsView(folderShortcuts) { setCurrentDirectory(it) }.apply {
            this.root.ensureOnlyUsesSpaceIfVisible()
            this.root.visibleProperty().bind(showFolderShortcutsView)
        })


        borderpane {
            hboxConstraints {
                hGrow = Priority.ALWAYS
            }

            top {
                add(CurrentDirectorySettingsView(currentDirectoryPath))
            }

            center {
                lstvwCurrentDirectoryContent = listview(currentDirectoryContent) {
                    cellFragment(FileInfoListCellFragment::class)

                    selectionModel.selectedItems.addListener(ListChangeListener<FileInfo> {
                        selectedFilesChanged()
                    })

                    onDoubleClick { this.selectedItem?.let { fileDoubleClicked(it) } }
                }
            }

            bottom {
                borderpaneConstraints {
                    this.marginTop = 12.0
                }

                anchorpane {

                    button(FX.messages["ok"]) {
                        isDisable = true

                        selectedFiles.addListener(ListChangeListener { change ->
                            isDisable = selectedFiles.isEmpty()
                        })

                        minHeight = DialogButtonsHeight
                        maxHeight = minHeight

                        minWidth = DialogButtonsWidth
                        maxWidth = minWidth

                        anchorpaneConstraints {
                            topAnchor = 4.0
                            rightAnchor = 4.0
                            bottomAnchor = 4.0
                        }

                        action { fileSelectionDone() }
                    }

                    button(FX.messages["cancel"]) {
                        minHeight = DialogButtonsHeight
                        maxHeight = minHeight

                        minWidth = DialogButtonsWidth
                        maxWidth = minWidth

                        anchorpaneConstraints {
                            topAnchor = 4.0
                            rightAnchor = DialogButtonsWidth + 24.0
                            bottomAnchor = 4.0
                        }

                        action { closeDialog() }
                    }
                }
            }
        }
    }


    protected open fun show(config: InternalFileChooserDialogConfig) {

        this.internalConfig = config


        show(config.dialogTitle)


        lstvwCurrentDirectoryContent.selectionModel.selectionMode =
                if (config.selectSingleFile) SelectionMode.SINGLE else SelectionMode.MULTIPLE

        showFolderShortcutsView.value = internalConfig.showFolderShortcutsView

        val initialDirectory = config.initialDirectory ?: fileUtils.getOsRootFolder().toFileInfo()
        setCurrentDirectory(initialDirectory)

        internalConfig.directoryContentRetriever.getFolderShortcuts { retrievedShortcuts ->
            runLater {
                folderShortcuts.setAll(retrievedShortcuts)

                showFolderShortcutsView.value = internalConfig.showFolderShortcutsView and retrievedShortcuts.isNotEmpty()
            }
        }
    }

    open fun setCurrentDirectory(directory: FileInfo) {
        currentDirectoryProperty = directory
        currentDirectoryPath.value = if (currentDirectoryProperty == GetDirectoryContentMessage.RootFolderFileInfo) "/"
                                    else currentDirectoryProperty.absolutePath

        internalConfig.directoryContentRetriever.getFilesOfDirectorySorted(directory, internalConfig.listDirectory, 1,
                internalConfig.extensionsFilters) { files ->

            files?.let {
                showDirectoryContent(files, directory)
            }

        }
    }

    protected open fun showDirectoryContent(files: List<FileInfo>?, directory: FileInfo) {
        val filesToDisplay = ArrayList(files)
        if (directory.parent != null) {
            filesToDisplay.add(0, ParentDirectoryFileInfo)
        }

        runLater {
            selectedFiles.clear()
            lstvwCurrentDirectoryContent.selectionModel.clearSelection()

            currentDirectoryContent.setAll(filesToDisplay)

            lstvwCurrentDirectoryContent.scrollTo(0)
        }
    }

    protected open fun fileDoubleClicked(file: FileInfo) {
        if (file.isDirectory) {
            if (file == ParentDirectoryFileInfo) {
                currentDirectoryProperty.parent?.let { setCurrentDirectory(it) }
            }
            else {
                setCurrentDirectory(file)
            }
        }
    }


    protected open fun selectedFilesChanged() {
        selectedFiles.setAll(getResultingSelectedFiles(lstvwCurrentDirectoryContent.selectionModel.selectedItems))
    }


    protected open fun fileSelectionDone() {
        selectingFilesDone(selectedFiles.toList())

        closeDialog()
    }

    protected open fun closeDialog() {
        close()
    }

}