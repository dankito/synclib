package net.dankito.sync.test.java.window.files.service

import net.dankito.sync.test.java.window.files.IDirectoryContentRetriever
import net.dankito.sync.test.java.window.files.LocalFilesystemDirectoryContentRetriever
import net.dankito.utils.io.FileInfo


open class FileChooserDialogConfig(
        val extensionsFilters: List<String> = listOf(),
        val initialDirectory: FileInfo? = null,
        val dialogTitle: String? = null,
        val selectSingleFile: Boolean = false,
        val showFolderShortcutsView: Boolean = true,
        val tryToLoadThumbnailForImageFiles: Boolean = true,
        val tryToLoadThumbnailForVideoFiles: Boolean = true,
        val tryToLoadAlbumArtForAudioFiles: Boolean = true,
        val directoryContentRetriever: IDirectoryContentRetriever = LocalFilesystemDirectoryContentRetriever()
)