package net.dankito.sync.discovery

import net.dankito.sync.discovery.IDeviceDiscoverer.Companion.DefaultServiceType
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.slf4j.LoggerFactory
import java.time.Duration
import java.time.Instant
import java.util.concurrent.TimeUnit

class JmDnsDeviceDiscovererTest {

    companion object {
        private val ServiceName = "SyncTest"

        private val ServicePort = 1234

        private val ServiceDescription = "Your Description could be placed here"

        private val log = LoggerFactory.getLogger(JmDnsDeviceDiscovererTest::class.java)
    }


    private val first = JmDnsDeviceDiscoverer()

    private val second = JmDnsDeviceDiscoverer()


    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
        first.stop()
        second.stop()
    }


    @Test
    fun discoverDevice() {
        val devicesDiscoveredByFirst = mutableListOf<DiscoveredDevice>()
        val devicesDiscoveredBySecond = mutableListOf<DiscoveredDevice>()
        val start = Instant.now()

        first.addDeviceDiscoveredListener {
            devicesDiscoveredByFirst.add(it)
            log.info("First discovered device $it after ${Duration.between(start, Instant.now()).toMillis()} millis")
        }

        second.addDeviceDiscoveredListener {
            devicesDiscoveredBySecond.add(it)
            log.info("Second discovered device $it after ${Duration.between(start, Instant.now()).toMillis()} millis")
        }

        first.start(DeviceDiscovererConfig(DefaultServiceType, ServiceName, ServicePort, ServiceDescription))
        second.start(DeviceDiscovererConfig(DefaultServiceType, ServiceName, ServicePort, ServiceDescription))

        TimeUnit.SECONDS.sleep(10)

        assertThat(devicesDiscoveredByFirst).hasSize(1)
        assertThat(devicesDiscoveredBySecond).hasSize(1)
    }

    @Test
    fun discover4Devices() {
        val discoverers = mutableListOf<JmDnsDeviceDiscoverer>()
        val discoveredDevices = mutableMapOf<Int, MutableList<DiscoveredDevice>>()
        var start = Instant.now() // to be overwritten below but initialized here

        for (i in 0..5) {
            val discoverer = JmDnsDeviceDiscoverer()
            discoverers.add(discoverer)
            discoveredDevices.put(i, mutableListOf())

            discoverer.addDeviceDiscoveredListener {
                discoveredDevices[i]?.add(it)
                log.info("[$i] discovered device $it after ${Duration.between(start, Instant.now()).toMillis()} millis")
            }
        }

        start = Instant.now()

        discoverers.forEachIndexed { index, discoverer ->
            discoverer.start(DeviceDiscovererConfig(DefaultServiceType, ServiceName + index, ServicePort, ServiceDescription))
        }

        TimeUnit.SECONDS.sleep(10)

        discoverers.forEach { it.stop() }

        // TODO: will currently fail as own devices and multiple times discovered other devices aren't filtered out
        discoveredDevices.values.forEachIndexed { index, devicesDiscovered ->
            log.info("Discoverer [$index] has discovered ${devicesDiscovered.size} devices")
            assertThat(devicesDiscovered).hasSize(4)
        }
    }

    @Test
    fun discoverDeviceDisconnected() {
        val disconnectedDevicesByFirst = mutableListOf<DiscoveredDevice>()
        val disconnectedDevicesBySecond = mutableListOf<DiscoveredDevice>()
        var start = Instant.now() // to be overwritten below but initialized here

        first.addDeviceDisconnectedListener {
            disconnectedDevicesByFirst.add(it)
            log.info("First discovered disconnected device $it after ${Duration.between(start, Instant.now()).toMillis()} millis")
        }

        second.addDeviceDisconnectedListener {
            disconnectedDevicesBySecond.add(it)
            log.info("Second discovered disconnected device $it after ${Duration.between(start, Instant.now()).toMillis()} millis")
        }

        first.start(DeviceDiscovererConfig(DefaultServiceType, ServiceName + "1", ServicePort, ServiceDescription))
        second.start(DeviceDiscovererConfig(DefaultServiceType, ServiceName + "2", ServicePort, ServiceDescription))

        TimeUnit.SECONDS.sleep(10)

        start = Instant.now()
        second.stop()

        TimeUnit.SECONDS.sleep(10)

        assertThat(disconnectedDevicesByFirst).hasSize(1)
        assertThat(disconnectedDevicesBySecond).hasSize(0)
    }

}