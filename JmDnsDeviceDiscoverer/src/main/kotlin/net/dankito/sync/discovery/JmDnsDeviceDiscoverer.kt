package net.dankito.sync.discovery

import org.slf4j.LoggerFactory
import java.net.InetAddress
import javax.jmdns.JmDNS
import javax.jmdns.ServiceEvent
import javax.jmdns.ServiceInfo
import javax.jmdns.ServiceListener


/**
 * Example code taken from: https://github.com/jmdns/jmdns
 */
open class JmDnsDeviceDiscoverer : DeviceDiscovererBase() {

    companion object {
        private val log = LoggerFactory.getLogger(JmDnsDeviceDiscoverer::class.java)
    }


    protected var jmDns: JmDNS? = null


    override fun start(config: DeviceDiscovererConfig): Boolean {
        if (jmDns != null) {
            return false
        }

        try {
            // Create a JmDNS instance
            jmDns = JmDNS.create(InetAddress.getLocalHost())

            jmDns?.addServiceListener(config.serviceType, serviceListener)

            // Register a service
            val serviceInfo = ServiceInfo.create(config.serviceType, config.serviceName, config.servicePortToPromote, config.serviceDescription)
            jmDns?.registerService(serviceInfo)

            return true
        } catch (e: Exception) {
            log.error("Could not register Service", e)
        }

        return false
    }

    override fun stop() {
        // Unregister all services
        jmDns?.unregisterAllServices()

        jmDns?.close()

        jmDns = null
    }


    protected val serviceListener = object : ServiceListener {

        override fun serviceAdded(event: ServiceEvent) {
            // at this point device's IP address isn't resolved yet
            log.info("Service added: $event")
        }

        override fun serviceResolved(event: ServiceEvent) {
            log.info("Service resolved: $event")

            if (event.info.hostAddresses.isNotEmpty()) {
                // TODO: it's important to filter out self and devices for which multiple resolved events are fired
                callDeviceDiscoveredListeners(extractDiscoveredFromServiceEvent(event))
            }
        }

        override fun serviceRemoved(event: ServiceEvent) {
            log.info("Service removed: $event")

            if (event.info.hostAddresses.isNotEmpty()) {
                callDeviceDisconnectedListeners(extractDiscoveredFromServiceEvent(event))
            }
        }

    }

    protected open fun extractDiscoveredFromServiceEvent(event: ServiceEvent): DiscoveredDevice {
        return DiscoveredDevice(event.info.hostAddresses[0], event.info.port, event.name)
    }

}