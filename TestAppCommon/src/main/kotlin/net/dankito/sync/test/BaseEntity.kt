package net.dankito.sync.test

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import java.io.Serializable
import java.util.*
import javax.persistence.*


@MappedSuperclass
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator::class,
        property = "id")
abstract class BaseEntity : Serializable {

    companion object {
        val UnsetDate = Date(0)
    }


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    var id: String? = null

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_on", updatable = false)
    var createdOn: Date = UnsetDate
        protected set

    @Column(name = "modified_on")
    @Temporal(TemporalType.TIMESTAMP)
    var modifiedOn: Date = UnsetDate
        protected set

    @Version
    @Column(name = "version", nullable = false, columnDefinition = "BIGINT DEFAULT 1")
    var version: Long? = null
        protected set

    @Column(name = "deleted", columnDefinition = "SMALLINT DEFAULT 0", nullable = false)
    var deleted = false
        protected set


    @Transient
    fun isPersisted(): Boolean {
        return createdOn != UnsetDate
    }


    @PrePersist
    protected open fun prePersist() {
        createdOn = Date()
        modifiedOn = createdOn
        version = 1L
    }

    @PreUpdate
    protected open fun preUpdate() {
        modifiedOn = Date()
    }

    @PreRemove
    protected open fun preRemove() {
        modifiedOn = Date()
        deleted = true
    }


}