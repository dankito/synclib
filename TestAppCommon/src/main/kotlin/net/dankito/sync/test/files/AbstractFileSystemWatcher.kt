package net.dankito.sync.test.files

import java.io.File
import java.util.concurrent.CountDownLatch
import kotlin.concurrent.thread


abstract class AbstractFileSystemWatcher : IFileSystemWatcher {

    protected abstract fun doWatchFolderRecursively(folder: File, changeListener: (FileChangeInfo) -> Unit)

    override fun watchFolderRecursively(folder: File, changeListener: (FileChangeInfo) -> Unit) {
        val waitTillWatcherThreadStartedLatch = CountDownLatch(1)

        thread {
            waitTillWatcherThreadStartedLatch.countDown()

            doWatchFolderRecursively(folder, changeListener)
        }

        try {
            waitTillWatcherThreadStartedLatch.await()

            Thread.sleep(250)
        } catch (ignored: Exception) { }
    }

}