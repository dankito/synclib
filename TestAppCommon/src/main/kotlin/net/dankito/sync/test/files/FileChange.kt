package net.dankito.sync.test.files


enum class FileChange {

    Created,
    Modified,
    Deleted

}