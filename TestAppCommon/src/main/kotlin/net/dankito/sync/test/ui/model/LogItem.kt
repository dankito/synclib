package net.dankito.sync.test.ui.model

import java.util.*


open class LogItem(val logMessage: String, val timestamp: Date = Date()) {

    override fun toString(): String {
        return logMessage
    }

}