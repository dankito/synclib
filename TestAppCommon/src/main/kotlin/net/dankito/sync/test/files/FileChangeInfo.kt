package net.dankito.sync.test.files

import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.JavaIoFileInfo
import java.io.File


class FileChangeInfo(val file: FileInfo, val relativePath: String, val change: FileChange) {

    companion object {

        fun of(file: FileInfo, baseFolder: File, change: FileChange): FileChangeInfo {
            val mappedFile = if (file is JavaIoFileInfo) FileInfo(file.name, file.absolutePath, file.isDirectory,
                    file.size, file.id.takeIf { it.isNotBlank() } ?: file.absolutePath) // TODO: what about subFiles?
                else file
            return FileChangeInfo(mappedFile, file.toFile().toRelativeString(baseFolder), change)
        }

    }


    protected constructor() : this(FileInfo("", "", true, 0), "", FileChange.Deleted)


    override fun toString(): String {
        return "$change ${file.absolutePath}"
    }

}