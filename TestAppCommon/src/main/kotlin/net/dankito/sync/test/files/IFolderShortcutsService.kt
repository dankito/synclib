package net.dankito.sync.test.files

import net.dankito.utils.io.FileInfo


interface IFolderShortcutsService {

    fun getFolderShortcuts(): List<FileInfo>

}