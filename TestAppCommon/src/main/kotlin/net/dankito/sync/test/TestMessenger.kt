package net.dankito.sync.test

import net.dankito.sync.messenger.NanoHttpdMessenger
import net.dankito.sync.messenger.http.IUrlToMessageBodyMapper
import net.dankito.sync.test.files.FolderShortcutsService
import net.dankito.sync.test.files.IFolderShortcutsService
import net.dankito.sync.test.messenger.http.MessageHandler
import net.dankito.sync.test.messenger.http.UrlToMessageBodyMapper
import net.dankito.sync.test.messenger.http.messages.DeviceInfo
import net.dankito.sync.test.messenger.http.messages.SynchronizeFolderMessage
import net.dankito.utils.serialization.ISerializer
import net.dankito.utils.serialization.JacksonJsonSerializer
import java.io.File


open class TestMessenger(device: DeviceInfo, urlToMessageBodyMapper: IUrlToMessageBodyMapper = UrlToMessageBodyMapper(),
                         serializer: ISerializer = JacksonJsonSerializer(),
                         folderShortcutsService: IFolderShortcutsService = FolderShortcutsService(),
                         private val synchronizeFolderHandler: (SynchronizeFolderMessage) -> Unit,
                         private val messageHandler: MessageHandler = MessageHandler(device, folderShortcutsService,
                            synchronizeFolderHandler = synchronizeFolderHandler))

    : NanoHttpdMessenger(urlToMessageBodyMapper, serializer, { _, _, requestBody -> messageHandler.handleMessage(requestBody) }) {


    init {
        setGetFileForIdCallback { File(it) }
    }

}
