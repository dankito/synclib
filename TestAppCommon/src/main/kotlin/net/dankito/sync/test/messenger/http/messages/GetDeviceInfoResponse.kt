package net.dankito.sync.test.messenger.http.messages


open class GetDeviceInfoResponse(val deviceName: String,
                                 val fileSyncPort: Int,
                                 val publicKey: ByteArray) {

    protected constructor() : this("", 0, ByteArray(0))


    override fun toString(): String {
        return "deviceName = $deviceName, fileSyncPort = $fileSyncPort"
    }

}