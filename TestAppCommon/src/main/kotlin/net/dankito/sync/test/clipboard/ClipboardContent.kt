package net.dankito.sync.test.clipboard

import net.dankito.sync.test.BaseEntity
import javax.persistence.Column
import javax.persistence.Entity


@Entity
class ClipboardContent(

        @Column
        val plainText: String?,

        @Column
        val html: String?,

        @Column
        val files: List<String> = listOf()

) : BaseEntity() {

    internal constructor() : this(null, null) // for object deserializers


    override fun toString(): String {
        return "$plainText, ${files.size} files"
    }

}