package net.dankito.sync.test.messenger.http.messages

import net.dankito.sync.test.files.FileChangeInfo


class SynchronizeFolderResponse(val filesInRemoteFolder: List<FileChangeInfo>) {

    protected constructor() : this(listOf())

}