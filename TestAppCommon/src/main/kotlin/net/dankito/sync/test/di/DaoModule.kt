package net.dankito.sync.test.di

import dagger.Module
import dagger.Provides
import net.dankito.jpa.entitymanager.EntityManagerConfiguration
import java.io.File
import javax.inject.Named
import javax.inject.Singleton


@Module
class DaoModule(private val dataFolder: File = File("data")) {

    companion object {

        const val DataFolderKey = "data.folder"

    }


    @Provides
    @Singleton
    @Named(DataFolderKey)
    fun provideDataFolder(): File {
        return dataFolder
    }


    @Provides
    @Singleton
    fun provideEntityManagerConfiguration(@Named(DataFolderKey) dataFolder: File) : EntityManagerConfiguration {
        return EntityManagerConfiguration(dataFolder.path, "sync_test")
    }

}