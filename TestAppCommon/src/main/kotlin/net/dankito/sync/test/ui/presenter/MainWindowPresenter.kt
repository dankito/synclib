package net.dankito.sync.test.ui.presenter

import net.dankito.jpa.entitymanager.IEntityManager
import net.dankito.sync.Endpoint
import net.dankito.sync.data.changeshandler.EntitySynchronizedEvent
import net.dankito.sync.discovery.IDeviceDiscoverer
import net.dankito.sync.security.ISecurityProvider
import net.dankito.sync.test.SyncTestClient
import net.dankito.sync.test.files.IFolderShortcutsService
import net.dankito.sync.test.messenger.http.messages.DeviceInfo
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentMessage
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentResponse
import net.dankito.sync.test.ui.model.LogItem
import net.dankito.utils.FormatUtils
import net.dankito.utils.IThreadPool
import net.dankito.utils.events.IEventBus
import net.dankito.utils.io.FileInfo
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


open class MainWindowPresenter(val eventBus: IEventBus, val threadPool: IThreadPool,
                               protected val showLogMessageToUser: (LogItem) -> Unit) {

    companion object {
        val LogItemTimestampFormat = SimpleDateFormat("HH:mm:ss.SSS")
    }


    lateinit var syncTestClient: SyncTestClient
        protected set


    protected val formatUtils = FormatUtils()


    fun start(deviceName: String, entityManager: IEntityManager, discoverer: IDeviceDiscoverer,
              securityProvider: ISecurityProvider,
              folderShortcutsService: IFolderShortcutsService) {

        eventBus.subscribe(EntitySynchronizedEvent::class.java) {
            addLogMessage("Synchronized entity ${it.entity}")
        }

        syncTestClient = SyncTestClient(deviceName, entityManager, securityProvider, discoverer, folderShortcutsService)

        syncTestClient.start()
    }

    fun destroy() {
        syncTestClient.stop()
    }


    fun downloadSelectedFilesAsync(device: DeviceInfo, downloadDirectory: File, files: List<FileInfo>?) {
        threadPool.runAsync {
            downloadSelectedFiles(device, downloadDirectory, files)
        }
    }

    fun downloadSelectedFiles(device: DeviceInfo, downloadDirectory: File, files: List<FileInfo>?) {
        files?.forEachIndexed { index, file ->
            val destinationFile = File(downloadDirectory, file.name)
            addLogMessage("Downloading file ${file.name} of size ${file.size} to ${destinationFile.absolutePath}")

            retrieveFile(device.endpoint, file, destinationFile,
                    "${index + 1} of ${files.size}")
        }
    }


    fun downloadFolderAsync(device: DeviceInfo, remoteFolderToDownload: File, localDestinationFolder: File) {
        threadPool.runAsync {
            downloadFolder(device, remoteFolderToDownload, localDestinationFolder)
        }
    }

    fun downloadFolder(device: DeviceInfo, remoteFolderToDownload: File, localDestinationFolder: File) {
        addLogMessage("Downloading folder '${remoteFolderToDownload.name}' from $device to '$localDestinationFolder' ...")

        val countFilesBefore = localDestinationFolder.list()?.size ?: 0
        val startTime = Date().time

        syncTestClient.downloadFolder(device, remoteFolderToDownload, localDestinationFolder)

        val durationMillis = Date().time - startTime
        val countFilesAfter = localDestinationFolder.list()?.size ?: 0
        val countDownloadedFiles = countFilesAfter - countFilesBefore
        addLogMessage("Downloaded $countDownloadedFiles files in ${durationMillis / 1000} seconds from $device to '$localDestinationFolder' ...")
    }


    fun synchronizeFoldersAsync(device: DeviceInfo, localFolder: FileInfo, remoteFolder: FileInfo) {
        threadPool.runAsync {
            synchronizeFolders(device, localFolder, remoteFolder)
        }
    }

    fun synchronizeFolders(device: DeviceInfo, localFolder: FileInfo, remoteFolder: FileInfo) {
        syncTestClient.synchronizeFolders(device, localFolder, remoteFolder)
    }


    fun doTransferSpeedTest(device: DeviceInfo) {
        syncTestClient.messenger.sendMessageAsync(device.endpoint,
                syncTestClient.messageBuilder.getContentDirectoryMessage(GetDirectoryContentMessage.FilesForDownloadSpeedTest),
                GetDirectoryContentResponse::class.java) { response ->

            if (response.isSuccessful == false) {
                addLogMessage("Could not receive files from $device: ${response.error}")
            }
            else {
                response.body?.files?.let { receivedFiles ->
                    addLogMessage("Received list with ${response.body?.files?.size} files from ${device.address}")

                    var totalDuration = 0L
                    var totalDownloadedBytes = 0L

                    receivedFiles.forEachIndexed { index, file ->
                        if (file.isDirectory == false) {
                            val tempFile = File.createTempFile(file.name, ".tmp")

                            totalDuration += retrieveFile(device.endpoint, file, tempFile,
                                    "${index + 1} of ${receivedFiles.size}")
                            totalDownloadedBytes += file.size

                            tempFile.delete()
                        }
                    }

                    addLogMessage("Downloaded $totalDownloadedBytes bytes in ${totalDuration / 1000} seconds, ${formatUtils.formatSpeed(totalDownloadedBytes, totalDuration)}")
                }
            }
        }
    }

    private fun retrieveFile(endpoint: Endpoint, file: FileInfo, destinationFile: File, index: String): Long {
        val start = Date().time

        syncTestClient.retrieveFile(endpoint, file, destinationFile)

        val duration = Date().time - start
        printFileSynchronizationStatistics(file, destinationFile, duration, index)

        return duration
    }

    private fun printFileSynchronizationStatistics(originalFileInfo: FileInfo, synchronizedFile: File,
                                                   durationMillis: Long, index: String) {

        addLogMessage("[$index] ${originalFileInfo.name}: Copied ${synchronizedFile.length()} of " +
                "${originalFileInfo.size} bytes in ${durationMillis / 1000} seconds, " +
                formatUtils.formatSpeed(synchronizedFile.length(), durationMillis))
    }

    private fun addLogMessage(message: String) {
        showLogMessageToUser(LogItem(message))
    }

}