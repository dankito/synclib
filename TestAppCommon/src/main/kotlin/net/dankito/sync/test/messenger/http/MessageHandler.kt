package net.dankito.sync.test.messenger.http

import net.dankito.sync.messenger.http.HttpResponse
import net.dankito.sync.messenger.http.HttpResponseStatus
import net.dankito.sync.test.files.FileChange
import net.dankito.sync.test.files.FileChangeInfo
import net.dankito.sync.test.files.IFolderShortcutsService
import net.dankito.sync.test.messenger.http.messages.*
import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.FileUtils
import net.dankito.utils.io.ListDirectory
import net.dankito.utils.os.OsHelper
import java.io.File
import kotlin.concurrent.thread


open class MessageHandler(protected val localDevice: DeviceInfo,
                          protected val folderShortcutsService: IFolderShortcutsService,
                          protected val fileUtils: FileUtils = FileUtils(),
                          protected val synchronizeFolderHandler: (SynchronizeFolderMessage) -> Unit) {

    open fun handleMessage(message: Any?): HttpResponse? {
        message?.let {
            val response = getResponse(message)

            response?.let {
                return HttpResponse(HttpResponseStatus.OK, response)
            }
        }

        return HttpResponse(HttpResponseStatus.NOT_FOUND)
    }

    protected open fun getResponse(message: Any): Any? {
        when (message) {
            is GetDeviceInfoMessage -> return handleGetDeviceInfoMessage(message)
            is GetDirectoryContentMessage -> return handleGetDirectoryContentMessage(message)
            is GetFolderShortcutsMessage -> return handleGetFolderShortcutsMessage(message)
            is SynchronizeFolderMessage -> return handleSynchronizeFolderMessage(message)
            else -> return null
        }
    }


    protected open fun handleGetDeviceInfoMessage(message: GetDeviceInfoMessage): GetDeviceInfoResponse {
        return GetDeviceInfoResponse(localDevice.deviceName, localDevice.fileSyncPort, localDevice.publicKey)
    }


    protected open fun handleGetDirectoryContentMessage(message: GetDirectoryContentMessage): GetDirectoryContentResponse? {
        val directory = getDirectoryForGetDirectoryContentMessage(message)

        return handleGetDirectoryContentMessage(message, directory)
    }

    protected open fun getDirectoryForGetDirectoryContentMessage(message: GetDirectoryContentMessage): File {
        if (message.directory == GetDirectoryContentMessage.RootFolder) {
            return fileUtils.getOsRootFolder() // TODO: no support for Windows yet (and MacOS questionable)
        }

        if (message.directory == GetDirectoryContentMessage.FilesForDownloadSpeedTest) { // TODO: remove again
            if (OsHelper().isRunningOnAndroid) {
                return File("/storage/emulated/0/Download")
            }
            return File(System.getProperty("user.home"))
        }

        // TODO: this is part of AndroidFolderUtils from AndroidUtils, try to use this instead
        if(message.directory == "/storage/emulated") { // we're allowed to list /storage/emulated -> list /storage/emulated/0 instead
            return File(message.directory, "0")
        }

        if(message.directory == "/storage/self") { // we're allowed to list /storage/self -> list /storage/self/primary instead
            return File(message.directory, "primary")
        }

        return File(message.directory)
    }

    protected open fun handleGetDirectoryContentMessage(message: GetDirectoryContentMessage, directory: File): GetDirectoryContentResponse? {
        if (directory.exists()) { // TODO: throw an exception otherwise?
            fileUtils.getFilesOfDirectorySorted(directory, message.listDirectory, message.folderDepth)?.let { files ->
                return GetDirectoryContentResponse(files.map { FileInfo.from(it) })
            }
        }

        return null
    }


    protected open fun handleGetFolderShortcutsMessage(message: GetFolderShortcutsMessage): GetFolderShortcutsResponse {
        return GetFolderShortcutsResponse(folderShortcutsService.getFolderShortcuts())
    }


    protected open fun handleSynchronizeFolderMessage(message: SynchronizeFolderMessage): SynchronizeFolderResponse {
        thread {
            synchronizeFolder(message)
        }

        val removeFolderAsFile = message.remoteFolder.toFile()

        val filesInRemoteFolder = fileUtils.getFilesOfDirectorySortedAsFileInfo(message.remoteFolder.toFile(),
                ListDirectory.DirectoriesAndFiles, FileUtils.MaxFolderDepth)?.map {
            FileChangeInfo.of(it, removeFolderAsFile, FileChange.Created)
        }

        return SynchronizeFolderResponse(filesInRemoteFolder ?: listOf())
    }

    protected open fun synchronizeFolder(message: SynchronizeFolderMessage) {
        synchronizeFolderHandler(message)
    }

}