package net.dankito.sync.test.messenger.http.messages

import net.dankito.utils.io.FileInfo


open class GetFolderShortcutsResponse(val files: List<FileInfo>) {

    protected constructor() : this(listOf()) // for object deserializers


    override fun toString(): String {
        return "Shortcuts: $files"
    }

}