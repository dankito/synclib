package net.dankito.sync.test.messenger.http.messages

import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.ListDirectory


open class GetDirectoryContentMessage(val directory: String,
                                      val listDirectory: ListDirectory = ListDirectory.DirectoriesAndFiles,
                                      val folderDepth: Int = 1) {

    companion object {
        const val RootFolder = "\$RootFolder\$" // to have a platform independent way to list OS' root directories / drives

        const val FilesForDownloadSpeedTest = "\$FilesForDownloadSpeedTest\$" // TODO: remove again

        val RootFolderFileInfo = FileInfo("/", RootFolder, true, 0)
    }


    protected constructor() : this("")

}