package net.dankito.sync.test.messenger.http.messages

import net.dankito.sync.test.files.FileChangeInfo
import net.dankito.utils.io.FileInfo


class SynchronizeFolderMessage(val localFolder: FileInfo,
                               val remoteFolder: FileInfo,
                               val filesInLocalFolder: List<FileChangeInfo>,
                               val localDeviceUuid: String) {

    protected constructor() : this(FileInfo("", "", true, 0), FileInfo("", "", true, 0), listOf(), "")


    override fun toString(): String {
        return "Synchronizing ${localFolder.name} with remote ${remoteFolder.name}"
    }

}