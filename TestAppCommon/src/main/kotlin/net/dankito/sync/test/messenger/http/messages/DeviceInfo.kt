package net.dankito.sync.test.messenger.http.messages

import net.dankito.sync.Endpoint


open class DeviceInfo(val address: String,
                      val messagesPort: Int,
                      val fileSyncPort: Int,
                      val deviceUuid: String,
                      val deviceName: String,
                      val publicKey: ByteArray) {

    val endpoint = Endpoint(address, messagesPort, fileSyncPort)


    protected constructor() : this("", 0, 0, "", "", ByteArray(0))


    override fun toString(): String {
        return deviceName
    }

}