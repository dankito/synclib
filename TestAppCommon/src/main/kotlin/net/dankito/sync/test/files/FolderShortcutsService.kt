package net.dankito.sync.test.files

import net.dankito.utils.io.FileInfo
import java.io.File
import java.nio.file.Files


open class FolderShortcutsService : IFolderShortcutsService {

    override fun getFolderShortcuts(): List<FileInfo> {
        val roots = File.listRoots()

        if (roots.size == 1 && roots.first().absolutePath == "/") { // a Unix system
            return getFolderShortcutsForUnix()
        }

        return roots.asList().map { FileInfo.from(it) }
    }


    protected open fun getFolderShortcutsForUnix(): List<FileInfo> {
        val folderShortcuts = mutableListOf<FileInfo>()

        getUserHome()?.let { userHome ->
            folderShortcuts.add(userHome)
        }

        val mountPoints = getUnixMountPoints()

        mountPoints.forEach { mountPoint ->
            folderShortcuts.add(FileInfo.from(File(mountPoint)))
        }

        return folderShortcuts
    }

    protected open fun getUserHome(): FileInfo? {
        System.getProperty("user.home", null)?.let { userHomePath ->
            return FileInfo.from(File(userHomePath))
        }

        return null
    }

    protected open fun getUnixMountPoints(): List<String> {
        val dfLines = executeCommandlineCommand("df")

        return dfLines.map { lineFromDf ->
            val parts = lineFromDf.split(' ').filter { it.isNotEmpty() }
            if (parts.isNotEmpty() && parts.first().startsWith('/')) { // a mounted partition
                parts.last()
            } else {
                null
            }
        }.filterNotNull()
    }

    protected open fun getUnixMountPointsWithPartitionLabels(): List<Pair<String, String>> {
        val dfLines = executeCommandlineCommand("df")

        val devToMountPoints = dfLines.map { lineFromDf ->
            val parts = lineFromDf.split(' ').filter { it.isNotEmpty() }
            if (parts.isNotEmpty() && parts[0].startsWith('/')) { // a mounted partition
                Pair(parts.first(), parts.last())
            } else {
                null
            }
        }.filterNotNull().associateBy({ it.first }, { it.second })


        val blkidLines = executeCommandlineCommand("blkid")

        val devToLabels = blkidLines.map { lineFromBlkid ->
            val parts = lineFromBlkid.split(' ')
            if (parts.isNotEmpty()) {
                val dev = parts[0].trim().replace(":", "")
                var label = parts.firstOrNull { it.startsWith("label=\"", true) }?.substring("label=\"".length)?.replace("\"", "")
                if (label == null) {
                    label = parts.firstOrNull { it.startsWith("UUID=\"", true) }?.substring("UUID=\"".length)?.replace("\"", "")
                }

                if (label != null) {
                    Pair(dev, label)
                } else {
                    null
                }
            } else {
                null
            }
        }.filterNotNull().associateBy({ it.first }, { it.second })


        val mountPointsToLabel = mutableListOf<Pair<String, String>>()

        devToMountPoints.forEach { dev, mountPoint ->
            devToLabels[dev]?.let { label ->
                mountPointsToLabel.add(Pair(mountPoint, label))
            }
        }

        return mountPointsToLabel
    }

    protected open fun getDevToFileSystemType(mountPoints: List<String>): List<Pair<String, String>> {
        val fileStores = mountPoints.map { Files.getFileStore(File(it).toPath()) }

        return fileStores.map { Pair(it.name(), it.type()) }
    }

    protected open fun executeCommandlineCommand(command: String) =
            Runtime.getRuntime().exec(command).inputStream.bufferedReader().readLines()

}