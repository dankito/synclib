package net.dankito.sync.test.messenger.http

import net.dankito.sync.messenger.http.HttpMethod
import net.dankito.sync.messenger.http.UrlToMessageBodyMapperBase
import net.dankito.sync.test.messenger.http.messages.GetDeviceInfoMessage
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentMessage
import net.dankito.sync.test.messenger.http.messages.GetFolderShortcutsMessage
import net.dankito.sync.test.messenger.http.messages.SynchronizeFolderMessage


open class UrlToMessageBodyMapper : UrlToMessageBodyMapperBase(buildUrlToMessageBodyMap()) {

    companion object {

        const val MessagesSubUrl = "messages/"

        const val DirectoriesSubUrl = "directories/"

        const val FilesUrlStart = "files/"

        const val GetDeviceInfoUrl = MessagesSubUrl + "deviceinfo/"

        const val GetDirectoryContentUrl = MessagesSubUrl + DirectoriesSubUrl + "list/"

        const val GetFolderShortcutsUrl = MessagesSubUrl + DirectoriesSubUrl + "shortcuts/"

        const val SynchronizeFolderUrl = MessagesSubUrl + DirectoriesSubUrl + "sync/"


        fun buildUrlToMessageBodyMap(): Map<String, Map<HttpMethod, Class<*>>> {
            return mapOf<String, Map<HttpMethod, Class<*>>>(
                    Pair(GetDeviceInfoUrl, mapOf(Pair(HttpMethod.Get, GetDeviceInfoMessage::class.java))),
                    Pair(GetDirectoryContentUrl, mapOf(Pair(HttpMethod.Post, GetDirectoryContentMessage::class.java))),
                    Pair(GetFolderShortcutsUrl, mapOf(Pair(HttpMethod.Post, GetFolderShortcutsMessage::class.java))),
                    Pair(SynchronizeFolderUrl, mapOf(Pair(HttpMethod.Post, SynchronizeFolderMessage::class.java)))
            )
        }

    }

}