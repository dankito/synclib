package net.dankito.sync.test.di

import dagger.Module
import dagger.Provides
import net.dankito.utils.network.INetworkHelper
import net.dankito.utils.network.NetworkHelper
import javax.inject.Singleton


@Module
class NetworkModule {

    @Provides
    @Singleton
    fun provideNetworkHelper() : INetworkHelper {
        return NetworkHelper()
    }

}