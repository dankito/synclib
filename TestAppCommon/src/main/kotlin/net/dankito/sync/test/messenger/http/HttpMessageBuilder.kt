package net.dankito.sync.test.messenger.http

import net.dankito.sync.messenger.http.HttpMessage
import net.dankito.sync.messenger.http.HttpMessageBuilderBase
import net.dankito.sync.messenger.http.HttpMethod
import net.dankito.sync.test.files.FileChangeInfo
import net.dankito.sync.test.messenger.http.messages.GetDeviceInfoMessage
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentMessage
import net.dankito.sync.test.messenger.http.messages.GetFolderShortcutsMessage
import net.dankito.sync.test.messenger.http.messages.SynchronizeFolderMessage
import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.ListDirectory


open class HttpMessageBuilder(protected val urlMapper: UrlToMessageBodyMapper) : HttpMessageBuilderBase() {

    fun getDeviceInfoMessage(): HttpMessage<GetDeviceInfoMessage> {
        return createGetMessage(GetDeviceInfoMessage(), GetDeviceInfoMessage::class.java)
    }

    fun getContentDirectoryMessage(directory: String, listDirectory: ListDirectory = ListDirectory.DirectoriesAndFiles,
                                   folderDepth: Int = 1): HttpMessage<GetDirectoryContentMessage> {

        return createPostMessage(GetDirectoryContentMessage(directory, listDirectory, folderDepth),
                GetDirectoryContentMessage::class.java)
    }

    fun getGetFolderShortcutsMessage(): HttpMessage<GetFolderShortcutsMessage> {

        return createPostMessage(GetFolderShortcutsMessage(), GetFolderShortcutsMessage::class.java)
    }

    fun getSynchronizeFolderMessage(localFolder: FileInfo, remoteFolder: FileInfo,
                                    filesInLocalFolder: List<FileChangeInfo>, localDeviceUuid: String)
            : HttpMessage<SynchronizeFolderMessage> {

        return createPostMessage(SynchronizeFolderMessage(localFolder, remoteFolder, filesInLocalFolder, localDeviceUuid),
                SynchronizeFolderMessage::class.java)
    }


    protected open fun <TBody> createGetMessage(body: TBody, bodyTypeClass: Class<TBody>): HttpMessage<TBody> {
        return createHttpMessage(HttpMethod.Get, body, bodyTypeClass)
    }

    protected open fun <TBody> createPostMessage(body: TBody, bodyTypeClass: Class<TBody>): HttpMessage<TBody> {
        return createHttpMessage(HttpMethod.Post, body, bodyTypeClass)
    }

    protected open fun <TBody> createHttpMessage(method: HttpMethod, body: TBody, bodyTypeClass: Class<TBody>): HttpMessage<TBody> {
        return HttpMessage(method, urlMapper.getSubUrlForMessageBody(bodyTypeClass, method), bodyTypeClass, body)
    }

}