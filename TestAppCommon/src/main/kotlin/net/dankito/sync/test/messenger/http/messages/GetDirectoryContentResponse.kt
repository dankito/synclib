package net.dankito.sync.test.messenger.http.messages

import net.dankito.utils.io.FileInfo
import java.io.File


open class GetDirectoryContentResponse(val files: List<FileInfo>) {

    companion object {
        fun of(files: List<File>): GetDirectoryContentResponse {
            val fileInfos = files.map { file ->
                if (file.isDirectory) {
                    return@map FileInfo(file.name, file.absolutePath, true, 0L)
                }

                return@map FileInfo(file.name, file.absolutePath, false, file.length())
            }

            return GetDirectoryContentResponse(fileInfos)
        }
    }


    protected constructor() : this(listOf()) // for Jackson

}