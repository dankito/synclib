package net.dankito.sync.test

import net.dankito.jpa.couchbaselite.CouchbaseLiteEntityManagerBase
import net.dankito.jpa.entitymanager.EntityManagerConfiguration
import net.dankito.jpa.entitymanager.IEntityManager
import net.dankito.sync.Endpoint
import net.dankito.sync.data.CouchbaseLiteSyncManager
import net.dankito.sync.data.SyncManagerConfig
import net.dankito.sync.data.changeshandler.EntitySynchronizedEvent
import net.dankito.sync.data.changeshandler.PostToEventBusSynchronizedChangesHandler
import net.dankito.sync.discovery.DeviceDiscovererConfig
import net.dankito.sync.discovery.DiscoveredDevice
import net.dankito.sync.discovery.IDeviceDiscoverer
import net.dankito.sync.file.HttpServerFileSyncService
import net.dankito.sync.messenger.IMessenger
import net.dankito.sync.proxy.ReverseProxy
import net.dankito.sync.proxy.requestrouter.DataSyncRequestRouter
import net.dankito.sync.proxy.requestrouter.FilesRouter
import net.dankito.sync.proxy.requestrouter.MessagesRouter
import net.dankito.sync.security.ISecurityProvider
import net.dankito.sync.test.di.CommonComponent
import net.dankito.sync.test.files.FileChange
import net.dankito.sync.test.files.FileChangeInfo
import net.dankito.sync.test.files.IFolderShortcutsService
import net.dankito.sync.test.messenger.http.HttpMessageBuilder
import net.dankito.sync.test.messenger.http.UrlToMessageBodyMapper
import net.dankito.sync.test.messenger.http.messages.*
import net.dankito.utils.events.IEventBus
import net.dankito.utils.extensions.toFileInfo
import net.dankito.utils.io.FileInfo
import net.dankito.utils.io.FileUtils
import net.dankito.utils.io.ListDirectory
import org.slf4j.LoggerFactory
import java.io.File
import java.security.KeyPair
import java.util.*
import java.util.concurrent.ConcurrentSkipListMap
import javax.inject.Inject
import kotlin.collections.ArrayList


open class SyncTestClient(deviceName: String,
                          protected val entityManager: IEntityManager,
                          securityProvider: ISecurityProvider,
                          protected val discoverer: IDeviceDiscoverer,
                          folderShortcutsService: IFolderShortcutsService) {

    companion object {
        const val ProxyPort = 12345

        const val MessengerPort = 12346

        const val FileSyncPort = MessengerPort

        const val DataSynchronizationPort = MessengerPort + 1

        private val log = LoggerFactory.getLogger(SyncTestClient::class.java)
    }


    @Inject
    lateinit var entityManagerConfiguration: EntityManagerConfiguration

    @Inject
    lateinit var eventBus: IEventBus


    val localDevice: DeviceInfo

    val messenger: IMessenger

    val messageBuilder: HttpMessageBuilder = HttpMessageBuilder(UrlToMessageBodyMapper())


    protected val proxy = ReverseProxy()

    protected val fileSyncService = HttpServerFileSyncService()

    protected lateinit var syncManager: CouchbaseLiteSyncManager

    protected val fileUtils = FileUtils()

    protected val keys: KeyPair


    protected val discoveredDevices = ConcurrentSkipListMap<String, DeviceInfo>()

    protected val deviceDiscoveredListeners = ArrayList<(DeviceInfo) -> Unit>()

    protected val deviceDisconnectedListeners = ArrayList<(DeviceInfo) -> Unit>()


    init {
        CommonComponent.component.inject(this)

        keys = securityProvider.keyGenerator.generatePublicPrivateKeyPair(securityProvider.provider)

        localDevice = DeviceInfo("", MessengerPort, FileSyncPort, UUID.randomUUID().toString(), deviceName,
                keys.public.encoded)

        messenger = TestMessenger(localDevice, folderShortcutsService = folderShortcutsService,
                synchronizeFolderHandler = { synchronizeRemoteFolder(it) } )
    }


    fun start() {
        entityManager.open(entityManagerConfiguration)

        proxy.addRouter(DataSyncRequestRouter(entityManagerConfiguration.databaseName, DataSynchronizationPort))
        proxy.addRouter(FilesRouter(FileSyncPort))
        proxy.addRouter(MessagesRouter(MessengerPort))

        proxy.start(ProxyPort)

        messenger.start(MessengerPort)

        val synchronizedChangesHandler = PostToEventBusSynchronizedChangesHandler(entityManager, eventBus)
        syncManager = CouchbaseLiteSyncManager(entityManager as CouchbaseLiteEntityManagerBase,
                SyncManagerConfig(false, true, synchronizedChangesHandler, ProxyPort))

        syncManager.openSynchronizationPort(DataSynchronizationPort)

        discoverer.addDeviceDiscoveredListener { deviceDiscovered(it) }
        discoverer.addDeviceDisconnectedListener { deviceDisconnected(it) }

        discoverer.startAsync(DeviceDiscovererConfig("", "SyncTest", ProxyPort, localDevice.deviceUuid, 2703)) // for custom UDP based discoverers}

        eventBus.subscribe(EntitySynchronizedEvent::class.java) {
            log.info("Synchronized entity: ${it.entity}")
        }
    }

    fun stop() {
        syncManager.stop()

        discoverer.stop()

        messenger.stop()

        proxy.close()
    }


    fun retrieveFile(endpoint: Endpoint, file: FileInfo, destinationFile: File) {
        log.info("Trying to retrieve file ${file.name} (${file.size} bytes) from ${endpoint.address}:${endpoint.fileSyncPort}")

        fileSyncService.retrieveFile(endpoint, file.id, destinationFile)
    }


    fun downloadFolder(device: DeviceInfo, remoteFolderToDownload: File, localDestinationFolder: File) {
        val response = messenger.sendMessage(device.endpoint,
                messageBuilder.getContentDirectoryMessage(remoteFolderToDownload.absolutePath, ListDirectory.DirectoriesAndFiles,
                        FileUtils.MaxFolderDepth), GetDirectoryContentResponse::class.java)

        response.body?.files?.let { filesInRemoteFolder ->
            downloadFilesFromRemoteFolder(device.endpoint, localDestinationFolder, filesInRemoteFolder.map {
                FileChangeInfo(it, it.toFile().toRelativeString(remoteFolderToDownload), FileChange.Created) })
        }

        // TODO: show error
    }


    fun synchronizeFolders(device: DeviceInfo, localFolder: FileInfo, remoteFolder: FileInfo) {
        val localFolderAsFile = localFolder.toFile()

        val filesInLocalFolder = fileUtils.getFilesOfDirectorySortedAsFileInfo(
                localFolder.toFile(), ListDirectory.DirectoriesAndFiles, FileUtils.MaxFolderDepth)?.map {
                    FileChangeInfo.of(it, localFolderAsFile, FileChange.Created)
                }

        val response = messenger.sendMessage(device.endpoint,
                messageBuilder.getSynchronizeFolderMessage(localFolderAsFile.toFileInfo(), remoteFolder,
                        filesInLocalFolder ?: listOf(), localDevice.deviceUuid), SynchronizeFolderResponse::class.java)

        response.body?.filesInRemoteFolder?.let { filesInRemoteFolder ->
            downloadFilesFromRemoteFolder(device.endpoint, localFolderAsFile, filesInRemoteFolder)
        }

        // TODO: show error
    }

    protected open fun synchronizeRemoteFolder(message: SynchronizeFolderMessage) {
        findDiscoveredDevice(message.localDeviceUuid)?.let { remoteDevice ->
            val localBaseFolder = message.remoteFolder.toFile()

            downloadFilesFromRemoteFolder(remoteDevice.endpoint, localBaseFolder, message.filesInLocalFolder)
        }
    }

    protected open fun downloadFilesFromRemoteFolder(endpoint: Endpoint, localBaseFolder: File,
                                                     filesInRemoteFolder: List<FileChangeInfo>) {

        filesInRemoteFolder.forEach { remoteFile ->
            val destinationFile = File(localBaseFolder, remoteFile.relativePath)

            if (remoteFile.file.isDirectory) {
                destinationFile.mkdirs()
            }
            else {
                destinationFile.parentFile?.mkdirs()

                retrieveFile(endpoint, remoteFile.file, destinationFile)
            }
        }
    }


    protected open fun deviceDiscovered(discoveredDevice: DiscoveredDevice) {
        log.info("Discovered device $discoveredDevice")

        messenger.sendMessageAsync(Endpoint(discoveredDevice.address, discoveredDevice.servicePort), messageBuilder.getDeviceInfoMessage(),
                GetDeviceInfoResponse::class.java) { response ->

            response.error?.let { log.error("Could not retrieve GetDeviceInfo response from $discoveredDevice", it) }
            ?: log.info("GetDeviceInfo response from $discoveredDevice: $response")

            if (response.isSuccessful == false) {
                // TODO: what to do in this case?
            }
            else {
                response.body?.let { getDeviceInfoResponse ->
                    deviceDiscovered(discoveredDevice, getDeviceInfoResponse)
                }
            }
        }
    }

    protected open fun deviceDiscovered(discoveredDevice: DiscoveredDevice, getDeviceInfoResponse: GetDeviceInfoResponse) {
        val remoteDevice = DeviceInfo(discoveredDevice.address, discoveredDevice.servicePort,
                getDeviceInfoResponse.fileSyncPort, discoveredDevice.serviceDescription,
                getDeviceInfoResponse.deviceName, getDeviceInfoResponse.publicKey)

        discoveredDevices.put(discoveredDevice.serviceDescription, remoteDevice)

        syncManager.startSynchronizationWithDevice(discoveredDevice)

        deviceDiscoveredListeners.forEach { it(remoteDevice) }
    }

    protected open fun deviceDisconnected(disconnectedDevice: DiscoveredDevice) {
        discoveredDevices.remove(disconnectedDevice.serviceDescription)?.let { removedDevice ->

            deviceDisconnectedListeners.forEach { it(removedDevice) }

        }
    }

    protected open fun findDiscoveredDevice(deviceUuid: String): DeviceInfo? {
        return discoveredDevices[deviceUuid]
    }


    fun addDeviceDiscoveredListener(listener: (DeviceInfo) -> Unit) {
        deviceDiscoveredListeners.add(listener)
    }

    fun addDeviceDisconnectedListener(listener: (DeviceInfo) -> Unit) {
        deviceDisconnectedListeners.add(listener)
    }

}