package net.dankito.sync.test.files

import java.io.File


interface IFileSystemWatcher {

    fun watchFolderRecursively(folder: File, changeListener: (FileChangeInfo) -> Unit)

    fun stopWatching(folder: File)

    fun stopAllWatchers()

}