package net.dankito.sync.test.di

import dagger.Component
import net.dankito.sync.test.SyncTestClient
import javax.inject.Singleton


@Singleton
@Component(modules = [ CommonUtilsModule::class, NetworkModule::class, DaoModule::class ])
interface CommonComponent {

    companion object {
        lateinit var component: CommonComponent
    }


    fun inject(syncTestClient: SyncTestClient)

}