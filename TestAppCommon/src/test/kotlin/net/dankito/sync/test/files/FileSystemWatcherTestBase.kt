package net.dankito.sync.test.files

import net.dankito.utils.io.FileUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.File


abstract class FileSystemWatcherTestBase {

    private val fileUtils = FileUtils()

    private lateinit var testFolder: File

    private val underTest: IFileSystemWatcher = createFileSystemWatcher()


    abstract fun createFileSystemWatcher(): IFileSystemWatcher


    @Before
    @Throws(Exception::class)
    fun setUp() {
        val tempFile = File.createTempFile("JustWantToKnowTempDir", ".tmp")

        testFolder = File(tempFile.parent, "FileSystemWatcherTest")
        testFolder.mkdirs()

        tempFile.delete()
    }

    @After
    fun tearDown() {
        underTest.stopAllWatchers()

        fileUtils.deleteFolderRecursively(testFolder)
    }


    @Test
    fun createFile() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFile = createTestFile()

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos.get(0).relativePath).isEqualTo(testFile.name)
        assertThat(raisedFileChangeInfos.get(0).change).isEqualTo(FileChange.Created)
    }

    @Test
    fun modifyFile() {
        // given
        val testFile = createTestFile()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        modifyFile(testFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos.get(0).relativePath).isEqualTo(testFile.name)
        assertThat(raisedFileChangeInfos.get(0).change).isEqualTo(FileChange.Modified)
    }

    @Test
    fun createAndModifyFile() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFile = createTestFile()
        modifyFile(testFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFile.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Modified)
    }

    @Test
    fun deleteFile() {
        // given
        val testFile = createTestFile()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        deleteFile(testFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos.get(0).relativePath).isEqualTo(testFile.name)
        assertThat(raisedFileChangeInfos.get(0).change).isEqualTo(FileChange.Deleted)
    }

    @Test
    fun createAndDeleteFile() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFile = createTestFile()
        deleteFile(testFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFile.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted)
    }

    @Test
    fun modifyAndDeleteFile() {
        // given
        val testFile = createTestFile()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        modifyFile(testFile)
        deleteFile(testFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFile.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Modified, FileChange.Deleted)
    }

    @Test
    fun createModifyAndDeleteFile() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFile = createTestFile()
        modifyFile(testFile)
        deleteFile(testFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(3)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFile.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Modified, FileChange.Deleted)
    }


    @Test
    fun createFolder() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFolder = createTestFolder()

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsExactly(testFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Created)
    }

    @Test
    fun renameFolder() {
        // given
        val testFolder = createTestFolder()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val renamedFolder = rename(testFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsExactly(testFolder.name, renamedFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Deleted, FileChange.Created)
    }

    @Test
    fun createAndRenameFolder() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFolder = createTestFolder()
        val renamedFolder = rename(testFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(3)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFolder.name, renamedFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted, FileChange.Created)
    }

    @Test
    fun deleteFolder() {
        // given
        val testFolder = createTestFolder()
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        deleteFile(testFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change").containsOnly(FileChange.Deleted)
    }

    @Test
    fun createAndDeleteFolder() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFolder = createTestFolder()
        deleteFile(testFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted)
    }

    @Test
    fun createRenameAndDeleteFolder() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFolder = createTestFolder()
        val renamedFolder = rename(testFolder)
        deleteFile(renamedFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(4)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFolder.name, renamedFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted, FileChange.Created, FileChange.Deleted)
    }


    @Test
    fun createSubFolder() {
        // given
        val testFolder = createTestFolder()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolder = createTestFolder(testFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsExactly(subPath(subFolder))
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Created)
    }

    @Test
    fun renameSubFolder() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val renamedSubFolder = rename(subFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsExactly(subPath(subFolder), subPath(renamedSubFolder))
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Deleted, FileChange.Created)
    }

    @Test
    fun createAndRenameSubFolder() {
        // given
        val testFolder = createTestFolder()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolder = createTestFolder(testFolder)
        val renamedSubFolder = rename(subFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(3)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsOnly(subPath(subFolder), subPath(renamedSubFolder))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted, FileChange.Created)
    }

    @Test
    fun deleteSubFolder() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        deleteFile(subFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(subPath(subFolder))
        assertThat(raisedFileChangeInfos).extracting("change").containsOnly(FileChange.Deleted)
    }

    @Test
    fun createAndDeleteSubFolder() {
        // given
        val testFolder = createTestFolder()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolder = createTestFolder(testFolder)
        deleteFile(subFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(subPath(subFolder))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted)
    }

    @Test
    fun createRenameAndDeleteSubFolder() {
        // given
        val testFolder = createTestFolder()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolder = createTestFolder(testFolder)
        val renamedSubFolder = rename(subFolder)
        deleteFile(renamedSubFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(4)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsOnly(subPath(subFolder), subPath(renamedSubFolder))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted, FileChange.Created, FileChange.Deleted)
    }


    @Test
    fun createSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolderFile = createTestFile(subFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsExactly(subPath(subFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Created)
    }

    @Test
    fun renameSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)
        val subFolderFile = createTestFile(subFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val renamedSubFolderFile = renameTestFile(subFolderFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsExactly(subPath(subFolderFile), subPath(renamedSubFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Deleted, FileChange.Created)
    }

    @Test
    fun createAndRenameSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolderFile = createTestFile(subFolder)
        val renamedSubFolderFile = renameTestFile(subFolderFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(3)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsOnly(subPath(subFolderFile), subPath(renamedSubFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted, FileChange.Created)
    }

    @Test
    fun deleteSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)
        val subFolderFile = createTestFile(subFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        deleteFile(subFolderFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(subPath(subFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change").containsOnly(FileChange.Deleted)
    }

    @Test
    fun createAndDeleteSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolderFile = createTestFile(subFolder)
        deleteFile(subFolderFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(subPath(subFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted)
    }

    @Test
    fun createRenameAndDeleteSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolderFile = createTestFile(subFolder)
        val renamedSubFolderFile = renameTestFile(subFolderFile)
        deleteFile(renamedSubFolderFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(4)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsOnly(subPath(subFolderFile), subPath(renamedSubFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted, FileChange.Created, FileChange.Deleted)
    }


    protected open fun detectFileSystemChanges(): MutableList<FileChangeInfo> {
        val raisedFileChangeInfos = mutableListOf<FileChangeInfo>()

        underTest.watchFolderRecursively(testFolder) { fileChangeInfo ->
            raisedFileChangeInfos.add(fileChangeInfo)
        }

        return raisedFileChangeInfos
    }

    protected open fun createTestFile(parentFolder: File? = null, fileName: String = "test.txt"): File {
        val parent = parentFolder ?: testFolder

        val testFile = File(parent, fileName)

        testFile.createNewFile()

        giveFileSystemWatcherTimeToDetectChanges()

        return testFile
    }

    protected open fun createTestFolder(parentFolder: File? = null, folderName: String = "folder"): File {
        val parent = parentFolder ?: testFolder

        val folder = File(parent, folderName)

        folder.mkdirs()

        giveFileSystemWatcherTimeToDetectChanges()

        return folder
    }

    protected open fun renameTestFile(file: File): File {
        return rename(file, "NewName.txt")
    }

    protected open fun rename(fileOrFolder: File, newName: String = "NewName"): File {
        val destination = File(fileOrFolder.parent, newName)

        fileOrFolder.renameTo(destination)

        giveFileSystemWatcherTimeToDetectChanges()

        return destination
    }

    protected open fun modifyFile(file: File, newFileContent: String = "test2") {
        file.writeText(newFileContent)

        giveFileSystemWatcherTimeToDetectChanges()
    }

    protected open fun deleteFile(testFile: File) {
        testFile.delete()

        giveFileSystemWatcherTimeToDetectChanges()
    }

    protected open fun giveFileSystemWatcherTimeToDetectChanges() {
        Thread.sleep(250)
    }

    protected open fun subPath(fileOrFolder: File): String {
        return fileOrFolder.toRelativeString(this.testFolder)
    }

}