package net.dankito.sync.messenger

import fi.iki.elonen.NanoHTTPD
import net.dankito.sync.Endpoint
import net.dankito.sync.Response
import net.dankito.sync.file.GetFileForIdCallback
import net.dankito.sync.file.HttpServerFileSyncService.Companion.FilesUrlStart
import net.dankito.sync.file.IFileServer
import net.dankito.sync.messenger.http.*
import net.dankito.sync.messenger.http.HttpMethod
import net.dankito.utils.serialization.ISerializer
import net.dankito.utils.web.client.*
import java.io.BufferedInputStream
import java.io.DataInputStream
import java.io.FileInputStream


open class NanoHttpdMessenger(protected val client: IWebClient, urlToMessageBodyMapper: IUrlToMessageBodyMapper,
                              serializer: ISerializer, responseCreator: HttpResponseCreator)
    : HttpMessengerBase(urlToMessageBodyMapper, serializer, responseCreator), IFileServer {


    constructor(urlToMessageBodyMapper: IUrlToMessageBodyMapper, serializer: ISerializer, responseCreator: HttpResponseCreator)
            : this(OkHttpWebClient(), urlToMessageBodyMapper, serializer, responseCreator)

    constructor(urlToMessageBodyMapper: IUrlToMessageBodyMapper, serializer: ISerializer,
                responseCreator: (subUrl: String, method: HttpMethod, requestBody: Any?) -> HttpResponse?)
            : this(urlToMessageBodyMapper, serializer, object : HttpResponseCreator {

        override fun getResponse(subUrl: String, method: HttpMethod, requestBody: Any?): HttpResponse? {
            return responseCreator(subUrl, method, requestBody)
        }

    })


    protected var server: HttpServer? = null


    override var fileForIdCallback: GetFileForIdCallback? = null


    override fun start(port: Int): Boolean {
        server = HttpServer(port) { subUrl, method, receivedMessage ->
            handleRequest(subUrl, method, receivedMessage)
        }

        // TODO: set SSLServerSocketFactory before calling start()
        server?.start()

        return true
    }


    /*          Handling requests           */

    protected open fun handleRequest(subUrl: String, method: HttpMethod, receivedMessage: String?): NanoHTTPD.Response {
        if (subUrl.startsWith(FilesUrlStart)) {
            return respondToFileRequest(subUrl)
        }
        else if (subUrl.startsWith("/" + FilesUrlStart)) {
            return respondToFileRequest(subUrl.substring(1)) // remove leading '/'
        }

        return respondToMessageRequest(subUrl, method, receivedMessage)
    }

    protected open fun respondToMessageRequest(subUrl: String, method: HttpMethod, receivedMessage: String?): NanoHTTPD.Response {
        val responseToDispatch = determineResponse(subUrl, method, receivedMessage)

        if (responseToDispatch == CouldNotDetermineResponseResponse) {
            return NanoHTTPD.newFixedLengthResponse(NanoHTTPD.Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, "Not Found")
        }

        return NanoHTTPD.newFixedLengthResponse(NanoHTTPD.Response.Status.OK, "application/json", responseToDispatch)
    }

    protected open fun respondToFileRequest(uri: String): NanoHTTPD.Response {
        fileForIdCallback?.getFileForId(uri.substring(FilesUrlStart.length))?.let { file ->
            // TODO: determine file's MIME type
            return NanoHTTPD.newChunkedResponse(NanoHTTPD.Response.Status.OK, "", DataInputStream(BufferedInputStream(FileInputStream(file))))
        }

        return NanoHTTPD.newFixedLengthResponse(NanoHTTPD.Response.Status.NOT_FOUND, "", "") // TODO: provide error message
    }

    override fun stop(): Boolean {
        server?.stop()

        return true
    }


    /*          Sending requests to other clients           */

    override fun <TRequest : Any, TResponse> sendMessage(endpoint: Endpoint, message: MessageEnvelop<TRequest>,
                                                         responseClass: Class<TResponse>): Response<TResponse> {

        try {
            val response = getResponse(endpoint.address, endpoint.messengerPort, message)

            if (response.isSuccessful == false) {
                return Response(false, response.error)
            }

            response.body?.let { responseBody ->
                return Response(true, null, deserializeMessage(responseBody, responseClass))
            }
        } catch (e: Exception) {
            return Response(false, e)
        }

        return Response(false)
    }

    protected open fun <TRequest : Any> getResponse(address: String, port: Int, message: MessageEnvelop<TRequest>): WebClientResponse {

        var url = "http://$address:$port"
        var method = HttpMethod.Post
        var serializeBody = true

        if (message is HttpMessage) {
            if (message.subUrl.startsWith("/") == false) {
                url += "/"
            }
            url += message.subUrl
            method = message.method

            serializeBody = (message.body is HttpMessageBuilderBase.Bodyless) == false && message != HttpMethod.Get // currently OkHttpClient doesn't serialize body for Get requests
        }

        val contentType: String? = if (serializeBody == false) null else determineContentType(message)
        val requestParameters = RequestParameters(url, contentType = contentType)

        if (serializeBody) {
            requestParameters.body = serializeMessageToString(message.body)
        }

        when (method) {
            HttpMethod.Get -> return client.get(requestParameters)
            // TODO: implement PUT and DELETE
            else -> return client.post(requestParameters)
        }
    }

    protected open fun <TRequest : Any> determineContentType(message: MessageEnvelop<TRequest>): String? {
        when {
            serializer.isSerializingToJson() -> return ContentType.JSON
            serializer.isSerializingToXml() -> return ContentType.XML
            else -> return null
        }
    }

}