package net.dankito.sync.messenger

import fi.iki.elonen.NanoHTTPD
import net.dankito.sync.messenger.http.HttpMethod


open class HttpServer(port: Int, protected val messageReceivedListener: (url: String, method: HttpMethod, body: String?) -> Response)
    : NanoHTTPD(port) {

    override fun serve(uri: String, method: Method, headers: MutableMap<String, String>,
                       parms: MutableMap<String, String>, files: MutableMap<String, String>): Response {

        var body = files["postData"] // POST body // TODO: this is bad, it uses internal knowledge
        if (body == null) {
            body = parms["NanoHttpd.QUERY_STRING"] // TODO: this is bad, it uses internal knowledge
        }

        val subUrl = if (uri.startsWith("/")) uri.substring(1) else uri
        return messageReceivedListener(subUrl, mapMethod(method), body)
    }


    protected open fun mapMethod(method: Method): HttpMethod {
        when (method) {
            Method.POST -> return HttpMethod.Post
            else -> return HttpMethod.Get // TODO: add others when implemented in HttpMethod
        }
    }

}