package net.dankito.sync.messenger

import net.dankito.sync.Endpoint
import net.dankito.sync.Response
import net.dankito.sync.file.HttpServerFileSyncService
import net.dankito.sync.messenger.http.*
import net.dankito.utils.serialization.JacksonJsonSerializer
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Test
import java.io.File
import java.time.Duration
import java.time.Instant


class NanoHttpdMessengerTest {

    companion object {
        private val MessagesSubUrl = "messages"

        private val FirstMessengerPort = 54321
        private val SecondMessengerPort = 54322
        private val ThirdMessengerPort = 54323

        private val FirstMessengerEndpoint = Endpoint("127.0.0.1", FirstMessengerPort, FirstMessengerPort)
        private val SecondMessengerEndpoint = Endpoint("127.0.0.1", SecondMessengerPort, SecondMessengerPort)
        private val ThirdMessengerEndpoint = Endpoint("127.0.0.1", ThirdMessengerPort, ThirdMessengerPort)

        private val request = "Hello"
        private val response = "World"

        private val disturbingRequest = "Sorry to disturb you"
        private val disturbingResponse = "Don't mind"
    }


    private val serializer = JacksonJsonSerializer()

    private val urlMapper = UrlToMessageBodyMapperBase(
            mapOf(Pair(MessagesSubUrl, mapOf(Pair(HttpMethod.Post, String::class.java)))))

    private val messageBuilder = HttpMessageBuilderBase()

    private var first: NanoHttpdMessenger? = null

    private var second: NanoHttpdMessenger? = null

    private var third: NanoHttpdMessenger? = null


    @After
    fun tearDown() {
        first?.stop()
        second?.stop()
        third?.stop()
    }


    @Test
    fun sendMessageToSecondEndpoint_ReceivesResponse() {
        this.first = createAndStartMessenger(FirstMessengerPort)

        this.second = createAndStartMessenger(SecondMessengerPort) { message ->
            if (message == request) {
                return@createAndStartMessenger response
            }

            return@createAndStartMessenger null
        }


        // when
        val receivedResponse = first?.sendMessage(SecondMessengerEndpoint, createMessage(request), String::class.java)

        // then
        assertThat(receivedResponse?.isSuccessful).isTrue()
        assertThat(receivedResponse?.body).isEqualTo(response)
    }

    @Test
    fun sendMessageToThirdEndpoint_OnlyThirdEndpointReceivesResponse() {
        this.first = createAndStartMessenger(FirstMessengerPort)

        this.second = createAndStartMessenger(SecondMessengerPort)

        this.third = createAndStartMessenger(ThirdMessengerPort)  { message ->
            if (message == request) {
                return@createAndStartMessenger response
            }

            return@createAndStartMessenger disturbingResponse
        }


        // when
        var disturbingMessageResponse = Response<String>(false)
        second?.sendMessageAsync(ThirdMessengerEndpoint, createMessage(disturbingRequest), String::class.java) {
            disturbingMessageResponse = it
        }

        val receivedResponse = first?.sendMessage(ThirdMessengerEndpoint, createMessage(request), String::class.java)

        // then
        assertThat(receivedResponse?.isSuccessful).isTrue()
        assertThat(receivedResponse?.body).isEqualTo(response)

        assertThat(disturbingMessageResponse.body).isNotEqualTo(response)
    }


    @Test
    fun retrieveFile() {
        this.first = createAndStartMessenger(FirstMessengerPort)

        // TODO: add test file
        val testFile = File("/media/data/programs/DeepThought/files/videos/Nackt im Netz.mp4")
        first?.setGetFileForIdCallback { testFile }

        val start = Instant.now()
        val destinationFile = File.createTempFile("file_test_", ".part")

        val syncService = HttpServerFileSyncService()
        syncService.retrieveFile(FirstMessengerEndpoint, "", destinationFile)

        val duration = Duration.between(start, Instant.now())
        println("Copied ${destinationFile.length()} bytes to ${destinationFile.absolutePath} in " +
                "${duration.toMillis()} millis")
    }

    private fun createMessenger(): NanoHttpdMessenger {
        return createMessenger { }
    }

    private fun createMessenger(listener: (Any) -> Any?): NanoHttpdMessenger {
        return NanoHttpdMessenger(urlMapper, serializer) { _, _, requestBody ->
            requestBody?.let {
                val responseBody = listener(requestBody)
                return@NanoHttpdMessenger HttpResponse(HttpResponseStatus.OK, responseBody)
            }

            null
        }
    }

    private fun createAndStartMessenger(port: Int): NanoHttpdMessenger {
        return createAndStartMessenger(port) { }
    }

    private fun createAndStartMessenger(port: Int, listener: (Any) -> Any?): NanoHttpdMessenger {
        val messenger = createMessenger(listener)

        messenger.start(port)

        return messenger
    }

    private fun createMessage(body: String): HttpMessage<String> {
        return messageBuilder.getStringBodyMessage(HttpMethod.Post, MessagesSubUrl, body)
    }

}