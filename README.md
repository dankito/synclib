
## Device Discovery

<table>
    <tr>
        <td></td>
        <th>jmDNS</th>
        <th>Custom UDP implementation</td>
    </tr>
    <tr>
        <td>Advantages</td>
        <td>
            <ul>
                <li>Uses standardized protocol</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Fast</li>
                <li>Takes only a few hundred lines of code</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Disadvantages</td>
        <td>
            <ul>
                <li>Slow, takes up to 9 seconds to discover a device</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Custom, non standardized protocol</li>
            </ul>
        </td>
    </tr>
</table>

## Messaging

<table>
    <tr>
        <td></td>
        <th>JeroMQ (ZeroMQ)</th>
        <th>NanoHTTPD</td>
    </tr>
    <tr>
        <td>Advantages</td>
        <td>
            <ul>
                <li>Very fast <br>(but high speed here is not actually a criteria)</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Easy implementation</li>
                <li>Small code footprint</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Disadvantages</td>
        <td>
            <ul>
                <li>
                    Ich habe es nicht hinbekommen, dass man genau an den Sender einer Nachricht antworten kann.
                    <ul>
                        <li>REP & REQ: Müsste auf Empfängerseite so lange blockieren (= können keine weiteren Nachrichten bearbeitet werden) bis Antwort gesendet wurde (kann z. B. beim PIN eingeben sehr lange dauern)</li>
                        <li>Exclusive Pair: Es muss für jeden verbundenen Client eine eigene Verbindung aufgemacht und ein weiterer Port geöffnet werden.</li>
                    </ul>
                </li>
            </ul>
        </td>
        <td>
            <ul>
                <li>HTTP overhead</li>
                <li>Quite slow</li>
            </ul>
        </td>
    </tr>
</table>

## File synchronization

According to this benchmark Java NIO should be the fastest technology: https://baptiste-wicht.com/posts/2010/08/file-copy-in-java-benchmark.html.

And this article explains that the preferred way should be the NIO zero copy approach: https://www.ibm.com/developerworks/library/j-zerocopy/.

However in my tests all approaches were almost equally fast (or slow).<br />
This is may due to that network latency is way greater than any performance improvements for writing files.<br />
The only thing I wonder is that NIO zero copy didn't show any faster results.


<table>
    <tr>
        <td></td>
        <th>JeroMQ</th>
        <th>NanoHTTPD Http Stream</td>
        <th>Java Stream</th>
        <th>Java NIO</th>
        <th>Java NIO zero copy</th>
    </tr>
    <tr>
        <td>Speed: Wifi / LAN</td>
        <td><center>not tested</center></td>
        <td><center>3.15 MB/s / 10.6 MB/s</center></td>
        <td><center>3.2 MB/s / 10.7 MB/s</center></td>
        <td><center>3.1 MB/s / 10.6 MB/s</center></td>
        <td><center>1.6 MB/s / 10.7 MB/s</center></td>
    </tr>
    <tr>
        <td>Advantages</td>
        <td></td>
        <td>
            <ul>
                <li>Only few additional code needed on top of NanoHTTPD messenger</li>
                <li>Same port for messenger and file synchronization can be used</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Fast</li>
                <li>Pure JDK methods / small code footprint</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Fast, by theory also faster than Java Streams</li>
                <li>Pure JDK methods / small code footprint</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>No need to copy network data from kernel space to user space and from user space to kernel space to write to file</li>
                <li>Should be the fastest option</li>
                <li>Pure JDK methods / small code footprint</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Disadvantages</td>
        <td>
            <ul>
                <li>Need to implement custom protocol for splitting file into chunks and telling receiver when all chunks have been sent.<br>May test FileMQ (https://github.com/zeromq/filemq)</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Quite slow</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Needs an extra port</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Needs an extra port</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Needs an extra port</li>
                <li>Reserves memory as large as file's size (why, Java developers, why?) and therefore crashes Android with OutOfMemoryException for larger files</li>
            </ul>
        </td>
    </tr>
</table>

## Security / encryption

For message security I suggest using asymmetric keys with self generated public and private keys and digital signatures for smaller messages.
Keys could be exchanged with Diffie-Hellman.

For larger messages like file transfers I suggest symmetric keys, a new one for each message, exchanged via above facilities.

For transport security I suggest using TLS 1.3.

To evaluate:
* Use RSA or elliptic curve for key and signature generation?
* If elliptic curve: Use explicit parameters or names curves? http://www.bouncycastle.org/wiki/display/JA1/Elliptic+Curve+Key+Pair+Generation+and+Key+Factories

<table>
    <tr>
        <td></td>
        <th>RSA</th>
        <th>Elliptic curve</td>
    </tr>
    <tr>
        <td>Advantages</td>
        <td>
            <ul>
                <li>Fast encryption and verification</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Smaller keys, ciphertexts and signatures</li>
                <li>Fast key generation, encryption, decryption and signatures</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Disadvantages</td>
        <td>
            <ul>
                <li>Slow key generation</li>
                <li>Slow decryption and signing</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Signing with a broken random number generator compromises the key</li>
                <li>At least DUAL_EC_DRBG has a NSA backdoor, Intel CPU's 'random' number generators is predictive for NSA</li>
                <li>But if newer Elliptic curve has this known NSA backdoors, why should the older RSA have no unknown ones?</li>
            </ul>
        </td>
    </tr>
</table>

Sources:
* https://www.quora.com/Which-one-is-better-elliptic-curve-cryptography-or-RSA-algorithm-and-why?share=1
* https://bits.blogs.nytimes.com/2013/09/10/government-announces-steps-to-restore-confidence-on-encryption-standards/

### To evaluate

* How and where to store private keys?
* How to realize key revocation e.g. if a private key is compromised?
* How to ensure that the exchanged public key is really the other user's one (seems to be unsolvable right now)


## Cipher providers

As TLS 1.3 is only available from Java 11 and therefore not usable on Android, a non-JDK JCE provider has to be used.

<table>
    <tr>
        <td></td>
        <th>BouncyCastle (SpongyCastle)</th>
        <th>libsodium-jni</td>
        <th>Google Conscrypt (based on BoringSSL)</th>
    </tr>
    <tr>
        <td>Advantages</td>
        <td>
            <ul>
                <li>Independent developers</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Independent developers</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>TLS 1.3</li>
                <li>Up to date, amazing list of capabilities:<br><a href="https://github.com/google/conscrypt/blob/master/CAPABILITIES.md">https://github.com/google/conscrypt/blob/master/CAPABILITIES.md</a></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>Disadvantages</td>
        <td>
            <ul>
                <li>A bit outdated, no TLS 1.3</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>How fast is it with JNI?</li>
                <li>There seems to be no implementation of TLS 1.3</li>
            </ul>
        </td>
        <td>
            <ul>
                <li>Can you trust Google?</li>
            </ul>
        </td>
    </tr>
</table>

Also to evaluate: [Tink](https://github.com/google/tink), a Google library to fix many encryption based weaknesses and which only takes a few lines of code to encrypt data (https://github.com/google/tink/blob/master/docs/JAVA-HOWTO.md).
Only helps to encrypt data, it's not a JCE.

## To evaluate:

- Proxy:
    - Little Proxy: https://github.com/adamfisk/LittleProxy
- Port unification (using one port for multiple protocols; runs hopefully also on Android):
https://javaee.github.io/grizzly/portunification.html
- Web server:
    - Tiny Java Web Server: http://tjws.sourceforge.net/