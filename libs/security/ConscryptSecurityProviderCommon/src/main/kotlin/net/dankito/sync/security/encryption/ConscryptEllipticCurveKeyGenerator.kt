package net.dankito.sync.security.encryption

import java.security.SecureRandom


class ConscryptEllipticCurveKeyGenerator : EllipticCurveKeyGenerator() {

    override fun getKeyPairAlgorithm(): String {
        return "EC"
    }

    override fun getEclipticCurveName(): String {
        // supported values for Conscrypt:
        // - secp224r1
        // - prime256v1 (aka secp256r1)
        // - secp384r1
        // - secp521r1

        return "prime256v1"
    }

    override fun generateSecureRandom(): SecureRandom {
        return SecureRandom.getInstance("SHA1PRNG")
    }

}