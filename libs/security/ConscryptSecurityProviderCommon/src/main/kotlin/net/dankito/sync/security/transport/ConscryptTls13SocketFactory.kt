package net.dankito.sync.security.transport

import java.net.ServerSocket
import java.net.Socket
import java.security.KeyPair
import java.security.Provider
import java.security.PublicKey
import java.security.SecureRandom
import javax.net.ssl.*


open class ConscryptTls13SocketFactory(protected val provider: Provider) : ISecureSocketFactory {

    override fun generateTcpSocket(address: String, port: Int): Socket {
        return generateTcpSocket(address, port, null, null)
    }

    override fun generateTcpSocket(address: String, port: Int, ownKeys: List<KeyPair>?, trustedKeys: List<PublicKey>?): Socket {
        val socketFactory = createSslContext(ownKeys, trustedKeys).socketFactory
        val socket = socketFactory.createSocket(address, port)

        (socket as SSLSocket).enabledProtocols = arrayOf(getTlsVersion())
        socket.needClientAuth = true

        return socket
    }

    override fun generateTcpServerSocket(port: Int): ServerSocket {
        return generateTcpServerSocket(port, null, null)
    }

    override fun generateTcpServerSocket(port: Int, ownKeys: List<KeyPair>?, trustedKeys: List<PublicKey>?): ServerSocket {
        val serverSocketFactory = createSslContext(ownKeys, trustedKeys).serverSocketFactory

        val serverSocket = serverSocketFactory.createServerSocket(port)
        (serverSocket as SSLServerSocket).enabledProtocols = arrayOf(getTlsVersion())
        serverSocket.needClientAuth = true

        // TODO: set supported cipher suites

        return serverSocket
    }


    protected open fun createSslContext(ownKeys: List<KeyPair>?, trustedKeys: List<PublicKey>?): SSLContext {
        val sslContext = SSLContext.getInstance(getTlsVersion(), provider)

        sslContext.init(createKeyManagers(ownKeys), createTrustManagers(trustedKeys), generateSecureRandom())

        return sslContext
    }

    protected open fun getTlsVersion(): String {
        return "TLSv1.3"
    }

    protected open fun createKeyManagers(ownKeys: List<KeyPair>?): Array<out KeyManager>? {
        ownKeys?.let {
            return ownKeys.map { key -> SelfSignedCertificateKeyManager(key) }.toTypedArray()
        }

        return null
    }

    protected open fun createTrustManagers(trustedKeys: List<PublicKey>?): Array<out TrustManager>? {
        trustedKeys?.let {
            return arrayOf(AllowedKeysTrustManager(it))
        }

        return null
    }

    protected open fun generateSecureRandom(): SecureRandom {
        return SecureRandom.getInstance("SHA1PRNG")
    }

}