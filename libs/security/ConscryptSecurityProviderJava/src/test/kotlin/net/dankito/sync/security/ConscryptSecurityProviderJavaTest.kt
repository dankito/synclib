package net.dankito.sync.security

import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert
import org.junit.Test
import java.net.Socket
import java.util.concurrent.CountDownLatch
import kotlin.concurrent.thread


class ConscryptSecurityProviderJavaTest {

    companion object {
        const val CommunicationSocket = 23456
    }

    private val underTest = ConscryptSecurityProviderJava()


    @Test
    fun install() {

        // given
        val keyGenerator = underTest.keyGenerator

        try {
            keyGenerator.generatePublicPrivateKeyPair() // should not succeed
            Assert.fail("Should not be able to generate Elliptic Curve public-private key pair without Conscrypt")
        } catch (e: Exception) {
            // expected
        }

        // when
        underTest.install()

        // then
        // TODO: this will actually also succeed if underTest.install() will not be called
        assertThat(keyGenerator.generatePublicPrivateKeyPair(underTest.provider)).isNotNull()
    }


    @Test
    fun testTls13Communication() {

        // given
        val testMessage = "Can you hear me?"
        val testResponse = "Of course, dummy!"

        val serverKeys = underTest.keyGenerator.generatePublicPrivateKeyPair(underTest.provider)
        val clientKeys = underTest.keyGenerator.generatePublicPrivateKeyPair(underTest.provider)

        val serverSocket = underTest.socketFactory.generateTcpServerSocket(CommunicationSocket, listOf(serverKeys), listOf(clientKeys.public))
        val clientSocket = underTest.socketFactory.generateTcpSocket("127.0.0.1", CommunicationSocket, listOf(clientKeys), listOf(serverKeys.public))

        val waitTillServerStartedLatch = CountDownLatch(1)


        // when
        thread {
            waitTillServerStartedLatch.countDown()
            val connectedClient = serverSocket.accept()
            val receivedMessage = connectedClient.getInputStream().bufferedReader().readLine()

            if (receivedMessage == testMessage) {
                writeLineToSocket(connectedClient, testResponse)
            }

            connectedClient.close()
        }

        waitTillServerStartedLatch.await()
        writeLineToSocket(clientSocket, testMessage)

        val receivedResponse = clientSocket.getInputStream().bufferedReader().readLine()


        // then
        assertThat(receivedResponse).isEqualTo(testResponse)
    }

    private fun writeLineToSocket(clientSocket: Socket, message: String) {
        val writer = clientSocket.getOutputStream().bufferedWriter()

        writer.write(message + "\n")

        writer.flush()
    }

}