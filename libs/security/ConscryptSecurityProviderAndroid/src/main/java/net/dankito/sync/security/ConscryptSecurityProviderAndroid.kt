package net.dankito.sync.security

import net.dankito.sync.security.encryption.ConscryptEllipticCurveKeyGenerator
import net.dankito.sync.security.transport.ConscryptTls13SocketFactory
import org.conscrypt.Conscrypt
import java.security.Security


class ConscryptSecurityProviderAndroid: ISecurityProvider {

    override val provider = Conscrypt.newProvider()

    override val keyGenerator = ConscryptEllipticCurveKeyGenerator()

    override val socketFactory = ConscryptTls13SocketFactory(provider)


    override fun install() {
        Security.addProvider(provider)
    }

}