package net.dankito.sync.security.transport

import java.security.PublicKey
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.X509TrustManager


// TODO: this should actually be two classes, one for clients where only checkServerTrusted() is implemented and one for server instances where only checkClientTrusted() is implemented
open class AllowedKeysTrustManager(protected val trustedKeys: List<PublicKey>) : X509TrustManager {

    @Throws(CertificateException::class)
    override fun checkClientTrusted(chain: Array<out X509Certificate>, authType: String) {
        throwExceptionIfNotTrusted(chain)
    }

    @Throws(CertificateException::class)
    override fun checkServerTrusted(chain: Array<out X509Certificate>, authType: String) {
        throwExceptionIfNotTrusted(chain)
    }

    override fun getAcceptedIssuers(): Array<X509Certificate> {
        return arrayOf() // TODO: implement
    }


    protected open fun throwExceptionIfNotTrusted(chain: Array<out X509Certificate>) {
        if (isTrusted(trustedKeys, chain) == false) {
            throw CertificateException("No trusted key found for change")
        }
    }

    protected open fun isTrusted(trustedKeys: List<PublicKey>, chain: Array<out X509Certificate>): Boolean {
        trustedKeys.forEach { trustedKey ->
            chain.forEach { certificate ->
                if (trustedKey == certificate.publicKey) { // TODO: also check subject and issuer
                    return true
                }
            }
        }

        return false
    }

}