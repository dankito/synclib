package net.dankito.sync.security

import net.dankito.sync.security.encryption.IAsymmetricKeyGenerator
import net.dankito.sync.security.transport.ISocketFactory
import java.security.Provider


interface ISecurityProvider {

    val provider: Provider?

    val keyGenerator: IAsymmetricKeyGenerator

    val socketFactory: ISocketFactory


    fun install()

}