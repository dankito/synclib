package net.dankito.sync.security.transport

import org.bouncycastle.asn1.x500.X500Name
import java.net.Socket
import java.security.KeyPair
import java.security.Principal
import java.security.PrivateKey
import java.security.cert.X509Certificate
import javax.net.ssl.X509KeyManager


open class SelfSignedCertificateKeyManager(protected val key: KeyPair) : X509KeyManager {

    protected val keyAlias = key.private.algorithm


    override fun getPrivateKey(alias: String): PrivateKey? {
        if (alias == keyAlias) {
            return key.private
        }

        return null
    }

    override fun getCertificateChain(alias: String): Array<X509Certificate>? {
        if (alias == keyAlias) {
            return generateSelfSignedCertificate(key)
        }

        return null
    }

    private fun generateSelfSignedCertificate(keyPair: KeyPair): Array<X509Certificate>? {
        // TODO: set X500Name and may also hostname
        return arrayOf(SelfSignedCertificateGenerator().generateCertificateFromKeyPair(keyPair, X500Name("CN="), null) as X509Certificate)
    }

    override fun getServerAliases(keyType: String?, issuers: Array<out Principal>?): Array<String> {
        return arrayOf(keyAlias)
    }

    override fun getClientAliases(keyType: String?, issuers: Array<out Principal>?): Array<String> {
        return arrayOf(keyAlias)
    }

    override fun chooseServerAlias(keyType: String?, issuers: Array<out Principal>?, socket: Socket): String {
        return keyAlias
    }

    override fun chooseClientAlias(keyType: Array<out String>?, issuers: Array<out Principal>?, socket: Socket): String {
        return keyAlias
    }

}