package net.dankito.sync.security.transport

import org.bouncycastle.asn1.ASN1Sequence
import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x509.*
import org.bouncycastle.cert.X509v3CertificateBuilder
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder
import java.math.BigInteger
import java.security.KeyPair
import java.security.SecureRandom
import java.security.cert.Certificate
import java.util.*



class SelfSignedCertificateGenerator {

    private val DEFAULT_SIG_ALG = "SHA1WITHECDSA" // TODO: adjust this, see DefaultSignatureAlgorithmIdentifierFinder for possible values


    fun generateCertificateFromKeyPair(keyPair: KeyPair, subject: X500Name, hostname: String?): Certificate {

        val rand = SecureRandom() // TODO: use Conscrypt's SecureRandom?
        val sigGen = JcaContentSignerBuilder(DEFAULT_SIG_ALG).build(keyPair.private)

        val subPubKeyInfo = SubjectPublicKeyInfo(ASN1Sequence.getInstance(keyPair.public.encoded))

        val now = Date() // now

        /* force it to use a English/Gregorian dates for the cert, hardly anyone
           ever looks at the cert metadata anyway, and its very likely that they
           understand English/Gregorian dates */
        val calendar = GregorianCalendar(Locale.ENGLISH)
        calendar.setTime(now)
        calendar.add(Calendar.YEAR, 1)
        val startTime = Time(now, Locale.ENGLISH)
        val endTime = Time(calendar.getTime(), Locale.ENGLISH) // TODO: set endTime

        val v3CertGen = X509v3CertificateBuilder(
                subject,
                BigInteger.valueOf(rand.nextLong()),
                startTime,
                endTime,
                subject,
                subPubKeyInfo)

        if (hostname != null) {
            val subjectAltName = GeneralNames(GeneralName(GeneralName.iPAddress, hostname))
            v3CertGen.addExtension(X509Extension.subjectAlternativeName, false, subjectAltName)
        }

        val certHolder = v3CertGen.build(sigGen)
        return JcaX509CertificateConverter().getCertificate(certHolder)
    }

}