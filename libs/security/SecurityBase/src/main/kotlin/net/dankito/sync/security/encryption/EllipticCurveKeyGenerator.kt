package net.dankito.sync.security.encryption

import java.security.KeyPair
import java.security.KeyPairGenerator
import java.security.Provider
import java.security.SecureRandom
import java.security.spec.AlgorithmParameterSpec
import java.security.spec.ECGenParameterSpec


open class EllipticCurveKeyGenerator : IAsymmetricKeyGenerator {

    override fun generatePublicPrivateKeyPair(): KeyPair {
        return generatePublicPrivateKeyPair(null, getParameterSpec())
    }

    override fun generatePublicPrivateKeyPair(provider: Provider?): KeyPair {
        return generatePublicPrivateKeyPair(provider, getParameterSpec())
    }

    // example code taken from http://www.bouncycastle.org/wiki/display/JA1/Elliptic+Curve+Key+Pair+Generation+and+Key+Factories
    override fun generatePublicPrivateKeyPair(provider: Provider?, parameterSpec: AlgorithmParameterSpec): KeyPair {
        val generator =
                if (provider != null) KeyPairGenerator.getInstance(getKeyPairAlgorithm(), provider)
                else KeyPairGenerator.getInstance(getKeyPairAlgorithm())

        generator.initialize(parameterSpec, generateSecureRandom())

        return generator.generateKeyPair()
    }


    protected open fun getKeyPairAlgorithm(): String {
        return "ECDSA"
    }

    protected open fun getParameterSpec(): AlgorithmParameterSpec {
        return ECGenParameterSpec(getEclipticCurveName())
    }

    protected open fun getEclipticCurveName(): String {
        return "prime192v1"
    }

    protected open fun generateSecureRandom(): SecureRandom {
        return SecureRandom()
    }

}