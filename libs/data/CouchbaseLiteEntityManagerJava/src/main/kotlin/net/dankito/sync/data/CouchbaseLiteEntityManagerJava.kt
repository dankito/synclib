package net.dankito.sync.data

import net.dankito.jpa.couchbaselite.CouchbaseLiteEntityManagerBase
import net.dankito.jpa.entitymanager.EntityManagerConfiguration


class CouchbaseLiteEntityManagerJava(configuration: EntityManagerConfiguration)
    : CouchbaseLiteEntityManagerBase(CouchbaseLiteJavaContext(configuration.dataFolder))
