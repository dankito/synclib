package net.dankito.sync.data

import net.dankito.sync.test.model.TestEntity
import net.dankito.sync.test.util.SynchronizedDevice
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Test
import java.util.*
import java.util.concurrent.TimeUnit

class CouchbaseLiteSyncManagerTest {

    companion object {
        private const val DatabaseName = "test_db"

        private const val TestEntityMessage = "Can you hear me?"

        private val TestEntityTimestamp = Date()

        private const val FirstMessengerPort = 54321
        private const val FirstSynchronizationPort = 54322
        private const val SecondMessengerPort = 54331
        private const val SecondSynchronizationPort = 54332
    }


    /*          First device            */

    private val first = SynchronizedDevice(DatabaseName, FirstMessengerPort, FirstSynchronizationPort)


    /*          Second device            */

    private val second = SynchronizedDevice(DatabaseName, SecondMessengerPort, SecondSynchronizationPort)


    @After
    fun tearDown() {
        first.shutDown()

        second.shutDown()
    }


    @Test
    fun syncData() {
        // given
        second.startSynchronizationWith(first)

        val testEntity = TestEntity(TestEntityMessage, TestEntityTimestamp)


        // when
        second.entityManager.persistEntity(testEntity)

        // then
        waitForEntitySynchronization()
        val synchronizedEntity = first.entityManager.getEntityById(TestEntity::class.java, testEntity.id!!)

        assertThat(synchronizedEntity).isNotNull
        assertThat(testEntity === synchronizedEntity).isFalse()

        assertThat(synchronizedEntity?.message).isEqualTo(testEntity.message)
        assertThat(synchronizedEntity?.timestamp).isEqualTo(testEntity.timestamp)
    }


    private fun waitForEntitySynchronization() {
        TimeUnit.SECONDS.sleep(10)
    }

}