package net.dankito.sync.test.util

import net.dankito.jpa.entitymanager.EntityManagerConfiguration
import net.dankito.sync.data.CouchbaseLiteEntityManagerJava
import net.dankito.sync.data.CouchbaseLiteSyncManager
import net.dankito.sync.data.SyncManagerConfig
import net.dankito.sync.data.changeshandler.EntitySynchronizedEvent
import net.dankito.sync.data.changeshandler.PostToEventBusSynchronizedChangesHandler
import net.dankito.sync.discovery.DiscoveredDevice
import net.dankito.sync.proxy.ReverseProxy
import net.dankito.sync.proxy.requestrouter.DataSyncRequestRouter
import net.dankito.sync.test.model.TestEntity
import net.dankito.utils.events.RxEventBus
import net.dankito.utils.io.FileUtils
import java.io.File


class SynchronizedDevice(val databaseName: String, val messengerPort: Int, val synchronizationPort: Int) {

    companion object {
        private var DeviceIndex = 0
    }


    val deviceName = "SynchronizedDevice${DeviceIndex++}"

    val dataFolder = File("test_data", deviceName)

    val entityManagerConfiguration = EntityManagerConfiguration(dataFolder.path, databaseName)

    val entityManager = CouchbaseLiteEntityManagerJava(entityManagerConfiguration)

    val synchronizedData = mutableListOf<Any>()

//    val synchronizedChangesHandler = object : ISynchronizedChangesHandler {
//
//        override fun handleChange(event: Database.ChangeEvent) {
//            synchronizedData.add(event)
//        }
//
//    }

    val eventBus = RxEventBus()

    val synchronizedChangesHandler = PostToEventBusSynchronizedChangesHandler(entityManager, eventBus)


    val proxy = ReverseProxy()

    val device = DiscoveredDevice("127.0.0.1", messengerPort, deviceName)

    val synchronizer: CouchbaseLiteSyncManager


    init {
        eventBus.subscribe(EntitySynchronizedEvent::class.java) {
            synchronizedData.add(it.entity as TestEntity)
        }

        proxy.addRouter(DataSyncRequestRouter(databaseName, synchronizationPort))
        proxy.start(messengerPort)
        entityManager.open(entityManagerConfiguration)

        synchronizer = CouchbaseLiteSyncManager(entityManager, SyncManagerConfig(false, true,
                synchronizedChangesHandler, messengerPort))
        synchronizer.openSynchronizationPort(synchronizationPort)
    }


    fun shutDown() {
        synchronizer.closeSynchronizationPort()
        entityManager.close()
        proxy.close()

        FileUtils().deleteFolderRecursively(dataFolder)
    }


    fun startSynchronizationWith(device: SynchronizedDevice) {
        synchronizer.startSynchronizationWithDevice(device.device)
    }

}