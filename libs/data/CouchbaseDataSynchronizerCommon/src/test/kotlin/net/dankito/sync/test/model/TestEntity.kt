package net.dankito.sync.test.model

import java.util.*
import javax.persistence.*


@Entity
class TestEntity(

        @Column
        val message: String,

        @Temporal(TemporalType.TIMESTAMP)
        @Column
        val timestamp: Date

) {

    internal constructor() : this("", Date(0)) // for object deserializers


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    var id: String? = null

}