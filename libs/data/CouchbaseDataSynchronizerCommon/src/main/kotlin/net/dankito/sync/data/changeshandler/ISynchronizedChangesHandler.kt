package net.dankito.sync.data.changeshandler

import com.couchbase.lite.Database


interface ISynchronizedChangesHandler {

    fun handleChange(event: Database.ChangeEvent)

}