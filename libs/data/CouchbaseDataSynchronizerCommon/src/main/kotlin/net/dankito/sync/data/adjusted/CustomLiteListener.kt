package net.dankito.sync.data.adjusted

import Acme.Serve.Serve
import com.couchbase.lite.Manager
import com.couchbase.lite.listener.Credentials
import com.couchbase.lite.listener.LiteListener
import org.slf4j.LoggerFactory
import java.util.*


class CustomLiteListener(manager: Manager, suggestedPort: Int, allowedCredentials: Credentials?, tjwsProperties: Properties = Properties())
    : LiteListener(manager, suggestedPort, allowedCredentials, tjwsProperties) {

    companion object {
        private val log = LoggerFactory.getLogger(CustomLiteListener::class.java)
    }


    private val httpServer: Serve


    init {
        // this is bad. It uses a) application internal details and b) reflection.

        // copied from LiteListener constructor
        val httpServer = CustomLiteServer() // this is the only changed line
        httpServer.setProps(tjwsProperties)
        httpServer.setManager(manager)
        httpServer.setListener(this)
        httpServer.setPort(this.listenPort)
        httpServer.setAllowedCredentials(allowedCredentials)

        this.httpServer = httpServer
    }

    override fun run() {
        val serverStatus = httpServer.serve()

        try {
            val serverStatusField =  LiteListener::class.java.getDeclaredField("serverStatus")
            serverStatusField.isAccessible = true
            serverStatusField.set(this, serverStatus)
        } catch (e: Exception) {
            log.error("Could not set serverStatus", e)
        }
    }

    override fun stop() {
        httpServer.notifyStop()
    }

}