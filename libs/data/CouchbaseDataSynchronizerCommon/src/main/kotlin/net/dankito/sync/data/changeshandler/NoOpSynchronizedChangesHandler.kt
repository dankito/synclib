package net.dankito.sync.data.changeshandler

import com.couchbase.lite.Database


class NoOpSynchronizedChangesHandler : ISynchronizedChangesHandler {

    override fun handleChange(event: Database.ChangeEvent) {
    }

}