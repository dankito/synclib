package net.dankito.sync.data.adjusted

import com.couchbase.lite.listener.LiteServlet
import net.dankito.sync.proxy.ReverseProxy
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class CustomLiteServlet : LiteServlet() {

    override fun service(request: HttpServletRequest?, response: HttpServletResponse?) {
        if (request != null) {
            super.service(object : HttpServletRequestWrapper(request) {
                override fun getRemoteAddr(): String {
                    return adjustRemoteAddress(request)
                }
            }, response)
        }
        else {
            super.service(request, response)
        }
    }

    private fun adjustRemoteAddress(request: HttpServletRequest): String {
        if (request.remoteAddr == "127.0.0.1") {
            // fix that in LiteServlet.remoteURL() null gets returned if remoteAddr equals 127.0.0.1 - which will always be the case if using a proxy.
            // as a consequence source gets set to null and isExternal gets set to false on Database.ChangeEvent objects.
            // therefore if RemoteAddress header is set by proxy we return that address
            request.getHeader(ReverseProxy.RemoteAddressHeaderName)?.let { remoteAddress ->
                return remoteAddress
            }
        }

        return request.remoteAddr
    }

}