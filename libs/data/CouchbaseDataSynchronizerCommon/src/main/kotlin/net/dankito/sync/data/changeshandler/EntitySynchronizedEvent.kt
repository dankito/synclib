package net.dankito.sync.data.changeshandler


class EntitySynchronizedEvent<T>(val entity: T) {

    override fun toString(): String {
        return "Synchronized $entity"
    }

}