package net.dankito.sync.data.changeshandler

import com.couchbase.lite.Database
import net.dankito.jpa.couchbaselite.Dao
import net.dankito.jpa.entitymanager.IEntityManager
import net.dankito.utils.events.IEventBus
import org.slf4j.LoggerFactory


open class PostToEventBusSynchronizedChangesHandler(protected val entityManager: IEntityManager,
                                                    protected val eventBus: IEventBus) : ISynchronizedChangesHandler {

    companion object {
        private val log = LoggerFactory.getLogger(PostToEventBusSynchronizedChangesHandler::class.java)
    }


    override fun handleChange(event: Database.ChangeEvent) {
        event.changes.forEach { change ->
            try {
                val className = change.addedRevision.getPropertyForKey(Dao.TYPE_COLUMN_NAME).toString()
                val type = Class.forName(className)
                entityManager.getEntityById(type, change.documentId)?.let { entity ->
                    eventBus.post(EntitySynchronizedEvent(entity))
                }
            } catch (e: Exception) {
                log.error("Could not handle change $change", e)
            }
        }
    }

}