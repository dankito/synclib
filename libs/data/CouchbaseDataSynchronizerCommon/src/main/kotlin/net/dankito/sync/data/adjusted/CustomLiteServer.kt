package net.dankito.sync.data.adjusted

import Acme.Serve.Serve
import com.couchbase.lite.Manager
import com.couchbase.lite.listener.Credentials
import com.couchbase.lite.listener.LiteListener
import java.util.*


/**
 * Copied from LiteServer.java
 */
class CustomLiteServer : Serve() {

    val CBLServer_KEY = "CBLServerInternal"
    val CBL_URI_SCHEME = "cblite://"

    private var props: Properties? = Properties()
    private var manager: Manager? = null
    private var listener: LiteListener? = null

    // if this is non-null, then users must present BasicAuth
    // credential in every request which matches up with allowedCredentials,
    // or else the request will be refused.
    // REF: https://github.com/couchbase/couchbase-lite-java-listener/issues/35
    private var allowedCredentials: Credentials? = null


    fun setManager(manager: Manager) {
        this.manager = manager
    }

    fun setListener(listener: LiteListener) {
        this.listener = listener
    }

    fun setProps(properties: Properties?) {
        this.props = properties
    }

    fun setAllowedCredentials(allowedCredentials: Credentials?) {
        this.allowedCredentials = allowedCredentials
    }

    fun setPort(port: Int) {
        props!!["port"] = port
    }

    override fun serve(): Int {
        //pass our custom properties in
        this.arguments = props

        //pass in the CBLServerInternal to the servlet
        val servlet = CustomLiteServlet() // this is the only changed line
        servlet.setManager(manager)
        servlet.setListener(listener)
        if (allowedCredentials != null) {
            servlet.setAllowedCredentials(allowedCredentials)
        }

        this.addServlet("/", servlet)
        return super.serve()
    }

}