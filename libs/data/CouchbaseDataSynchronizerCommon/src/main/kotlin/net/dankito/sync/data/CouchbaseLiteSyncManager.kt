package net.dankito.sync.data

import com.couchbase.lite.Database
import com.couchbase.lite.listener.LiteListener
import com.couchbase.lite.replicator.Replication
import com.couchbase.lite.support.CouchbaseLiteHttpClientFactory
import net.dankito.jpa.couchbaselite.CouchbaseLiteEntityManagerBase
import net.dankito.jpa.couchbaselite.Dao
import net.dankito.sync.data.adjusted.CustomLiteListener
import net.dankito.sync.discovery.DiscoveredDevice
import okhttp3.OkHttpClient
import org.slf4j.LoggerFactory
import java.net.InetSocketAddress
import java.net.MalformedURLException
import java.net.Proxy
import java.net.URL
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit


class CouchbaseLiteSyncManager(private val entityManager: CouchbaseLiteEntityManagerBase,
                               /*private val networkSettings: INetworkSettings,*/ private val config: SyncManagerConfig) :
        IDataSynchronizer {

    companion object {
        val PortNotSet = -1

        private val SynchronizationDefaultPort = 27387

        private val EntitiesFilterName = "Entities_Filter"

        private val log = LoggerFactory.getLogger(CouchbaseLiteSyncManager::class.java)
    }


    private val database = entityManager.database
    private val manager = entityManager.manager

    private val synchronizedChangesHandler = config.synchronizedChangesHandler

    private var couchbaseLiteListener: LiteListener? = null
    private var synchronizationPort: Int = PortNotSet
    private var listenerThread: Thread? = null

    private var pushReplications: MutableMap<DiscoveredDevice, Replication> = ConcurrentHashMap()
    private var pullReplications: MutableMap<DiscoveredDevice, Replication> = ConcurrentHashMap()


    init {
        setReplicationFilter(database)
    }

    private fun setReplicationFilter(database: Database) {
        val entitiesToFilter = config.entitiesNotToSynchronize

        database.setFilter(EntitiesFilterName) { revision, params ->
            revision.getProperty(Dao.TYPE_COLUMN_NAME)?.let { entityType ->
                return@setFilter entitiesToFilter.contains(entityType) == false
            }
            true
        }
    }


    /*override*/ fun stop() {
        closeSynchronizationPort()

        stopSynchronizingWithAllDevices()
    }


    @Throws(Exception::class)
    /*override*/ fun openSynchronizationPort(desiredPort: Int): Int? {
        if(couchbaseLiteListener != null) { // listener already started
            return synchronizationPort
        }

        log.info("Starting Couchbase Lite Listener")

        // to fix that on Database.ChangeEvent objects source gets set to null and isExternal to false if remoteAddr is 127.0.0.1 (see LiteServlet.remoteURL())
        couchbaseLiteListener = CustomLiteListener(manager, desiredPort, null) // TODO: set allowedCredentials
        synchronizationPort = couchbaseLiteListener?.listenPort ?: PortNotSet

        if(synchronizationPort > 0 && synchronizationPort < 65536) {
//            networkSettings.synchronizationPort = synchronizationPort

            listenerThread = Thread(couchbaseLiteListener)
            listenerThread?.start()

            database.addChangeListener(databaseChangeListener)

            log.info("Listening now on synchronization port $synchronizationPort")
            return synchronizationPort
        }

        return null
    }

    /*override*/ fun closeSynchronizationPort() {
        log.info("Stopping Couchbase Lite Listener")

        couchbaseLiteListener?.stop()
        couchbaseLiteListener = null

        listenerThread?.let { listenerThread ->
            try { listenerThread.interrupt() } catch (ignored: Exception) { }
            this.listenerThread = null
        }

        synchronizationPort = PortNotSet
//        networkSettings.synchronizationPort = PortNotSet
    }


    @Throws(Exception::class)
    /*override*/ fun startSynchronizationWithDevice(device: DiscoveredDevice) {
        if(pullReplications.containsKey(device) || pushReplications.containsKey(device)) { // synchronization already started with this device
            return
        }

        // TODO: this is bad. Either pass synchronization port or throw an exception that listener isn't started yet
        openSynchronizationPort(SynchronizationDefaultPort) // as at this stage it may not be opened yet but is needed for synchronization

        log.info("Starting Replication with Device $device on ${device.address}:${device.synchronizationPort}")

        val syncUrl: URL
        try {
            syncUrl = createSyncUrl(device.address, device.synchronizationPort)
        } catch (e: MalformedURLException) {
            throw Exception(e)
        }

        synchronized(this) {
            startReplication(syncUrl, device)
        }
    }

    private fun startReplication(syncUrl: URL, device: DiscoveredDevice) {
        if (config.usePushReplication) {
            val pushReplication = startReplication(syncUrl, Replication.Direction.PUSH)

            pushReplications.put(device, pushReplication)
        }

        if (config.usePullReplication) {
            val pullReplication = startReplication(syncUrl, Replication.Direction.PULL)

            pullReplications.put(device, pullReplication)
        }
    }

    private fun startReplication(syncUrl: URL, replicationDirection: Replication.Direction): Replication {
        val replication = Replication(database, syncUrl, replicationDirection, httpClientFactory)

        replication.filter = EntitiesFilterName
        replication.isContinuous = true

        replication.start()

        return replication
    }

    private fun stopSynchronizingWithAllDevices() {
        for(device in ArrayList(pushReplications.keys)) {
            stopSynchronizationWithDevice(device)
        }
    }

    /*override*/ fun stopSynchronizationWithDevice(device: DiscoveredDevice) {
        synchronized(this) {
            log.info("Stopping Replication with Device " + device)

            pullReplications.remove(device)?.let { it.stop() }

            pushReplications.remove(device)?.let { it.stop() }

            // so this was the cause for the app crash: stopping listener modifies connections -> (ConcurrentModificationException in serve())
//            if(pushReplications.isEmpty()) { // no devices connected anymore
//                closeSynchronizationPort()
//            }
        }
    }


    private fun createSyncUrl(address: String, syncPort: Int): URL {
        return URL("http://" + address + ":" + syncPort + "/" + database.name)
    }


    private val databaseChangeListener = Database.ChangeListener { event ->
        if (event.isExternal) {
            synchronizedChangesHandler.handleChange(event)
        }
    }


    private val httpClientFactory = object : CouchbaseLiteHttpClientFactory(database.persistentCookieStore) {

        private var client: OkHttpClient? = null


        override fun getOkHttpClient(): OkHttpClient {
            client?.let { return it }

            // this is kind of bad as in this way a second OkHttpClient gets instantiated, but there's no other way to access parent's OkHttpClient.Builder's values
            val newClient = createFromParentOkHttpClient(super.getOkHttpClient())
            this.client = newClient

            return newClient
        }

        private fun createFromParentOkHttpClient(parentOkHttpClient: OkHttpClient): OkHttpClient {
            val builder = OkHttpClient.Builder()

            copyValuesFromParentOkHttpClient(builder, parentOkHttpClient)

            // fixes unexpected end of stream bug, see https://github.com/square/okhttp/issues/2738
            builder.retryOnConnectionFailure(true)

            config.proxyPort?.let { proxyPort ->
                builder.proxy(Proxy(Proxy.Type.HTTP, InetSocketAddress("127.0.0.1", proxyPort)))
            }

            return builder.build()
        }

        private fun copyValuesFromParentOkHttpClient(builder: OkHttpClient.Builder, parentOkHttpClient: OkHttpClient) {
            builder.connectTimeout(parentOkHttpClient.connectTimeoutMillis().toLong(), TimeUnit.MILLISECONDS)
                    .writeTimeout(parentOkHttpClient.writeTimeoutMillis().toLong(), TimeUnit.MILLISECONDS)
                    .readTimeout(parentOkHttpClient.readTimeoutMillis().toLong(), TimeUnit.MILLISECONDS)

            if(parentOkHttpClient.sslSocketFactory() != null) {
                builder.sslSocketFactory(parentOkHttpClient.sslSocketFactory())
            }

            if(parentOkHttpClient.hostnameVerifier() != null) {
                builder.hostnameVerifier(parentOkHttpClient.hostnameVerifier())
            }

            builder.cookieJar(parentOkHttpClient.cookieJar())

            builder.followRedirects(parentOkHttpClient.followRedirects())
        }
    }

}
