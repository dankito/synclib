package net.dankito.sync.data

import net.dankito.sync.data.changeshandler.ISynchronizedChangesHandler


class SyncManagerConfig(val usePullReplication: Boolean = true,
                        val usePushReplication: Boolean = false,
                        val synchronizedChangesHandler: ISynchronizedChangesHandler,
                        /**
                         * If SyncManager should communicate via proxy, specify its port here.
                         */
                        val proxyPort: Int? = null,
                        val entitiesNotToSynchronize: List<Class<*>> = listOf()
) {
}