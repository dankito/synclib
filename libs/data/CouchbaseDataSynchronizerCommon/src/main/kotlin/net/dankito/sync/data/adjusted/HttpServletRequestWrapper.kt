package net.dankito.sync.data.adjusted

import java.util.*
import javax.servlet.RequestDispatcher
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpSession


/**
 * A wrapper class around HttpServletRequest.
 *
 * So it makes it easy for you to derive from this class and adjust the values and behaviour to your liking.
 * (E.g. if you only want to override one field, than you don't have to do the hard work to override all of them many HttpServletRequest methods.)
 */
open class HttpServletRequestWrapper(protected val request: HttpServletRequest) : HttpServletRequest {

    override fun isUserInRole(p0: String?) = request.isUserInRole(p0)

    override fun getPathInfo() = request.pathInfo

    override fun getProtocol() = request.protocol

    override fun getCookies() = request.cookies

    override fun getParameterMap() = request.parameterMap

    override fun getRequestURL() = request.requestURL

    override fun getAttributeNames() = request.attributeNames

    override fun setCharacterEncoding(p0: String?) {
        request.characterEncoding = p0
    }

    override fun getParameterValues(p0: String?) = request.getParameterValues(p0)

    override fun getRemoteAddr() = request.remoteAddr

    override fun getLocales() = request.locales

    override fun getRealPath(p0: String?): String? {
        return request.getRealPath(p0)
    }

    override fun getContextPath() = request.contextPath

    override fun isRequestedSessionIdValid() = request.isRequestedSessionIdValid

    override fun getServerPort() = request.serverPort

    override fun getAttribute(p0: String?): Any? {
        return request.getAttribute(p0)
    }

    override fun getDateHeader(p0: String?): Long {
        return request.getDateHeader(p0)
    }

    override fun getRemoteHost() = request.remoteHost

    override fun getRequestedSessionId() = request.requestedSessionId

    override fun getServletPath() = request.servletPath

    override fun getSession(p0: Boolean): HttpSession? {
        return request.getSession(p0)
    }

    override fun getSession() = request.session

    override fun getServerName() = request.serverName

    override fun getLocalAddr() = request.localAddr

    override fun isSecure() = request.isSecure

    override fun isRequestedSessionIdFromCookie() = request.isRequestedSessionIdFromCookie

    override fun getRemoteUser() = request.remoteUser

    override fun getLocale() = request.locale

    override fun getMethod() = request.method

    override fun isRequestedSessionIdFromURL() = request.isRequestedSessionIdFromURL

    override fun getLocalPort() = request.localPort

    override fun isRequestedSessionIdFromUrl() = request.isRequestedSessionIdFromUrl

    override fun getQueryString() = request.queryString

    override fun getHeaders(p0: String?): Enumeration<*>? {
        return request.getHeaders(p0)
    }

    override fun getUserPrincipal() = request.userPrincipal

    override fun getReader() = request.reader

    override fun getScheme() = request.scheme

    override fun getInputStream() = request.inputStream

    override fun getLocalName() = request.localName

    override fun getAuthType() = request.authType

    override fun getCharacterEncoding() = request.characterEncoding

    override fun getParameterNames() = request.parameterNames

    override fun removeAttribute(p0: String?) {
        request.removeAttribute(p0)
    }

    override fun getPathTranslated() = request.pathTranslated

    override fun getContentLength() = request.contentLength

    override fun getHeader(p0: String?): String? {
        return request.getHeader(p0)
    }

    override fun getIntHeader(p0: String?): Int {
        return request.getIntHeader(p0)
    }

    override fun getContentType() = request.contentType

    override fun getRequestURI() = request.requestURI

    override fun getRequestDispatcher(p0: String?): RequestDispatcher? {
        return request.getRequestDispatcher(p0)
    }

    override fun getHeaderNames() = request.headerNames

    override fun setAttribute(p0: String?, p1: Any?) {
        request.setAttribute(p0, p1)
    }

    override fun getParameter(p0: String?): String {
        return request.getParameter(p0)
    }

    override fun getRemotePort() = request.remotePort
}