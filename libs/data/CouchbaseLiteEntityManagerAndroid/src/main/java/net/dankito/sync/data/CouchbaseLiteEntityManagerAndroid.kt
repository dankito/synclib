package net.dankito.sync.data

import com.couchbase.lite.Context
import com.couchbase.lite.android.AndroidContext
import net.dankito.jpa.couchbaselite.CouchbaseLiteEntityManagerBase
import net.dankito.jpa.entitymanager.EntityManagerConfiguration
import java.io.File


class CouchbaseLiteEntityManagerAndroid(androidContext: android.content.Context)
    : CouchbaseLiteEntityManagerBase(AndroidContext(androidContext)) {

    override fun adjustDatabasePath(context: Context, configuration: EntityManagerConfiguration): String {
        return File(this.context.getFilesDir(), configuration.databaseName).absolutePath
    }

}
