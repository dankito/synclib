package net.dankito.data_access.network.discovery


import net.dankito.sync.discovery.DiscoveredDevice
import net.dankito.sync.discovery.IDeviceDiscoverer
import net.dankito.sync.discovery.UdpDeviceDiscoverer
import net.dankito.sync.discovery.UdpDeviceDiscovererConfig
import net.dankito.utils.IThreadPool
import net.dankito.utils.ThreadPool
import net.dankito.utils.network.INetworkConnectivityManager
import net.dankito.utils.network.INetworkHelper
import net.dankito.utils.network.NetworkHelper
import net.dankito.utils.network.PeriodicCheckingNetworkConnectivityManager
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit


class UdpDeviceDiscovererTest {

    companion object {

        protected val DISCOVERY_PORT = 32788

        protected val CHECK_FOR_DEVICES_INTERVAL = 100

        protected val DISCOVERY_PREFIX = "UdpDeviceDiscovererTest"

        protected val FIRST_DISCOVERER_ID = "Gandhi"

        protected val SECOND_DISCOVERER_ID = "Mandela"
    }


    protected lateinit var firstDiscoverer: UdpDeviceDiscoverer

    protected lateinit var secondDiscoverer: UdpDeviceDiscoverer

    protected val networkHelper: INetworkHelper = NetworkHelper()

    protected val networkConnectivityManager: INetworkConnectivityManager = PeriodicCheckingNetworkConnectivityManager(networkHelper)

    protected val threadPool: IThreadPool = ThreadPool()

    protected var startedDiscoverers: MutableList<IDeviceDiscoverer> = CopyOnWriteArrayList<IDeviceDiscoverer>()


    @Before
    fun setUp() {
        firstDiscoverer = createDiscoverer()
        secondDiscoverer = createDiscoverer()
    }

    @After
    fun tearDown() {
        closeStartedDiscoverers()
    }

    protected fun closeStartedDiscoverers() {
        for (discoverer in startedDiscoverers) {
            discoverer.stop()
        }

        startedDiscoverers.clear()
    }


    @Test
    fun startTwoInstances_BothGetDiscovered() {
        val countDownLatch = CountDownLatch(2)

        val foundDevicesForFirstDevice = CopyOnWriteArrayList<DiscoveredDevice>()
        startFirstDiscoverer( { discoveredDevice ->
                foundDevicesForFirstDevice.add(discoveredDevice)
                countDownLatch.countDown()
            }, { disconnectedDevice ->
                foundDevicesForFirstDevice.remove(disconnectedDevice)
                countDownLatch.countDown()
            })

        val foundDevicesForSecondDevice = CopyOnWriteArrayList<DiscoveredDevice>()
        startSecondDiscoverer( { discoveredDevice ->
                foundDevicesForSecondDevice.add(discoveredDevice)
                countDownLatch.countDown()
            }, { disconnectedDevice ->
                foundDevicesForSecondDevice.add(disconnectedDevice)
                countDownLatch.countDown()
            })

        try {
            countDownLatch.await(3, TimeUnit.SECONDS)
        } catch (ignored: Exception) {
        }

        Assert.assertEquals(1, foundDevicesForFirstDevice.size)
        Assert.assertEquals(SECOND_DISCOVERER_ID, foundDevicesForFirstDevice[0].serviceDescription)

        Assert.assertEquals(1, foundDevicesForSecondDevice.size)
        Assert.assertEquals(FIRST_DISCOVERER_ID, foundDevicesForSecondDevice[0].serviceDescription)
    }

    @Test
    fun startElevenInstances_AllGetDiscovered() {
        val discoveredDevices = ConcurrentHashMap<String, MutableList<DiscoveredDevice>>()
        val createdDiscoverers = CopyOnWriteArrayList<IDeviceDiscoverer>()

        for (i in 0..10) {
            val discoverer = createDiscoverer()
            createdDiscoverers.add(discoverer)

            val serviceDescription = "" + (i + 1)
            discoveredDevices.put(serviceDescription, CopyOnWriteArrayList<DiscoveredDevice>())

            startDiscoverer(discoverer, serviceDescription, { discoveredDevice ->
                    val discoveredDevicesForDevice = discoveredDevices[serviceDescription]
                    discoveredDevicesForDevice?.add(discoveredDevice)
                }, { disconnectedDevice ->
                    val discoveredDevicesForDevice = discoveredDevices[serviceDescription]
                    discoveredDevicesForDevice?.remove(disconnectedDevice)
                })
        }

        try {
            Thread.sleep(3000)
        } catch (ignored: Exception) {
        }

        for (serviceDescription in discoveredDevices.keys) {
            val discoveredDevicesForDevice = discoveredDevices[serviceDescription]
            Assert.assertEquals(10, discoveredDevicesForDevice?.size)
            Assert.assertNull(discoveredDevicesForDevice?.filter { it.serviceDescription == serviceDescription }?.firstOrNull())
        }
    }


    @Test
    fun startTwoInstances_DisconnectOne() {
        val countDownLatch = CountDownLatch(3)

        val foundDevicesForFirstDevice = CopyOnWriteArrayList<DiscoveredDevice>()
        startFirstDiscoverer( { discoveredDevice ->
                foundDevicesForFirstDevice.add(discoveredDevice)
                countDownLatch.countDown()
            }, { disconnectedDevice ->
                foundDevicesForFirstDevice.remove(disconnectedDevice)
                countDownLatch.countDown()
            })

        val foundDevicesForSecondDevice = CopyOnWriteArrayList<DiscoveredDevice>()
        startSecondDiscoverer( { discoveredDevice ->
                foundDevicesForSecondDevice.add(discoveredDevice)
                countDownLatch.countDown()
            }, { disconnectedDevice ->
                foundDevicesForSecondDevice.remove(disconnectedDevice)
                countDownLatch.countDown()
            })

        try { Thread.sleep(3000) } catch (ignored: Exception) { } // so that they have enough time to discover each other

        firstDiscoverer.stop()

        try { countDownLatch.await(3, TimeUnit.SECONDS) } catch (ignored: Exception) { }

        Assert.assertEquals(1, foundDevicesForFirstDevice.size)
        Assert.assertEquals(SECOND_DISCOVERER_ID, foundDevicesForFirstDevice[0].serviceDescription)

        Assert.assertEquals(0, foundDevicesForSecondDevice.size)
    }

    @Test
    fun startElevenInstances_FiveGetDisconnected() {
        val discoveredDevices = ConcurrentHashMap<IDeviceDiscoverer, MutableList<DiscoveredDevice>>()
        val createdDiscoverers = CopyOnWriteArrayList<IDeviceDiscoverer>()

        for (i in 0..10) {
            val discoverer = createDiscoverer()
            createdDiscoverers.add(discoverer)
            discoveredDevices.put(discoverer, CopyOnWriteArrayList<DiscoveredDevice>())

            startDiscoverer(discoverer, "" + (i + 1), { discoveredDevice ->
                val discoveredDevicesForDevice = discoveredDevices[discoverer]
                discoveredDevicesForDevice?.add(discoveredDevice)
            }, { discoveredDevice ->
                val discoveredDevicesForDevice = discoveredDevices[discoverer]
                discoveredDevicesForDevice?.remove(discoveredDevice)
            })
        }

        try {
            Thread.sleep(500)
        } catch (ignored: Exception) {
        }

        for (i in 0..4) {
            val discoverer = createdDiscoverers[i]
            discoveredDevices.remove(discoverer)

            discoverer.stop()
        }

        try {
            Thread.sleep(5000)
        } catch (ignored: Exception) {
        }

        for (discoverer in discoveredDevices.keys) {
            val discoveredDevicesForDevice = discoveredDevices[discoverer]
            Assert.assertEquals(5, discoveredDevicesForDevice?.size)
        }
    }


    protected fun createDiscoverer() = UdpDeviceDiscoverer(networkConnectivityManager, threadPool, networkHelper)

    protected fun startFirstDiscoverer(deviceDiscoveredListener: (DiscoveredDevice) -> Unit,
                                       deviceDisconnectedListener: (DiscoveredDevice) -> Unit) {

        startDiscoverer(firstDiscoverer, FIRST_DISCOVERER_ID, deviceDiscoveredListener, deviceDisconnectedListener)
    }

    protected fun startSecondDiscoverer(deviceDiscoveredListener: (DiscoveredDevice) -> Unit,
                                        deviceDisconnectedListener: (DiscoveredDevice) -> Unit) {

        startDiscoverer(secondDiscoverer, SECOND_DISCOVERER_ID, deviceDiscoveredListener, deviceDisconnectedListener)
    }

    protected fun startDiscoverer(discoverer: UdpDeviceDiscoverer, deviceId: String, deviceDiscoveredListener: (DiscoveredDevice) -> Unit,
                                  deviceDisconnectedListener: (DiscoveredDevice) -> Unit) {

        startedDiscoverers.add(discoverer)

        discoverer.addDeviceDiscoveredListener(deviceDiscoveredListener)
        discoverer.addDeviceDisconnectedListener(deviceDisconnectedListener)

        val config = UdpDeviceDiscovererConfig(DISCOVERY_PREFIX, 0, deviceId, DISCOVERY_PORT, CHECK_FOR_DEVICES_INTERVAL)

        discoverer.startAsync(config)
    }

}
