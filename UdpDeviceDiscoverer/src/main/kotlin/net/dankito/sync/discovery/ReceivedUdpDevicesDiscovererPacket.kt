package net.dankito.sync.discovery


class ReceivedUdpDevicesDiscovererPacket(val receivedData: ByteArray, val senderAddress: String, val localServiceDescription: String,
                                         val localServiceName: String)
