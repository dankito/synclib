package net.dankito.sync.discovery


interface ConnectionsAliveWatcherListener {

    fun deviceDisconnected(discoveredDevice: DiscoveredDevice)

}
