package net.dankito.sync.discovery

import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.concurrent.scheduleAtFixedRate


class ConnectionsAliveWatcher(protected var connectionTimeout: Int) {

    companion object {
        private val log = LoggerFactory.getLogger(ConnectionsAliveWatcher::class.java)
    }


    protected var connectionsAliveCheckTimer: Timer? = null

    protected var lastMessageReceivedFromDeviceTimestamps: MutableMap<DiscoveredDevice, Long> = ConcurrentHashMap()


    val isRunning: Boolean
        get() = connectionsAliveCheckTimer != null

    fun startWatchingAsync(discoveredDevices: List<DiscoveredDevice>, listener: ConnectionsAliveWatcherListener) {
        synchronized(this) {
            stopWatching()

            log.info("Starting ConnectionsAliveWatcher ...")

            val checkInterval = (connectionTimeout / 2f).toLong()

            val connectionsAliveCheckTimer = Timer("ConnectionsAliveWatcher Timer")

            connectionsAliveCheckTimer.scheduleAtFixedRate(checkInterval, checkInterval) {
                checkIfConnectedDevicesStillAreConnected(discoveredDevices, listener)
            }

            this.connectionsAliveCheckTimer = connectionsAliveCheckTimer
        }
    }

    fun stopWatching() {
        synchronized(this) {
            connectionsAliveCheckTimer?.let { connectionsAliveCheckTimer ->
                log.info("Stopping ConnectionsAliveWatcher ...")

                connectionsAliveCheckTimer.cancel()

                lastMessageReceivedFromDeviceTimestamps.clear()
            }

            connectionsAliveCheckTimer = null
        }
    }


    fun receivedMessageFromDevice(discoveredDevice: DiscoveredDevice) {
        lastMessageReceivedFromDeviceTimestamps.put(discoveredDevice, Date().time)
    }


    protected fun checkIfConnectedDevicesStillAreConnected(discoveredDevices: List<DiscoveredDevice>, listener: ConnectionsAliveWatcherListener) {
        val now = Date().time

        for(discoveredDevice in discoveredDevices) {
            if(hasDeviceExpired(discoveredDevice, now)) {
                lastMessageReceivedFromDeviceTimestamps[discoveredDevice]?.let { timestamp ->
                    log.info("Device $discoveredDevice has disconnected, last message received at ${Date(timestamp)}, now = ${Date(now)}. " +
                            "Remaining keys = ${lastMessageReceivedFromDeviceTimestamps.keys}")
                }

                deviceDisconnected(discoveredDevice, listener)
            }
        }
    }

    protected fun hasDeviceExpired(discoveredDevice: DiscoveredDevice, now: Long): Boolean {
        val lastMessageReceivedFromDeviceTimestamp = lastMessageReceivedFromDeviceTimestamps[discoveredDevice]

        if(lastMessageReceivedFromDeviceTimestamp != null) {
            val hasExpired = lastMessageReceivedFromDeviceTimestamp < now - connectionTimeout
            if(hasExpired) {
                if(hasReconnected(discoveredDevice)) {
                    lastMessageReceivedFromDeviceTimestamps.remove(discoveredDevice)
                }
                else {
                    return true
                }
            }
        }

        return false
    }

    private fun hasReconnected(discoveredDevice: DiscoveredDevice): Boolean {
        // TODO: i think this check is not going to work anymore
//        val index = discoveredDevice.lastIndexOf(":") // this is actually bad as it uses knowledge from ConnectedDevicesService.MESSAGES_PORT_AND_BASIC_DATA_SYNC_PORT_SEPARATOR
//        if(index > 0) {
//            val deviceKeyWithoutBasicDataSyncPort = discoveredDevice.substring(0, index)
//
//            for(otherDeviceKey in lastMessageReceivedFromDeviceTimestamps.keys) {
//                if(discoveredDevice != otherDeviceKey && otherDeviceKey.startsWith(deviceKeyWithoutBasicDataSyncPort)) { // device just reconnected shortly with different basic data sync port
//                    return true
//                }
//            }
//        }

        return false
    }

    protected fun deviceDisconnected(discoveredDevice: DiscoveredDevice, listener: ConnectionsAliveWatcherListener?) {
        lastMessageReceivedFromDeviceTimestamps.remove(discoveredDevice)

        listener?.deviceDisconnected(discoveredDevice)
    }

}
