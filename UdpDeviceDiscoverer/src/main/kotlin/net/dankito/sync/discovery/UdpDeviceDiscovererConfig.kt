package net.dankito.sync.discovery


class UdpDeviceDiscovererConfig(
                                /**
                                 * Will be used as prefix for discovery message
                                 */
                                serviceName: String,
                                /**
                                 * Will be send together with serviceDescription in discovery message for remote devices
                                 */
                                servicePortToPromote: Int,
                                /**
                                 * Will be send together with servicePortToPromote in discovery message for remote devices
                                 */
                                serviceDescription: String,
                                /**
                                 * Port to which discovery messages will be sent to
                                 */
                                discovererPort: Int,
                                /**
                                 * The interval in which discovery messages should get sent
                                 */
                                val checkForDevicesIntervalMillis: Int
)

    : DeviceDiscovererConfig("" /* serviceType will be ignored in UdpDevicesDiscoverer */,
        serviceName, servicePortToPromote, serviceDescription, discovererPort) {


    override fun toString(): String {
        return "$serviceDescription:$servicePortToPromote"
    }

}
