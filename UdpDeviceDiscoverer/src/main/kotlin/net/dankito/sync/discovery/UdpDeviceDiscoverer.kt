package net.dankito.sync.discovery

import net.dankito.utils.AsyncProducerConsumerQueue
import net.dankito.utils.IThreadPool
import net.dankito.utils.network.INetworkConnectivityManager
import net.dankito.utils.network.INetworkHelper
import net.dankito.utils.network.NetworkHelper
import net.dankito.utils.network.NetworkInterfaceState
import org.slf4j.LoggerFactory
import java.net.*
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CopyOnWriteArrayList
import kotlin.concurrent.schedule


// TODO: separate in Listener and Broadcast sender?

// TODO: be aware that this approach can disturb busy local networks. From Wikipedia UPnP article:
// UPnP is generally regarded as unsuitable for deployment in business settings for reasons of economy, complexity, and consistency: the multicast foundation makes it chatty, consuming too many network resources on networks with a large population of devices
open class UdpDeviceDiscoverer(protected val networkConnectivityManager: INetworkConnectivityManager, protected val threadPool: IThreadPool,
                               protected val networkHelper: INetworkHelper = NetworkHelper()) : DeviceDiscovererBase() {

    companion object {

        protected const val MessageHeaderAndBodySeparator = ":|"

        protected const val ServiceDescriptionAndServicePortSeparator = "|:"

        protected val MessagesCharset: Charset = Charset.forName("utf8")

        protected const val DelayBeforeRestartingBroadcastForAddressMillis = 5000

        private val log = LoggerFactory.getLogger(UdpDeviceDiscoverer::class.java)
    }


    private var connectionsAliveWatcher: ConnectionsAliveWatcher? = null

    private var listenerThread: Thread? = null

    private var listenerSocket: DatagramSocket? = null
    private var isListenerSocketOpened = false

    private var broadcastThreads: MutableMap<String, Thread> = ConcurrentHashMap()

    private var networkInterfaceConnectivityChangedListener: ((NetworkInterfaceState) -> Unit)? = null

    private var openedBroadcastSockets: MutableList<DatagramSocket> = ArrayList()
    private var areBroadcastSocketsOpened = false

    private var timerToRestartBroadcastForBroadcastAddress: Timer? = null

    private val receivedPacketsQueue: AsyncProducerConsumerQueue<ReceivedUdpDevicesDiscovererPacket>

    private var discoveredDevices: MutableList<DiscoveredDevice> = CopyOnWriteArrayList()


    init {
        receivedPacketsQueue = AsyncProducerConsumerQueue(3, autoStart = false) { receivedPacket ->
            handleReceivedPacket(receivedPacket.receivedData, receivedPacket.senderAddress, receivedPacket.localServiceDescription,
                    receivedPacket.localServiceName)
        }
    }


//    override val isRunning: Boolean
//        get() = isListenerSocketOpened && areBroadcastSocketsOpened

    override fun start(config: DeviceDiscovererConfig): Boolean {
        start(UdpDeviceDiscovererConfig(config.serviceName, config.servicePortToPromote, config.serviceDescription, 12345, 2000)) // TODO: how to tell default values?

        return true
    }

    open fun startAsync(config: UdpDeviceDiscovererConfig) {
        threadPool.runAsync { start(config) }
    }

    open fun start(config: UdpDeviceDiscovererConfig) {
        log.info("Starting UdpDeviceDiscoverer " + config.serviceDescription + " ...")

        receivedPacketsQueue.start()

        // * 3.5 = from 3 messages one must be received to be still valued as 'connected'
        this.connectionsAliveWatcher = ConnectionsAliveWatcher((config.checkForDevicesIntervalMillis * 3.5).toInt())

        startListenerAsync(config)

        startBroadcastAsync(config)
    }

    override fun stop() {
        log.info("Stopping UdpDeviceDiscoverer ...")

        receivedPacketsQueue.stopAndClearQueue()

        connectionsAliveWatcher?.stopWatching()

        stopListener()

        stopBroadcast()
    }

    private fun stopBroadcast() {
        synchronized(broadcastThreads) {
            networkInterfaceConnectivityChangedListener?.let { networkConnectivityManager.removeNetworkInterfaceConnectivityChangedListener(it) }

            areBroadcastSocketsOpened = false

            for (clientSocket in openedBroadcastSockets) {
                clientSocket.close()
            }

            openedBroadcastSockets.clear()

            for (broadcastAddress in ArrayList(broadcastThreads.keys)) {
                stopBroadcast(broadcastAddress)
            }
        }
    }

    private fun stopBroadcast(broadcastAddress: String) {
        broadcastThreads[broadcastAddress]?.let { broadcastThread ->
            try {
                broadcastThread.interrupt()
            } catch (ignored: Exception) {
            }

            broadcastThreads.remove(broadcastAddress)
            log.info("Stopped broadcasting for Address " + broadcastAddress)
        }
    }

//    override fun disconnectedFromDevice(device: DiscoveredDevice) {
//        removeDeviceFromFoundDevices(device)
//    }


    private fun startListenerAsync(config: UdpDeviceDiscovererConfig) {
        stopListener()

        listenerThread = Thread({ startListener(config) }, "UdpDevicesDiscoverer_Listener")

        listenerThread?.start()
    }

    private fun stopListener() {
        try { listenerThread?.interrupt() } catch (ignored: Exception) { }
        listenerThread = null

        if (isListenerSocketOpened) {
            listenerSocket?.close()
            listenerSocket = null
            isListenerSocketOpened = false
        }
    }

    private fun startListener(config: UdpDeviceDiscovererConfig) {
        try {
            this.listenerSocket = createListenerSocket(config.discovererPort)

            val buffer = ByteArray(1024)
            val packet = DatagramPacket(buffer, buffer.size)

            while(isListenerSocketOpened) {
                try {
                    listenerSocket?.receive(packet)
                } catch (ex: Exception) {
                    if (isSocketCloseException(ex) == true)
                    // communication has been cancelled by close() method
                        break
                    else {
                        log.error("An Error occurred receiving Packets. listenerSocket = " + listenerSocket, ex)
                        startListener(config)
                    }
                }

                listenerReceivedPacket(buffer, packet, config)
            }
        } catch (ex: Exception) {
            log.error("An error occurred starting UdpDevicesSearcher", ex)
        }

    }

    @Throws(SocketException::class)
    private fun createListenerSocket(discoverDevicesPort: Int): DatagramSocket {
        val listenerSocket = DatagramSocket(null) // so that other Applications on the same Host can also use this port, set bindAddress to null ..,
        listenerSocket.reuseAddress = true // and reuseAddress to true
        listenerSocket.bind(InetSocketAddress(discoverDevicesPort))

        listenerSocket.broadcast = true
        isListenerSocketOpened = true

        return listenerSocket
    }

    private fun isSocketCloseException(ex: Exception): Boolean {
        return networkHelper.isSocketCloseException(ex)
    }


    private fun listenerReceivedPacket(buffer: ByteArray, packet: DatagramPacket, config: UdpDeviceDiscovererConfig) {
        receivedPacketsQueue.add(ReceivedUdpDevicesDiscovererPacket(Arrays.copyOf(buffer, packet.length), packet.address.hostAddress,
                config.serviceDescription, config.serviceName))
    }


    private fun handleReceivedPacket(receivedData: ByteArray, senderAddress: String, localServiceDescription: String, discoveryMessagePrefix: String) {
        val receivedMessage = parseBytesToString(receivedData, receivedData.size)

        if(isSearchingForDevicesMessage(receivedMessage, discoveryMessagePrefix)) {
            val remoteServiceDescription = getServiceDescriptionFromMessage(receivedMessage)

            if(isSelfSentPacket(remoteServiceDescription, localServiceDescription) == false) {
                handleReceivedRemotePacket(senderAddress, getServicePort(receivedMessage), remoteServiceDescription)
            }
        }
    }

    private fun getServicePort(receivedMessage: String): Int {
        try {
            val servicePortStartIndex = receivedMessage.lastIndexOf(ServiceDescriptionAndServicePortSeparator) +
                    ServiceDescriptionAndServicePortSeparator.length

            val servicePortString = receivedMessage.substring(servicePortStartIndex)

            return servicePortString.toInt()
        } catch (e: Exception) {
            log.error("Could not extract service port from received message '$receivedMessage'", e)
        }

        return -1
    }

    private fun handleReceivedRemotePacket(senderAddress: String, remoteServicePort: Int, remoteServiceDescription: String) {
        val discoveredDevice = DiscoveredDevice(senderAddress, remoteServicePort, remoteServiceDescription)
        connectionsAliveWatcher?.receivedMessageFromDevice(discoveredDevice) // if device just reconnected it's better to update its timestamp immediately

        if(hasDeviceAlreadyBeenFound(discoveredDevice) == false) {
            deviceDiscovered(discoveredDevice)
        }
    }

    private fun isSearchingForDevicesMessage(receivedMessage: String, discoveryMessagePrefix: String): Boolean {
        return receivedMessage.startsWith(discoveryMessagePrefix + MessageHeaderAndBodySeparator)
    }

    private fun isSelfSentPacket(remoteServiceDescription: String, localServiceDescription: String): Boolean {
        return localServiceDescription == remoteServiceDescription
    }

    private fun hasDeviceAlreadyBeenFound(discoveredDevice: DiscoveredDevice): Boolean {
        return discoveredDevices.contains(discoveredDevice)
    }

    private fun deviceDiscovered(discoveredDevice: DiscoveredDevice) {
        log.info("Found Device ${discoveredDevice.serviceDescription} on ${discoveredDevice.address}")

        synchronized(this) {
            discoveredDevices.add(discoveredDevice)

            if(discoveredDevices.size == 1) {
                startConnectionsAliveWatcher()
            }
        }

        callDeviceDiscoveredListeners(discoveredDevice)
    }

    private fun startConnectionsAliveWatcher() {
        connectionsAliveWatcher?.startWatchingAsync(discoveredDevices, object : ConnectionsAliveWatcherListener {
            override fun deviceDisconnected(discoveredDevice: DiscoveredDevice) {
                this@UdpDeviceDiscoverer.deviceDisconnected(discoveredDevice)
            }
        })
    }

    protected fun deviceDisconnected(disconnectedDevice: DiscoveredDevice) {
        log.info("Device disconnected: $disconnectedDevice")

        if (discoveredDevices.contains(disconnectedDevice)) {
            removeDeviceFromFoundDevices(disconnectedDevice)

            callDeviceDisconnectedListeners(disconnectedDevice)
        }
    }


    private fun startBroadcastAsync(config: UdpDeviceDiscovererConfig) {
        threadPool.runAsync { startBroadcast(config) }
    }

    private fun startBroadcast(config: UdpDeviceDiscovererConfig) {
        networkInterfaceConnectivityChangedListener = { networkInterfaceConnectivityChanged(it, config) }

        networkInterfaceConnectivityChangedListener?.let { networkConnectivityManager.addNetworkInterfaceConnectivityChangedListener(it) }

        for (broadcastAddress in networkConnectivityManager.getBroadcastAddresses()) {
            startBroadcastForBroadcastAddressAsync(broadcastAddress, config)
        }
    }

    private fun networkInterfaceConnectivityChanged(state: NetworkInterfaceState, config: UdpDeviceDiscovererConfig) {
        synchronized(broadcastThreads) {
            state.broadcastAddress?.hostAddress?.let { broadcastAddress ->
                if(broadcastThreads.containsKey(broadcastAddress)) {
                    if(state.isUp == false) {
                        stopBroadcast(broadcastAddress)
                    }
                }
                else if(state.isUp) {
                    state.broadcastAddress?.let { startBroadcastForBroadcastAddressAsync(it, config) }
                }
            }
        }
    }

    private fun startBroadcastForBroadcastAddressAsync(broadcastAddress: InetAddress, config: UdpDeviceDiscovererConfig) {
        synchronized(broadcastThreads) {
            val broadcastThread = Thread(Runnable { startBroadcastForBroadcastAddress(broadcastAddress, config) }, "UdpDevicesDiscoverer_BroadcastTo_" + broadcastAddress.hostAddress)

            broadcastThreads.put(broadcastAddress.hostAddress, broadcastThread)

            broadcastThread.start()
        }
    }

    protected fun startBroadcastForBroadcastAddress(broadcastAddress: InetAddress, config: UdpDeviceDiscovererConfig) {
        try {
            log.info("Starting broadcast for address ${broadcastAddress.hostAddress}")
            val broadcastSocket = DatagramSocket()

            synchronized(broadcastThreads) {
                openedBroadcastSockets.add(broadcastSocket)
                areBroadcastSocketsOpened = true
            }

            broadcastSocket.soTimeout = 10000

            val timer = Timer()
            timer.schedule(config.checkForDevicesIntervalMillis.toLong(), config.checkForDevicesIntervalMillis.toLong()) {
                if(broadcastSocket.isClosed || sendBroadcastOnSocket(broadcastSocket, broadcastAddress, config) == false) {
                    timer.cancel()
                }
            }
        } catch (e: Exception) {
            log.error("Could not start broadcast for address ${broadcastAddress.hostAddress}", e)
        }

    }

    private fun sendBroadcastOnSocket(broadcastSocket: DatagramSocket, broadcastAddress: InetAddress, config: UdpDeviceDiscovererConfig): Boolean {
        try {
            val searchDevicesPacket = createSearchDevicesDatagramPacket(broadcastAddress, config)
            broadcastSocket.send(searchDevicesPacket)
        } catch (e: Exception) {
            log.error("Could not send Broadcast to Address " + broadcastAddress, e)

            synchronized(broadcastThreads) {
                openedBroadcastSockets.remove(broadcastSocket)
            }
            broadcastSocket.close()

            restartBroadcastForBroadcastAddress(broadcastAddress, config)

            return false
        }

        return true
    }

    private fun restartBroadcastForBroadcastAddress(broadcastAddress: InetAddress, config: UdpDeviceDiscovererConfig) {
        if (timerToRestartBroadcastForBroadcastAddress == null) {
            timerToRestartBroadcastForBroadcastAddress = Timer(true)
        }

        // TODO: a problem about using Timer is, that then broadcasts are send on Timer thread and not on broadcastThread
        timerToRestartBroadcastForBroadcastAddress?.schedule(object : TimerTask() {
            override fun run() {
                startBroadcastForBroadcastAddress(broadcastAddress, config)
            }
        }, DelayBeforeRestartingBroadcastForAddressMillis.toLong())
    }

    private fun createSearchDevicesDatagramPacket(broadcastAddress: InetAddress, config: UdpDeviceDiscovererConfig): DatagramPacket {
        val message = config.serviceName + MessageHeaderAndBodySeparator + config.serviceDescription +
                ServiceDescriptionAndServicePortSeparator + config.servicePortToPromote
        val messageBytes = message.toByteArray(MessagesCharset)

        return DatagramPacket(messageBytes, messageBytes.size, broadcastAddress, config.discovererPort)
    }

    private fun getServiceDescriptionFromMessage(receivedMessage: String): String {
        val bodyStartIndex = receivedMessage.indexOf(MessageHeaderAndBodySeparator) + MessageHeaderAndBodySeparator.length
        val serviceDescriptionEndIndex = receivedMessage.indexOf(ServiceDescriptionAndServicePortSeparator)

        return receivedMessage.substring(bodyStartIndex, serviceDescriptionEndIndex)
    }

    private fun parseBytesToString(receivedData: ByteArray, packetLength: Int): String {
        return String(receivedData, 0, packetLength, MessagesCharset)
    }


    private fun removeDeviceFromFoundDevices(discoveredDevice: DiscoveredDevice) {
        discoveredDevices.remove(discoveredDevice)
    }

}
