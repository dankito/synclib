package net.dankito.sync.discovery

import android.content.Context
import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import org.slf4j.LoggerFactory


/**
 * Code taken from: https://developer.android.com/training/connect-devices-wirelessly/nsd
 */
open class NsdServiceDeviceDiscoverer(protected val context: Context) : DeviceDiscovererBase() {

    companion object {
        private val log = LoggerFactory.getLogger(NsdServiceDeviceDiscoverer::class.java)
    }


    protected var nsdManager: NsdManager? = null

    protected var localPort: Int? = null

    protected var serviceName: String? = null

    protected var serviceType: String? = null


    override fun start(config: DeviceDiscovererConfig): Boolean {
        if (nsdManager != null) {
            return false
        }

        try {
            // Create the NsdServiceInfo object, and populate it.
            val serviceInfo = NsdServiceInfo()
            // The name is subject to change based on conflicts
            // with other services advertised on the same network.
            serviceInfo.serviceName = config.serviceName
            serviceInfo.serviceType = config.serviceType
            serviceInfo.port = config.servicePortToPromote
            // no service description?

            this.serviceType = config.serviceType

            nsdManager = (context.getSystemService(Context.NSD_SERVICE) as NsdManager).apply {
                try {
                    registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, registrationListener)
                    discoverServices(config.serviceType, NsdManager.PROTOCOL_DNS_SD, discoveryListener)
                } catch (e: Exception) {
                    log.error("Could not start discovery", e)
                }
            }

            return true
        } catch (e: Exception) {
            log.error("Could not register Service", e)
        }

        return false
    }

    override fun stop() {
        // Unregister all services
        nsdManager?.unregisterService(registrationListener)
        nsdManager?.stopServiceDiscovery(discoveryListener)

        nsdManager = null
        localPort = null
        serviceName = null
    }


    protected val registrationListener = object : NsdManager.RegistrationListener {

        override fun onServiceRegistered(nsdServiceInfo: NsdServiceInfo) {
            // Save the service name. Android may have changed it in order to
            // resolve a conflict, so update the name you initially requested
            // with the name Android actually used.
            serviceName = nsdServiceInfo.serviceName
        }

        override fun onRegistrationFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
            // Registration failed! Put debugging code here to determine why.
        }

        override fun onServiceUnregistered(arg0: NsdServiceInfo) {
            // Service has been unregistered. This only happens when you call
            // NsdManager.unregisterService() and pass in this listener.
        }

        override fun onUnregistrationFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
            // Unregistration failed. Put debugging code here to determine why.
        }
    }

    // Instantiate a new DiscoveryListener
    protected val discoveryListener = object : NsdManager.DiscoveryListener {

        // Called as soon as service discovery begins.
        override fun onDiscoveryStarted(regType: String) {
            log.debug("Service discovery started")
        }

        override fun onServiceFound(service: NsdServiceInfo) {
            // A service was found! Do something with it.
            log.debug("Service discovery success $service")
            when {
                service.serviceType != serviceType -> // Service type is the string containing the protocol and
                    // transport layer for this service.
                    log.debug("Unknown Service Type: ${service.serviceType}")
                service.serviceName == serviceName -> // The name of the service tells the user what they'd be
                    // connecting to. It could be "Bob's Chat App".
                    log.debug("Same machine: $serviceName")
                service.serviceName.contains("Sync") -> try { nsdManager?.resolveService(service, resolveListener) } catch (e: Exception) { log.error("Could not start resolveListener", e)}
            }
        }

        override fun onServiceLost(service: NsdServiceInfo) {
            // When the network service is no longer available.
            // Internal bookkeeping code goes here.
            log.error("service lost: $service")
        }

        override fun onDiscoveryStopped(serviceType: String) {
            log.info("Discovery stopped: $serviceType")
        }

        override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
            log.error("Discovery failed: Error code:$errorCode")
            try {
                nsdManager?.stopServiceDiscovery(this)
            } catch (e: Exception) { log.error("Couldn't stop service discovery", e) }
        }

        override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
            log.error("Discovery failed: Error code:$errorCode")
            try {
                nsdManager?.stopServiceDiscovery(this)
            } catch (e: Exception) { log.error("Couldn't stop service discovery", e) }
        }
    }

    protected val resolveListener = object : NsdManager.ResolveListener {

        override fun onResolveFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
            // Called when the resolve fails. Use the error code to debug.
            log.error("Resolve failed: $errorCode")
        }

        override fun onServiceResolved(serviceInfo: NsdServiceInfo) {
            log.error("Resolve Succeeded. $serviceInfo")

            if (serviceInfo.serviceName == serviceName) {
                log.debug("Same IP.")
                return
            }

            callDeviceDiscoveredListeners(DiscoveredDevice(serviceInfo.host.hostAddress, serviceInfo.port, serviceInfo.serviceName))
        }
    }

}