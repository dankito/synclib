package net.dankito.sync.discovery

import android.os.Build
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import net.dankito.sync.discovery.IDeviceDiscoverer.Companion.DefaultServiceType
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import java.net.InetAddress
import java.util.concurrent.TimeUnit


@RunWith(AndroidJUnit4::class)
class NsdServiceDeviceDiscovererTest {

    companion object {
        private val ServiceName = "SyncTest"

        private val ServicePort = 1234

        private val ServiceDescription = "Your Description could be placed here"

        private val log = LoggerFactory.getLogger(NsdServiceDeviceDiscovererTest::class.java)
    }


    private val underTest = NsdServiceDeviceDiscoverer(InstrumentationRegistry.getTargetContext())

    @Test
    fun discoverDevice() {
        val discoveredDevices = mutableListOf<DiscoveredDevice>()

        underTest.addDeviceDiscoveredListener { endpoint ->
            log.info("[${Build.DEVICE}:${Build.MODEL} ${InetAddress.getLocalHost()}] Discovered device at $endpoint")
            discoveredDevices.add(endpoint)
        }

        underTest.startAsync(DeviceDiscovererConfig(DefaultServiceType, ServiceName, ServicePort, ServiceDescription))

        TimeUnit.SECONDS.sleep(60)

        assertThat(discoveredDevices.isNotEmpty(), `is`(true))
    }
}