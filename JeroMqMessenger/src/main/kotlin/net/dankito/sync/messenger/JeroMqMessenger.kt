package net.dankito.sync.messenger

import net.dankito.sync.Endpoint
import net.dankito.sync.Response
import net.dankito.utils.serialization.ISerializer
import org.slf4j.LoggerFactory
import org.zeromq.ZContext
import org.zeromq.ZMQ
import kotlin.concurrent.thread


open class JeroMqMessenger(serializer: ISerializer, listener: MessageReceivedListener)
    : NonHttpMessengerBase(serializer, listener) {

    companion object {
        private val ReceiveResponseTimeoutMillis = 10 * 1000

        private val log = LoggerFactory.getLogger(JeroMqMessenger::class.java)
    }


    protected var isStarted = false

    protected var context: ZContext? = null

    protected var receiverSocket: ZMQ.Socket? = null

    protected var receiverThread: Thread? = null


    constructor(serializer: ISerializer, listener: (Any) -> Any?) : this(serializer, object : MessageReceivedListener {

        override fun received(message: Any): Any? {
            return listener(message)
        }

    })


    override fun start(port: Int): Boolean {
        if (isStarted) {
            throw IllegalStateException("Already started")
        }

        val context = ZContext()

        val receiverSocket = context.createSocket(ZMQ.REP)
        val successful = receiverSocket.bind("tcp://*:$port")

        if (successful) {
            receiverThread = thread {
                receiveMessages(receiverSocket)
            }

            this.context = context
            this.receiverSocket = receiverSocket
            this.isStarted = true
        }

        return successful
    }

    protected open fun receiveMessages(receiverSocket: ZMQ.Socket) {
        while (isStarted) {
            val receivedMessage = receiverSocket.recvStr()

            // We have to block till response is sent as otherwise may another client connects and then response gets
            // sent to both.
            // This is really dangerous! If determining message blocks for a long time, no other clients can send
            // messages to us during that time.
            determineAndSendResponse(receivedMessage)
        }
    }

    protected open fun determineAndSendResponse(receivedMessage: String?) {
        val response = determineResponse(receivedMessage)

        try {
            sendResponse(response, receivedMessage)
        } catch (sendException: Exception) {
            log.error("Could not send response to received message '$receivedMessage'", sendException)

            receiverSocket?.let { receiverSocket ->
                sendCouldNotDetermineResponse(receivedMessage)
            }
        }
    }

    protected open fun sendResponse(response: String, receivedMessage: String?) {
        if (response != CouldNotDetermineResponseResponse) {
            receiverSocket?.send(response)
        }
        else {
            sendCouldNotDetermineResponse(receivedMessage)
        }
    }

    protected open fun sendCouldNotDetermineResponse(receivedMessage: String?) {
        receiverSocket?.let {
            val receivedMessage = receivedMessage ?: ReceivedEmptyMessageResponse
            sendWeDontHaveAResponse(it, CouldNotDetermineResponseResponse, receivedMessage)
        }
    }

    protected open fun sendWeDontHaveAResponse(receiverSocket: ZMQ.Socket, responseMessage: String,
                                               receivedMessage: String) {
        try {
            receiverSocket.send(responseMessage, 0)
        } catch (e: Exception) {
            log.error("Could not send that we don't have a response for received message '$receivedMessage'", e)
        }
    }


    override fun stop(): Boolean {
        if (isStarted == false) {
            throw IllegalStateException("JeroMqMessenger not started")
        }

        receiverThread?.join(200)

        receiverSocket?.close()

        context?.close()

        isStarted = false

        return true
    }


    override fun <TRequest : Any, TResponse> sendMessage(endpoint: Endpoint, message: MessageEnvelop<TRequest>,
                                                         responseClass: Class<TResponse>) : Response<TResponse> {
        if (isStarted) {
            context?.let { context ->
                try {
                    return sendMessage<TRequest, TResponse>(context, endpoint, message, responseClass)
                } catch (e: Exception) {
                    log.error("Could not send message", e)
                    return Response(false, e)
                }
            }
        }

        return Response(false)
    }

    protected open fun <TRequest : Any, TResponse> sendMessage(context: ZContext, endpoint: Endpoint, message: MessageEnvelop<TRequest>,
                                                          responseClass: Class<TResponse>) : Response<TResponse> {

        context.createSocket(ZMQ.REQ).use { publisher ->
            publisher.linger = 5000
            // In 0MQ 3.x pub socket could drop messages if sub can follow the
            // generation of pub messages
            publisher.sndHWM = 0
            publisher.receiveTimeOut = ReceiveResponseTimeoutMillis

            if (publisher.connect("tcp://${endpoint.address}:${endpoint.messengerPort}")) {
                if (publisher.send(serializeMessageToStringAndAddType(message))) {
                    val response = publisher.recvStr(0)

                    return Response(true, null, deserializeMessage(response, responseClass))
                }
            }
        }

        return Response(false)
    }

}