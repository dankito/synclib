package net.dankito.sync.messenger

import net.dankito.sync.Endpoint
import net.dankito.sync.Response
import net.dankito.utils.serialization.JacksonJsonSerializer
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Test


class JeroMqMessengerTest {

    companion object {
        private val FirstMessengerPort = 54321
        private val SecondMessengerPort = 54322
        private val ThirdMessengerPort = 54323

        private val FirstMessengerEndpoint = Endpoint("*", FirstMessengerPort)
        private val SecondMessengerEndpoint = Endpoint("*", SecondMessengerPort)
        private val ThirdMessengerEndpoint = Endpoint("*", ThirdMessengerPort)

        private val request = "Hello"
        private val response = "World"

        private val disturbingRequest = "Sorry to disturb you"
        private val disturbingResponse = "Don't mind"
    }


    private val serializer = JacksonJsonSerializer()

    private var first: JeroMqMessenger? = null

    private var second: JeroMqMessenger? = null

    private var third: JeroMqMessenger? = null


    @After
    fun tearDown() {
        first?.stop()
        second?.stop()
        third?.stop()
    }


    @Test
    fun sendMessageToSecondEndpoint_ReceivesResponse() {
        val first = JeroMqMessenger(serializer) { }
        this.first = first
        first.start(FirstMessengerPort)

        val second = JeroMqMessenger(serializer) { message ->
            if (message == request) {
                return@JeroMqMessenger response
            }

            return@JeroMqMessenger null
        }

        this.second = second
        second.start(SecondMessengerPort)


        // when
        val receivedResponse = first.sendMessage(SecondMessengerEndpoint, MessageEnvelop(String::class.java, request), String::class.java)

        // then
        assertThat(receivedResponse.isSuccessful).isTrue()
        assertThat(receivedResponse.body).isEqualTo(response)
    }

    @Test
    fun sendMessageToThirdEndpoint_OnlyThirdEndpointReceivesResponse() {
        val first = JeroMqMessenger(serializer) { }
        this.first = first
        first.start(FirstMessengerPort)

        val second = JeroMqMessenger(serializer) { }
        this.second = second
        second.start(SecondMessengerPort)

        val third = JeroMqMessenger(serializer) { message ->
            if (message == request) {
                return@JeroMqMessenger response
            }

            return@JeroMqMessenger disturbingResponse
        }

        this.third = third
        third.start(ThirdMessengerPort)


        // when
        var disturbingMessageResponse = Response<String>(false)
        second.sendMessageAsync(ThirdMessengerEndpoint, MessageEnvelop(String::class.java, disturbingRequest), String::class.java) {
            disturbingMessageResponse = it
        }

        val receivedResponse = first.sendMessage(ThirdMessengerEndpoint, MessageEnvelop(String::class.java, request), String::class.java)

        // then
        assertThat(receivedResponse.isSuccessful).isTrue()
        assertThat(receivedResponse.body).isEqualTo(response)

        assertThat(disturbingMessageResponse.body).isNotEqualTo(response)
    }

}