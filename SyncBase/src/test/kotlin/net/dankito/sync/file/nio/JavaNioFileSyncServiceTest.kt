package net.dankito.sync.file.nio

import net.dankito.sync.Endpoint
import net.dankito.utils.FormatUtils
import net.dankito.utils.serialization.JacksonJsonSerializer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.slf4j.LoggerFactory
import java.io.File
import java.time.Duration
import java.time.Instant

class JavaNioFileSyncServiceTest {

    companion object {
        private const val FileServerPort = 23456

        private val log = LoggerFactory.getLogger(JavaNioFileSyncServiceTest::class.java)
    }


    private val socketChannelHandler = SocketChannelHandler(JacksonJsonSerializer())

    private val fileServer = JavaNioFileServer(socketChannelHandler)

    private val underTest = JavaNioFileSyncService(socketChannelHandler)


    @Before
    fun setUp() {
        // TODO: add test file

        fileServer.start(FileServerPort)
    }

    @After
    fun tearDown() {
        fileServer.stop()
    }


    @Test
    fun retrieveFile() {
        val destinationFile = File.createTempFile("TcpFileSyncService_Test_", ".test")
        val start = Instant.now()

        underTest.retrieveFile(Endpoint("127.0.0.1", FileServerPort, FileServerPort), "anything", destinationFile)

        val duration = Duration.between(start, Instant.now())
        log.info("Retrieved ${destinationFile.length()} bytes to ${destinationFile.absolutePath} in " +
                "${duration.toMillis() / 1000} seconds")
        log.info(FormatUtils().formatSpeed(destinationFile.length(), duration.toMillis()))
    }

}