package net.dankito.sync.integrationtest.file.server

import net.dankito.sync.file.IFileServer


class NanoHttpdFileServerSpeedTest : FileServerSpeedTestBase() {

    override fun getFileSyncPort(): Int {
        return MessagingPort
    }

    override fun createFileServer(): IFileServer {
        return messenger
    }

}


fun main(args: Array<String>) {
    val serverSpeedTest = NanoHttpdFileServerSpeedTest()

    serverSpeedTest.runServer()
}