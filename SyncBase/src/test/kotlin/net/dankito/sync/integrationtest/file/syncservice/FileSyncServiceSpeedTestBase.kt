package net.dankito.sync.integrationtest.file.syncservice

import net.dankito.sync.file.IFileSyncService
import net.dankito.sync.integrationtest.file.SpeedTestBase
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentMessage
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentResponse
import net.dankito.utils.FormatUtils
import net.dankito.utils.io.FileInfo
import org.junit.Ignore
import org.junit.Test
import org.slf4j.LoggerFactory
import java.io.File


abstract class FileSyncServiceSpeedTestBase : SpeedTestBase() {

    companion object {
        private val log = LoggerFactory.getLogger(FileSyncServiceSpeedTestBase::class.java)
    }


    protected val formatUtils = FormatUtils()

    protected val fileSyncService: IFileSyncService = createFileSyncService()


    protected abstract fun createFileSyncService(): IFileSyncService


    @Ignore // don't run test on CI server etc.
    @Test
    fun testSpeed() {
        val response = messenger.sendMessage(testEndpoint, messageBuilder.getContentDirectoryMessage(GetDirectoryContentMessage.FilesForDownloadSpeedTest),
                GetDirectoryContentResponse::class.java)

        var totalDownloadedBytes = 0L
        var totalTimeSpentNanos = 0L

        response.body?.files?.let { files ->
            files.forEachIndexed { index, file ->
                val sizeAndDuration = downloadFile(file, "${index + 1} / ${files.size}")

                totalDownloadedBytes += sizeAndDuration.first
                totalTimeSpentNanos += sizeAndDuration.second
            }
        }

        val totalTimeSpentMillis = getMillisFromNanos(totalTimeSpentNanos)
        log.info("In total downloaded $totalDownloadedBytes bytes in ${totalTimeSpentMillis / 1000} seconds = " +
                formatUtils.formatSpeed(totalDownloadedBytes, totalTimeSpentMillis))
    }

    protected open fun downloadFile(file: FileInfo, index: String): Pair<Long, Long> {
        log.info("[$index] Downloading ${file.name}: ${formatUtils.formatFileSize(file.size)}")
        val filename = File(file.name)
        val destinationFile = File.createTempFile(filename.name, filename.extension)
        val startTime = System.nanoTime()

        fileSyncService.retrieveFile(testEndpoint, file.id, destinationFile)

        val duration = System.nanoTime() - startTime
        val downloadedBytes = destinationFile.length()
        log.info("[$index] Downloaded $downloadedBytes bytes of ${file.size} in " +
                formatUtils.formatSpeed(downloadedBytes, getMillisFromNanos(duration)))

        destinationFile.delete()

        return Pair(downloadedBytes, duration)
    }

    private fun getMillisFromNanos(durationInNanos: Long): Long {
        return durationInNanos / 1000 / 1000
    }

}