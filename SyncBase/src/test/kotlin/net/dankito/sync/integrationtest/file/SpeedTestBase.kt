package net.dankito.sync.integrationtest.file

import net.dankito.sync.Endpoint
import net.dankito.sync.messenger.NanoHttpdMessenger
import net.dankito.sync.security.ConscryptSecurityProviderJava
import net.dankito.sync.test.TestMessenger
import net.dankito.sync.test.messenger.http.HttpMessageBuilder
import net.dankito.sync.test.messenger.http.UrlToMessageBodyMapper
import net.dankito.sync.test.messenger.http.messages.DeviceInfo
import net.dankito.utils.serialization.JacksonJsonSerializer
import java.util.*


abstract class SpeedTestBase {

    companion object {
        const val MessagingPort = 23456
    }


    protected val serializer = JacksonJsonSerializer()

    protected val urlToMessageBodyMapper = UrlToMessageBodyMapper()

    val messageBuilder: HttpMessageBuilder = HttpMessageBuilder(urlToMessageBodyMapper)

    protected val securityProvider = ConscryptSecurityProviderJava()


    protected val deviceInfo: DeviceInfo

    protected val messenger: NanoHttpdMessenger


    // TODO: adjust remote endpoint address here
    protected val testEndpoint = Endpoint("192.168.178.22", MessagingPort, getFileSyncPort())


    protected open fun getFileSyncPort(): Int { // may adjust file sync port in concrete class
        return MessagingPort + 1
    }


    init {
        val keys = securityProvider.keyGenerator.generatePublicPrivateKeyPair(securityProvider.provider)

        deviceInfo = DeviceInfo("", MessagingPort, getFileSyncPort(), UUID.randomUUID().toString(),
                "${System.getProperty("os.name")} ${System.getProperty("os.version")}", keys.public.encoded)

        messenger = TestMessenger(deviceInfo, urlToMessageBodyMapper, serializer, { })
        messenger.start(MessagingPort)
    }

}