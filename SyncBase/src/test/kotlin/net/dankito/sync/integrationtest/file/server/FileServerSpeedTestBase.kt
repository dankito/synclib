package net.dankito.sync.integrationtest.file.server

import net.dankito.sync.file.IFileServer
import net.dankito.sync.integrationtest.file.SpeedTestBase
import net.dankito.sync.messenger.IMessenger
import java.io.File


abstract class FileServerSpeedTestBase : SpeedTestBase() {


    protected val fileServer: IFileServer = createFileServer()


    protected abstract fun createFileServer(): IFileServer


    fun runServer() {
        fileServer.setGetFileForIdCallback { File(it) }

        if (fileServer is IMessenger == false) { // don't start Http based file server a second time
            fileServer.start(getFileSyncPort())
        }

        println("Press Enter to exit ...")
        readLine()

        fileServer.stop()
        messenger.stop()

        System.exit(0)
    }

}