package net.dankito.sync.integrationtest.file.syncservice

import net.dankito.sync.file.HttpServerFileSyncService
import net.dankito.sync.file.IFileSyncService


class HttpServerFileSyncServiceSpeedTest : FileSyncServiceSpeedTestBase() {

    override fun getFileSyncPort(): Int {
        return MessagingPort
    }

    override fun createFileSyncService(): IFileSyncService {
        return HttpServerFileSyncService()
    }

}