package net.dankito.sync.integrationtest.file.syncservice

import net.dankito.sync.file.IFileSyncService
import net.dankito.sync.file.nio.JavaNioFileSyncService
import net.dankito.sync.file.nio.SocketChannelHandler


class JavaNioFileSyncServiceSpeedTest : FileSyncServiceSpeedTestBase() {

    override fun createFileSyncService(): IFileSyncService {
        return JavaNioFileSyncService(SocketChannelHandler(serializer))
    }

}