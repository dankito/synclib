package net.dankito.sync.integrationtest.file.syncservice

import net.dankito.sync.file.IFileSyncService
import net.dankito.sync.file.stream.JavaStreamFileSyncService
import net.dankito.sync.file.stream.SocketHandler


class JavaStreamFileSyncServiceSpeedTest : FileSyncServiceSpeedTestBase() {

    override fun createFileSyncService(): IFileSyncService {
        return JavaStreamFileSyncService(SocketHandler(serializer))
    }

}