package net.dankito.sync.integrationtest.file.server

import net.dankito.sync.file.IFileServer
import net.dankito.sync.file.nio.JavaNioFileServer
import net.dankito.sync.file.nio.SocketChannelHandler


class JavaNioFileServerSpeedTest : FileServerSpeedTestBase() {

    override fun createFileServer(): IFileServer {
        return JavaNioFileServer(SocketChannelHandler(serializer))
    }

}


fun main(args: Array<String>) {
    val serverSpeedTest = JavaNioFileServerSpeedTest()

    serverSpeedTest.runServer()
}