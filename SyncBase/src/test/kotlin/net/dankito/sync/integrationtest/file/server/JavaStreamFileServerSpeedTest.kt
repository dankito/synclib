package net.dankito.sync.integrationtest.file.server

import net.dankito.sync.file.IFileServer
import net.dankito.sync.file.stream.JavaStreamFileServer
import net.dankito.sync.file.stream.SocketHandler


class JavaStreamFileServerSpeedTest : FileServerSpeedTestBase() {

    override fun createFileServer(): IFileServer {
        return JavaStreamFileServer(SocketHandler(serializer))
    }

}


fun main(args: Array<String>) {
    val serverSpeedTest = JavaStreamFileServerSpeedTest()

    serverSpeedTest.runServer()
}