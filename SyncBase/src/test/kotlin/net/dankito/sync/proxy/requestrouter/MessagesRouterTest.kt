package net.dankito.sync.proxy.requestrouter

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class MessagesRouterTest : RouterTestBase() {
    
    companion object {
        private const val MessengerPort = 12345
    }
    

    private val underTest = MessagesRouter(MessengerPort)


    @Test
    fun relativeUrlStartsWithMessages_MessagesGetsRemoved() {
        // given
        val httpObject = createRequest("/messages/any/sub/url")
        assertThat(httpObject.uri()).startsWith("/messages")

        // when
        val didRoute = underTest.routeRemoteRequestToLocalService(httpObject)

        // then
        assertThat(didRoute).isTrue()
        assertThat(httpObject.uri()).doesNotContain("/messages")
        assertThat(httpObject.uri()).isEqualTo("http://127.0.0.1:$MessengerPort/any/sub/url")
    }

    @Test
    fun relativeUrlDoesNotStartWithMessages_NoChanges() {
        // given
        val httpObject = createRequest("/any/sub/url")
        assertThat(httpObject.uri()).doesNotStartWith("/messages")

        // when
        val didRoute = underTest.routeRemoteRequestToLocalService(httpObject)

        // then
        assertThat(didRoute).isFalse()
        assertThat(httpObject.uri()).doesNotStartWith("/messages")
        assertThat(httpObject.uri()).isEqualTo("/any/sub/url")
    }


    @Test
    fun httpLocalUrlWithoutMessages_MessagesGetsAdded() {
        // given
        val httpObject = createRequest("http://127.0.0.1:12345/any/sub/url")
        assertThat(httpObject.uri()).doesNotContain("/messages")

        // when
        val didRoute = underTest.routeLocalRequestToRemote(httpObject)

        // then
        assertThat(didRoute).isTrue()
        assertThat(httpObject.uri()).contains("/messages")
        assertThat(httpObject.uri()).isEqualTo("http://127.0.0.1:12345/messages/any/sub/url")
    }

    @Test
    fun httpsUpperCaseLocalUrlWithoutMessages_MessagesGetsAdded() {
        // given
        val httpObject = createRequest("HTTPS://127.0.0.1:12345/any/sub/url")
        assertThat(httpObject.uri()).doesNotContain("/messages")

        // when
        val didRoute = underTest.routeLocalRequestToRemote(httpObject)

        // then
        assertThat(didRoute).isTrue()
        assertThat(httpObject.uri()).contains("/messages")
        assertThat(httpObject.uri()).isEqualTo("HTTPS://127.0.0.1:12345/messages/any/sub/url")
    }

    @Test
    fun httpLocalUrlWithMessages_NoChanges() {
        // given
        val httpObject = createRequest("http://127.0.0.1:12345/messages/any/sub/url")
        assertThat(httpObject.uri()).contains("/messages")

        // when
        val didRoute = underTest.routeLocalRequestToRemote(httpObject)

        // then
        assertThat(didRoute).isFalse()
        assertThat(httpObject.uri()).contains("/messages")
        assertThat(httpObject.uri()).isEqualTo("http://127.0.0.1:12345/messages/any/sub/url")
    }

}