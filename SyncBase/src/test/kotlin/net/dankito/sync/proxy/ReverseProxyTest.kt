package net.dankito.sync.proxy

import fi.iki.elonen.NanoHTTPD
import net.dankito.sync.messenger.NanoHttpdMessenger
import net.dankito.sync.messenger.http.HttpMethod
import net.dankito.sync.messenger.http.HttpResponse
import net.dankito.sync.messenger.http.HttpResponseStatus
import net.dankito.sync.messenger.http.UrlToMessageBodyMapperBase
import net.dankito.sync.proxy.requestrouter.FilesRouter
import net.dankito.sync.proxy.requestrouter.MessagesRouter
import net.dankito.utils.serialization.JacksonJsonSerializer
import okhttp3.OkHttpClient
import okhttp3.Request
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.net.InetSocketAddress
import java.net.Proxy

class ReverseProxyTest {

    companion object {
        private const val ProxyPort = 12345

        private const val MessagesPort = 12346
        private const val FileSynchronizationPort = MessagesPort
    }


    private val receivedMessages = mutableListOf<Any>()

    private val receivedFileRequests = mutableListOf<Any>()

    private val messenger = object : NanoHttpdMessenger(object : UrlToMessageBodyMapperBase() { },
            JacksonJsonSerializer(), { _, _, _ -> HttpResponse(HttpResponseStatus.OK) }) {
        override fun determineResponse(subUrl: String, method: HttpMethod, receivedMessage: String?): String {
            receivedMessages.add(subUrl)
            return ""
        }

        override fun respondToFileRequest(uri: String): NanoHTTPD.Response {
            receivedFileRequests.add(uri)
            return super.respondToFileRequest(uri)
        }
    }

    private val underTest = ReverseProxy()


    @Before
    fun setUp() {
        underTest.addRouter(FilesRouter(FileSynchronizationPort))
        underTest.addRouter(MessagesRouter(MessagesPort))

        underTest.start(ProxyPort)

        messenger.start(MessagesPort)
    }

    @After
    fun tearDown() {
        underTest.close()

        messenger.stop()
    }


    @Test
    fun routeRemoteMessagesRequest() {
        // when
        sendRequest("/v1/messages/any/sub/url")

        // then
        assertThat(receivedMessages).contains("any/sub/url")
        assertThat(receivedFileRequests).isEmpty()
    }

    @Test
    fun routeLocalMessagesRequest() {
        // when
        sendRequest("/any/sub/url", true)

        // then
        assertThat(receivedMessages).contains("any/sub/url")
        assertThat(receivedFileRequests).isEmpty()
    }


    @Test
    fun routeRemoteFileSynchronizationRequest() {
        // when
        sendRequest("/v1/files/fileId")

        // then
        assertThat(receivedMessages).isEmpty()
        assertThat(receivedFileRequests).contains("files/fileId")
    }

    @Test
    fun routeLocalFileSynchronizationRequest() {
        // when
        sendRequest("/files/fileId", true)

        // then
        assertThat(receivedMessages).isEmpty()
        assertThat(receivedFileRequests).contains("files/fileId")
    }


    private fun sendRequest(subUrl: String) {
        sendRequest(subUrl, false)
    }

    private fun sendRequest(subUrl: String, useProxy: Boolean) {
        val clientBuilder = OkHttpClient.Builder()

        if (useProxy) {
            clientBuilder.proxy(Proxy(Proxy.Type.HTTP, InetSocketAddress("127.0.0.1", ProxyPort)))
        }

        val client = clientBuilder.build()


        val request = Request.Builder()
                .url("http://127.0.0.1:$ProxyPort" + subUrl)
                .build()

        client.newCall(request).execute()
    }
}