package net.dankito.sync.proxy.requestrouter

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class FilesRouterTest : RouterTestBase() {
    
    companion object {
        private const val FileSynchronizationPort = 12345
    }
    

    private val underTest = FilesRouter(FileSynchronizationPort)


    @Test
    fun relativeUrlStartsWithFiles_FilesGetsRemoved() {
        // given
        val httpObject = createRequest("/files/any/sub/url")
        assertThat(httpObject.uri()).startsWith("/files")

        // when
        val didRoute = underTest.routeRemoteRequestToLocalService(httpObject)

        // then
        assertThat(didRoute).isTrue()
        assertThat(httpObject.uri()).contains("/files")
        assertThat(httpObject.uri()).isEqualTo("http://127.0.0.1:$FileSynchronizationPort/files/any/sub/url")
    }

    @Test
    fun relativeUrlDoesNotStartWithFiles_NoChanges() {
        // given
        val httpObject = createRequest("/any/sub/url")
        assertThat(httpObject.uri()).doesNotStartWith("/files")

        // when
        val didRoute = underTest.routeRemoteRequestToLocalService(httpObject)

        // then
        assertThat(didRoute).isFalse()
        assertThat(httpObject.uri()).doesNotStartWith("/files")
        assertThat(httpObject.uri()).isEqualTo("/any/sub/url")
    }


    @Test
    fun httpLocalUrlWithFiles_RoutesButNothingGetsAdded() {
        // given
        val httpObject = createRequest("http://127.0.0.1:12345/files/any/sub/url")
        assertThat(httpObject.uri()).contains("/files")

        // when
        val didRoute = underTest.routeLocalRequestToRemote(httpObject)

        // then
        assertThat(didRoute).isTrue()
        assertThat(httpObject.uri()).contains("/files")
        assertThat(httpObject.uri()).isEqualTo("http://127.0.0.1:12345/files/any/sub/url")
    }

    @Test
    fun httpsUpperCaseLocalUrlWithFiles_RoutesButNothingGetsAdded() {
        // given
        val httpObject = createRequest("HTTPS://127.0.0.1:12345/files/any/sub/url")
        assertThat(httpObject.uri()).contains("/files")

        // when
        val didRoute = underTest.routeLocalRequestToRemote(httpObject)

        // then
        assertThat(didRoute).isTrue()
        assertThat(httpObject.uri()).contains("/files")
        assertThat(httpObject.uri()).isEqualTo("HTTPS://127.0.0.1:12345/files/any/sub/url")
    }

}