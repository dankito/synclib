package net.dankito.sync.proxy.requestrouter

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class V1RouterTest : RouterTestBase() {

    private val underTest = V1Router()


    @Test
    fun relativeUrlStartsWithV1_V1GetsRemoved() {
        // given
        val httpObject = createRequest("/v1/any/sub/url")
        assertThat(httpObject.uri()).startsWith("/v1")

        // when
        val didRoute = underTest.routeRemoteRequestToLocalService(httpObject)

        // then
        assertThat(didRoute).isTrue()
        assertThat(httpObject.uri()).doesNotStartWith("/v1")
    }

    @Test
    fun relativeUrlDoesNotStartWithV1_NoChanges() {
        // given
        val httpObject = createRequest("/any/sub/url")
        assertThat(httpObject.uri()).doesNotStartWith("/v1")

        // when
        val didRoute = underTest.routeRemoteRequestToLocalService(httpObject)

        // then
        assertThat(didRoute).isFalse()
        assertThat(httpObject.uri()).doesNotStartWith("/v1")
    }


    @Test
    fun httpLocalUrlWithoutV1_V1GetsAdded() {
        // given
        val httpObject = createRequest("http://127.0.0.1:12345/any/sub/url")
        assertThat(httpObject.uri()).doesNotContain("/v1")

        // when
        val didRoute = underTest.routeLocalRequestToRemote(httpObject)

        // then
        assertThat(didRoute).isTrue()
        assertThat(httpObject.uri()).contains("/v1")
        assertThat(httpObject.uri()).isEqualTo("http://127.0.0.1:12345/v1/any/sub/url")
    }

    @Test
    fun httpsUpperCaseLocalUrlWithoutV1_V1GetsAdded() {
        // given
        val httpObject = createRequest("HTTPS://127.0.0.1:12345/any/sub/url")
        assertThat(httpObject.uri()).doesNotContain("/v1")

        // when
        val didRoute = underTest.routeLocalRequestToRemote(httpObject)

        // then
        assertThat(didRoute).isTrue()
        assertThat(httpObject.uri()).contains("/v1")
        assertThat(httpObject.uri()).isEqualTo("HTTPS://127.0.0.1:12345/v1/any/sub/url")
    }

    @Test
    fun httpLocalUrlWithV1_NoChanges() {
        // given
        val httpObject = createRequest("http://127.0.0.1:12345/v1/any/sub/url")
        assertThat(httpObject.uri()).contains("/v1")

        // when
        val didRoute = underTest.routeLocalRequestToRemote(httpObject)

        // then
        assertThat(didRoute).isFalse()
        assertThat(httpObject.uri()).contains("/v1")
        assertThat(httpObject.uri()).isEqualTo("http://127.0.0.1:12345/v1/any/sub/url")
    }

}