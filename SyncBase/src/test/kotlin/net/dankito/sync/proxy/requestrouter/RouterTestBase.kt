package net.dankito.sync.proxy.requestrouter

import io.netty.handler.codec.http.DefaultHttpRequest
import io.netty.handler.codec.http.HttpMethod
import io.netty.handler.codec.http.HttpVersion

abstract class RouterTestBase {

    fun createRequest(uri: String): DefaultHttpRequest {
        return DefaultHttpRequest(HttpVersion.HTTP_1_1, HttpMethod.GET, uri)
    }

}