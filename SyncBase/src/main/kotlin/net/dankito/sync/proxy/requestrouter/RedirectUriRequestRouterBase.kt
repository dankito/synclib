package net.dankito.sync.proxy.requestrouter

import io.netty.handler.codec.http.HttpRequest


abstract class RedirectUriRequestRouterBase(
        protected val subUrl: String,
        /**
         * If request should be redirected locally to another port
         */
        protected val localPort: Int? = null
) : IRequestRouter {

    override fun routeLocalRequestToRemote(request: HttpRequest): Boolean {
        val uri = request.uri()

        val pathStartIndex = getPathStartIndex(uri)

        // a request from local service that sets ReverseProxy as proxy (uri therefore contains remote ip and port)
        if (uri.substring(pathStartIndex).startsWith("/$subUrl/") == false) { // -> add subUrl and send to remote
            val remoteUri = uri.substring(0, pathStartIndex) + "/" + subUrl + uri.substring(pathStartIndex)
            request.uri = remoteUri

            return true
        }

        return false
    }

    protected open fun getPathStartIndex(uri: String): Int {
        val schemeSeparatorIndex = uri.indexOf("://")

        return uri.indexOf('/', schemeSeparatorIndex + 3)
    }


    override fun routeRemoteRequestToLocalService(request: HttpRequest): Boolean {
        val uri = request.uri()

        // a request retrieved from remote endpoint (uri therefore does not contain ip and port)
        if (uri.startsWith("/$subUrl/")) {// -> remove subUrl and send to local service port
            var localUri = uri.substring(subUrl.length + 1)

            localUri = mayAdjustLocalUriPort(localUri, localPort)

            request.uri = localUri

            return true
        }

        return false
    }

    protected open fun mayAdjustLocalUriPort(localUri: String, localPort: Int?): String {
        if (localPort != null) { // if localPort is set, adjust uri
            return "http://127.0.0.1:${this.localPort}" + localUri
        }

        return localUri // otherwise leave localUri untouched
    }

}