package net.dankito.sync.proxy.requestrouter

import io.netty.handler.codec.http.HttpRequest


class FilesRouter(fileSynchronizationPort: Int) : RedirectUriRequestRouterBase(SubUrl, fileSynchronizationPort) {

    companion object {
        const val SubUrl = "files"
    }


    override fun routeLocalRequestToRemote(request: HttpRequest): Boolean {
        val uri = request.uri()

        val pathStartIndex = getPathStartIndex(uri)

        if (uri.substring(pathStartIndex).startsWith("/$subUrl/")) { // it's a files url
            // nothing to do, url is already correct, but return true so that no other Router changes it
            return true
        }

        return false
    }

    override fun routeRemoteRequestToLocalService(request: HttpRequest): Boolean {
        val uri = request.uri()

        if (uri.startsWith("/$subUrl/")) {
            val localUri = mayAdjustLocalUriPort(uri, localPort) // don't remove '/files', just route to local FilesSynchronizationPort

            request.uri = localUri

            return true
        }

        return false
    }

}