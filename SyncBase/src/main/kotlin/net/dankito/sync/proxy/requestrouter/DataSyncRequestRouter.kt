package net.dankito.sync.proxy.requestrouter

import io.netty.handler.codec.http.HttpRequest


class DataSyncRequestRouter(val databaseName: String, val synchronizationPort: Int) : IRequestRouter {

    companion object {
        const val SubUrl = "sync"
    }


    override fun routeLocalRequestToRemote(request: HttpRequest): Boolean {
        if (request is HttpRequest) {
            // a request from local CouchbaseLiteSyncManager that sets ReverseProxy as proxy (uri therefore contains remote ip and port)
            if (request.uri().contains("/$databaseName/")) { // -> add SubUrl and send to remote
                val remoteUri = request.uri().replace("/$databaseName/", "/$SubUrl/$databaseName/")
                request.uri = remoteUri

                return true
            }
        }

        return false
    }

    override fun routeRemoteRequestToLocalService(request: HttpRequest): Boolean {
        // a request retrieved from remote endpoint (uri therefore does not contain ip and port)
        if (request.uri().startsWith("/$SubUrl/$databaseName/")) {// -> remove SubUrl and send to CouchbaseLiteSyncManager port
            val localUri = "http://127.0.0.1:$synchronizationPort" + request.uri().substring(SubUrl.length + 1)
            request.uri = localUri

            return true
        }

        return false
    }

}