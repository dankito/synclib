package net.dankito.sync.proxy.requestrouter


class V1Router : RedirectUriRequestRouterBase(SubUrl) {

    companion object {
        const val SubUrl = "v1"
    }

}