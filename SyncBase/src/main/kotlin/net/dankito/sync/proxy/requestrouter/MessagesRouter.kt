package net.dankito.sync.proxy.requestrouter


class MessagesRouter(messengerPort: Int) : RedirectUriRequestRouterBase(SubUrl, messengerPort) {

    companion object {
        const val SubUrl = "messages"
    }


    override fun mayAdjustLocalUriPort(localUri: String, localPort: Int?): String {
        return "http://127.0.0.1:${this.localPort}" + "/" + SubUrl + localUri
    }

}