package net.dankito.sync.proxy.requestrouter

import io.netty.handler.codec.http.HttpRequest


interface IRequestRouter {

    fun routeLocalRequestToRemote(request: HttpRequest): Boolean

    fun routeRemoteRequestToLocalService(request: HttpRequest): Boolean

}