package net.dankito.sync.proxy

import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.http.HttpObject
import io.netty.handler.codec.http.HttpRequest
import io.netty.handler.codec.http.HttpResponse
import net.dankito.sync.proxy.requestrouter.IRequestRouter
import net.dankito.sync.proxy.requestrouter.V1Router
import org.littleshoot.proxy.HttpFilters
import org.littleshoot.proxy.HttpFiltersAdapter
import org.littleshoot.proxy.HttpFiltersSourceAdapter
import org.littleshoot.proxy.HttpProxyServer
import org.littleshoot.proxy.impl.DefaultHttpProxyServer
import org.slf4j.LoggerFactory
import java.net.InetSocketAddress


open class ReverseProxy {

    companion object {
        const val RemoteAddressHeaderName = "X-Remote-Address"
        const val RemotePortHeaderName = "X-Remote-Port"

        private val StartsWithHttpRegexMatcher = Regex("http[s]?://.*", RegexOption.IGNORE_CASE)

        private val log = LoggerFactory.getLogger(ReverseProxy::class.java)
    }


    private var proxy: HttpProxyServer? = null

    private val versionRouter = V1Router()

    private val routers = mutableListOf<IRequestRouter>()


    fun start(proxyPort: Int) {
        proxy = DefaultHttpProxyServer.bootstrap()
                .withPort(proxyPort)
                .withAllowLocalOnly(false)
                .withFiltersSource(requestFilter)
                .start()
    }

    fun close() {
        proxy?.stop()

        proxy = null
    }


    private val requestFilter = object : HttpFiltersSourceAdapter() {
        override fun filterRequest(originalRequest: HttpRequest, ctx: ChannelHandlerContext): HttpFilters {
            return object : HttpFiltersAdapter(originalRequest) {

                override fun clientToProxyRequest(httpObject: HttpObject): HttpResponse? {
                    if (httpObject is HttpRequest) {
                        if (isLocalRequest(httpObject)) {
                            routeLocalRequestToRemote(httpObject)
                        }
                        else {
                            setRemoteAddressHeader(httpObject, ctx)

                            routeRemoteRequestToLocalService(httpObject)
                        }
                    }

                    return super.clientToProxyRequest(httpObject)
                }

            }
        }
    }


    protected open fun isLocalRequest(httpObject: HttpRequest) = httpObject.uri().matches(StartsWithHttpRegexMatcher)

    protected open fun routeLocalRequestToRemote(request: HttpRequest) {
        for (router in routers) {
            if (router.routeLocalRequestToRemote(request)) {
                break // request got routed, don't call other routers
            }
        }

        // TODO: or return 404 if no router handles request?

        versionRouter.routeLocalRequestToRemote(request) // adds '/v1' to url
    }

    protected open fun routeRemoteRequestToLocalService(request: HttpRequest): HttpResponse? {
        versionRouter.routeRemoteRequestToLocalService(request) // removes '/v1' from url

        for (router in routers) {
            if (router.routeRemoteRequestToLocalService(request)) {
                break // request got routed, don't call other routers
            }
        }

        // TODO: or return 404 if no router handles request?
        return null
    }

    protected open fun setRemoteAddressHeader(httpObject: HttpRequest, ctx: ChannelHandlerContext) {
        try {
            val remoteSocketAddress = ctx.channel().remoteAddress()

            var remoteHostname = remoteSocketAddress.toString()
            var remotePort = -1

            if (remoteSocketAddress is InetSocketAddress) {
                remoteHostname = remoteSocketAddress.address.toString()
                remotePort = remoteSocketAddress.port
            }

            if (remoteHostname.startsWith('/')) {
                remoteHostname = remoteHostname.substring(1)
            }

            if (remoteHostname.contains(':')) { // hostname and port are separated by a colon
                val indexOfColon = remoteHostname.indexOf(':')
                try {
                    remotePort = remoteHostname.substring(indexOfColon + 1).toInt()
                } catch (e: Exception) {
                    log.info("Could not get port from $remoteHostname from index $indexOfColon on")
                }
                remoteHostname = remoteHostname.substring(0, indexOfColon)

            }

            httpObject.headers().set(RemoteAddressHeaderName, remoteHostname)
            httpObject.headers().set(RemotePortHeaderName, remotePort)
        } catch (e: Exception) {
            log.error("Could not set remote address header from remoteAddress '${ctx.channel().remoteAddress()}'", e)
        }
    }


    open fun addRouter(router: IRequestRouter) {
        routers.add(router)
    }

    open fun removeRouter(router: IRequestRouter) {
        routers.remove(router)
    }

}