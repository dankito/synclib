package net.dankito.sync.messenger.http


interface HttpResponseCreator {

    fun getResponse(subUrl: String, method: HttpMethod, requestBody: Any?): HttpResponse?

}