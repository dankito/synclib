package net.dankito.sync.messenger.http


open class HttpMessageBuilderBase() {

    companion object {

        val BodylessNullObject = Bodyless()

    }


    fun getBodylessMessage(method: HttpMethod, subUrl: String): HttpMessage<Bodyless> {
        return HttpMessage(method, subUrl, Bodyless::class.java, BodylessNullObject)
    }

    fun getStringBodyMessage(method: HttpMethod, subUrl: String, body: String): HttpMessage<String> {
        return HttpMessage(method, subUrl, String::class.java, body)
    }


    class Bodyless { }

}