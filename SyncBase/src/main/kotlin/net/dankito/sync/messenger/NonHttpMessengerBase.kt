package net.dankito.sync.messenger

import net.dankito.utils.serialization.ISerializer
import org.slf4j.LoggerFactory


abstract class NonHttpMessengerBase(serializer: ISerializer, protected val messageReceivedListener: MessageReceivedListener)
    : MessengerBase(serializer) {


    companion object {
        const val MessageTypeStart = "<|" // a combination most serialization formats (JSON, XML) don't start with
        const val MessageTypeEnd = "|>"

        private val log = LoggerFactory.getLogger(NonHttpMessengerBase::class.java)
    }



    protected open fun determineResponse(receivedMessage: String?): String {
        try {
            receivedMessage?.let {
                val messageBody: Any = tryToDeserializeMessageBody(receivedMessage)

                return determineResponse(receivedMessage, messageBody)
            }
        } catch (e: Exception) {
            log.error("Could not determine or deserialize response to '$receivedMessage'", e)
        }

        return CouldNotDetermineResponseResponse
    }

    protected open fun determineResponse(receivedMessage: String?, messageBody: Any): String {
        val response = messageReceivedListener.received(messageBody)

        response?.let {
            return serializeMessageToString(response)
        }

        return CouldNotDetermineResponseResponse
    }


    protected open fun tryToDeserializeMessageBody(receivedMessage: String): Any {
        if (receivedMessage.startsWith(MessageTypeStart) && receivedMessage.contains(MessageTypeEnd)) {
            val typeEndIndex = receivedMessage.indexOf(MessageTypeEnd)
            val typeString = receivedMessage.substring(
                    receivedMessage.indexOf(MessageTypeStart) + MessageTypeStart.length, typeEndIndex)
            try {
                val messageType = Class.forName(typeString)
                val serializedMessageBody = receivedMessage.substring(typeEndIndex + MessageTypeEnd.length)

                return deserializeMessage(serializedMessageBody, messageType)
            } catch (e: Exception) {
                log.error("Could not get message body's class from type string $typeString", e)
            }
        }

        return receivedMessage
    }

    protected open fun <TRequest : Any> serializeMessageToStringAndAddType(message: MessageEnvelop<TRequest>): String {
        if (message.bodyType == String::class.java.canonicalName && message.body is String) {
            return message.body
        }

        return MessageTypeStart + message.bodyType + MessageTypeEnd + serializeMessageToString(message.body)
    }

}