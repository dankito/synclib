package net.dankito.sync.messenger.http


interface IUrlToMessageBodyMapper {

    fun getMessageBodyForSubUrl(subUrl: String, method: HttpMethod): Class<*>?

    @Throws(IllegalArgumentException::class)
    fun getSubUrlForMessageBody(messageBodyClass: Class<*>, method: HttpMethod): String

}