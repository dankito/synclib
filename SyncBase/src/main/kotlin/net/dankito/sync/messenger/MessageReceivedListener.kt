package net.dankito.sync.messenger


interface MessageReceivedListener {

    fun received(message: Any): Any?

}