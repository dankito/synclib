package net.dankito.sync.messenger


open class MessageEnvelop<TBody>(val bodyType: String, val body: TBody) {

    companion object {

        fun getBodyTypeFromClass(bodyTypeClass: Class<*>): String {
            return bodyTypeClass.canonicalName
        }

    }


    constructor(bodyTypeClass: Class<TBody>, body: TBody) : this(getBodyTypeFromClass(bodyTypeClass), body)


    override fun toString(): String {
        return "$bodyType: $body"
    }

}