package net.dankito.sync.messenger

import net.dankito.utils.serialization.ISerializer


abstract class MessengerBase(protected val serializer: ISerializer) : IMessenger {

    companion object {
        const val ReceivedEmptyMessageResponse = "Empty received message"
        const val CouldNotDetermineResponseResponse = ""
    }


    protected open fun serializeMessageToString(message: Any): String {
        if (message is String) {
            return message
        }

        return serializer.serializeObject(message)
    }

    protected open fun <TResponse> deserializeMessage(response: String, responseClass: Class<TResponse>): TResponse {
        if (responseClass == String::class.java) {
            return response as TResponse
        }

        return serializer.deserializeObject(response, responseClass)
    }

}