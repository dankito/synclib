package net.dankito.sync.messenger.http


open class UrlToMessageBodyMapperBase(protected val urlToMessageBodyMap: Map<String, Map<HttpMethod, Class<*>>>)
    : IUrlToMessageBodyMapper {

    constructor() : this(emptyMap())


    override fun getMessageBodyForSubUrl(subUrl: String, method: HttpMethod): Class<*>? {
        return urlToMessageBodyMap[subUrl]?.get(method)
    }

    @Throws(IllegalArgumentException::class)
    override fun getSubUrlForMessageBody(messageBodyClass: Class<*>, method: HttpMethod): String {
        urlToMessageBodyMap.entries.forEach { entry ->
            if (entry.value[method] == messageBodyClass) {
                return entry.key
            }
        }

        throw IllegalArgumentException("Could not find sub url for message body type '$messageBodyClass' and http " +
                "method $method. Registered values are:" +
                urlToMessageBodyMap.values.flatMap { it.entries }.map { "\n${it.key}: ${it.value}" })
    }

}