package net.dankito.sync.messenger.http


class HttpResponse(val status: HttpResponseStatus,
                   val responseBody: Any? = null) {

    override fun toString(): String {
        return "$status: $responseBody"
    }

}