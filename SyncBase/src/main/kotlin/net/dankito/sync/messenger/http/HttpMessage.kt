package net.dankito.sync.messenger.http

import net.dankito.sync.messenger.MessageEnvelop


open class HttpMessage<TBody>(val method: HttpMethod, val subUrl: String, bodyTypeClass: Class<TBody>, body: TBody)
    : MessageEnvelop<TBody>(getBodyTypeFromClass(bodyTypeClass), body)