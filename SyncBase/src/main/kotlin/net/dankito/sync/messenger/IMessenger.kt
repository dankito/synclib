package net.dankito.sync.messenger

import net.dankito.sync.Endpoint
import net.dankito.sync.Response
import kotlin.concurrent.thread


interface IMessenger {

    fun start(port: Int): Boolean

    fun stop(): Boolean

    fun <TRequest : Any, TResponse> sendMessageAsync(endpoint: Endpoint, message: MessageEnvelop<TRequest>,
                                                responseClass: Class<TResponse>, callback: (Response<TResponse>) -> Unit) {
        thread { // TODO: use thread pool or coroutines
            callback(sendMessage(endpoint, message, responseClass))
        }
    }

    fun <TRequest : Any, TResponse> sendMessage(endpoint: Endpoint, message: MessageEnvelop<TRequest>,
                                                responseClass: Class<TResponse>) : Response<TResponse>

}