package net.dankito.sync.messenger.http


enum class HttpMethod {

    Get,
    Post,
    // TODO: implemente PUT and DELETE in NanoHttpdMessenger
//    Put,
//    Delete

}