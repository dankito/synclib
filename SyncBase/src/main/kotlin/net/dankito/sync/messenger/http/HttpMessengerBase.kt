package net.dankito.sync.messenger.http

import net.dankito.sync.messenger.MessengerBase
import net.dankito.utils.serialization.ISerializer
import org.slf4j.LoggerFactory


abstract class HttpMessengerBase(protected val urlToMessageBodyMapper: IUrlToMessageBodyMapper,
                                 serializer: ISerializer, protected val responseCreator: HttpResponseCreator)
    : MessengerBase(serializer) {


    companion object {
        private val log = LoggerFactory.getLogger(HttpMessengerBase::class.java)
    }


    protected open fun determineResponse(subUrl: String, method: HttpMethod, receivedMessage: String?): String {
        urlToMessageBodyMapper.getMessageBodyForSubUrl(subUrl, method)?.let { bodyTypeClass ->
            return determineResponse(subUrl, method, receivedMessage, bodyTypeClass)
        }

        return CouldNotDetermineResponseResponse
    }

    protected open fun determineResponse(subUrl: String, method: HttpMethod, serializedReceivedMessage: String?,
                                         messageBodyClass: Class<*>): String {
        try {
            deserializeReceivedMessage(method, serializedReceivedMessage, messageBodyClass)?.let { receivedMessage ->
                return determineResponse(subUrl, method, serializedReceivedMessage, receivedMessage)
            }
        } catch (e: Exception) {
            log.error("Could not create $messageBodyClass from received message '$serializedReceivedMessage'", e)
        }

        return CouldNotDetermineResponseResponse
    }

    protected open fun determineResponse(subUrl: String, method: HttpMethod, receivedMessage: String?, messageBody: Any): String {
        val response = responseCreator.getResponse(subUrl, method, messageBody)

        response?.let {
            response.responseBody?.let { responseBody ->
                return serializeMessageToString(responseBody)
            }
        }

        return CouldNotDetermineResponseResponse
    }

    protected open fun deserializeReceivedMessage(method: HttpMethod, receivedMessage: String?, messageBodyClass: Class<*>): Any? {
        receivedMessage?.let {
            return deserializeMessage(receivedMessage, messageBodyClass)
        }

        if (method == HttpMethod.Get) { // for GET requests it's perfectly legal that receivedMessage is null
            return messageBodyClass.newInstance()
        }

        return null
    }

}