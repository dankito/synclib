package net.dankito.sync.security.transport

import java.net.ServerSocket
import java.net.Socket


interface ISocketFactory {

    fun generateTcpSocket(address: String, port: Int): Socket


    fun generateTcpServerSocket(port: Int): ServerSocket

}