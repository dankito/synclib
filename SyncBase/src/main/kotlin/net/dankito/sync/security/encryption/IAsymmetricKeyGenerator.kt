package net.dankito.sync.security.encryption

import java.security.KeyPair
import java.security.Provider
import java.security.spec.AlgorithmParameterSpec


interface IAsymmetricKeyGenerator {

    fun generatePublicPrivateKeyPair(): KeyPair

    fun generatePublicPrivateKeyPair(provider: Provider?): KeyPair

    fun generatePublicPrivateKeyPair(provider: Provider?, parameterSpec: AlgorithmParameterSpec): KeyPair

}