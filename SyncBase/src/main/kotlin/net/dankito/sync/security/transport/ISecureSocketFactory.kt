package net.dankito.sync.security.transport

import java.net.ServerSocket
import java.net.Socket
import java.security.KeyPair
import java.security.PublicKey


interface ISecureSocketFactory : ISocketFactory {

    fun generateTcpSocket(address: String, port: Int, ownKeys: List<KeyPair>?, trustedKeys: List<PublicKey>?): Socket


    fun generateTcpServerSocket(port: Int, ownKeys: List<KeyPair>?, trustedKeys: List<PublicKey>?): ServerSocket

}