package net.dankito.sync


open class Endpoint(val address: String,
                    val messengerPort: Int,
                    val fileSyncPort: Int = messengerPort
) {

    protected constructor() : this("", 0)


    override fun toString(): String {
        return "$address:$messengerPort (fileSyncPort = $fileSyncPort)"
    }

}