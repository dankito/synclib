package net.dankito.sync


open class Response<TBody>(val isSuccessful: Boolean,
                           val error: Exception? = null,
                           val body: TBody? = null
) {

    override fun toString(): String {
        return "isSuccessful = $isSuccessful, error = $error, body = $body"
    }

}