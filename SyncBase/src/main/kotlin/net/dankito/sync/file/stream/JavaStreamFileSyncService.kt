package net.dankito.sync.file.stream

import net.dankito.sync.Endpoint
import net.dankito.sync.file.IFileSyncService
import net.dankito.sync.file.RequestSyncFile
import net.dankito.sync.file.RequestSyncFileResponse
import org.slf4j.LoggerFactory
import java.io.BufferedInputStream
import java.io.DataInputStream
import java.io.File
import java.io.FileOutputStream
import java.net.Socket


open class JavaStreamFileSyncService(protected val socketHandler: SocketHandler) : IFileSyncService {

    companion object {
        private val log = LoggerFactory.getLogger(JavaStreamFileSyncService::class.java)
    }


    override fun retrieveFile(endpoint: Endpoint, fileId: String, destinationFile: File) {
        var totalBytesRead = 0L

        Socket(endpoint.address, endpoint.fileSyncPort).use { clientSocket ->

            socketHandler.sendMessage(clientSocket, RequestSyncFile(fileId))

            val requestFileSyncResponse = socketHandler.receiveMessage(clientSocket, RequestSyncFileResponse::class.java, 100)

            if (requestFileSyncResponse != null && requestFileSyncResponse.permitted) {

                if (requestFileSyncResponse.permitted) {
                    socketHandler.sendMessage(clientSocket, SocketHandler.MessageEndChar.toString())

                    FileOutputStream(destinationFile).buffered().use { outputStream ->

                        val inputStream = DataInputStream(BufferedInputStream(clientSocket.getInputStream()))

                        val buffer = ByteArray(SocketHandler.BufferSize)

                        var receivedChunkSize: Int

                        do {
                            receivedChunkSize = inputStream.read(buffer, 0, buffer.size)

                            if(receivedChunkSize > 0) {
                                totalBytesRead += receivedChunkSize

                                outputStream.write(buffer, 0, receivedChunkSize)
                            }
                        } while(receivedChunkSize >= 0)

                        outputStream.flush()
                    }
                }
            }
        }

        log.info("Read $totalBytesRead from ${endpoint.address}")
    }
}