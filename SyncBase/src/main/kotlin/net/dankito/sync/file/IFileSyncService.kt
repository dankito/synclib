package net.dankito.sync.file

import net.dankito.sync.Endpoint
import java.io.File


interface IFileSyncService {

    fun retrieveFile(endpoint: Endpoint, fileId: String, destinationFile: File)

}