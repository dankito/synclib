package net.dankito.sync.file.nio

import net.dankito.utils.serialization.ISerializer
import java.nio.ByteBuffer
import java.nio.channels.SocketChannel


open class SocketChannelHandler(protected val serializer: ISerializer) {

    companion object {
        val MessageCharset = Charsets.UTF_8
    }


    open fun sendMessage(socketChannel: SocketChannel, message: Any): Int {
        val serializedMessage = serializer.serializeObject(message)
        val messageBytes = serializedMessage.toByteArray(MessageCharset)

        val messageBuffer = ByteBuffer.allocate(messageBytes.size)
        messageBuffer.put(messageBytes)
        messageBuffer.flip()

        return socketChannel.write(messageBuffer)
    }

    open fun <T> receiveMessage(socketChannel: SocketChannel, messageClass: Class<T>, messageMaxSize: Int): T? {
        val messageBuffer = ByteBuffer.allocate(messageMaxSize)
        val bytesRead = socketChannel.read(messageBuffer)

        if (bytesRead > 0) {
            val serializedMessage = String(messageBuffer.array(), MessageCharset)

            return serializer.deserializeObject(serializedMessage, messageClass)
        }

        return null
    }

}