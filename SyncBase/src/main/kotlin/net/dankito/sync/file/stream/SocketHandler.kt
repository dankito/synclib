package net.dankito.sync.file.stream

import net.dankito.utils.serialization.ISerializer
import okio.Okio
import org.slf4j.LoggerFactory
import java.io.BufferedInputStream
import java.io.ByteArrayInputStream
import java.io.IOException
import java.io.InputStream
import java.net.Socket


open class SocketHandler(protected val serializer: ISerializer) {

    companion object {
        val MessageCharset = Charsets.UTF_8

        const val MessageEndChar = '\n'

        const val MaxMessageSize = 2 * 1024 * 1024

        const val BufferSize = 8 * 1024

        private val log = LoggerFactory.getLogger(SocketHandler::class.java)
    }


    open fun sendMessage(socket: Socket, message: Any): SocketResult {
        val serializedMessage = serializer.serializeObject(message)
        val messageBytes = serializedMessage.toByteArray(MessageCharset)

        return sendMessage(socket, messageBytes)
    }

    open fun sendMessage(socket: Socket, message: ByteArray): SocketResult {
        return sendMessage(socket, ByteArrayInputStream(message), MessageEndChar)
    }

    open fun sendMessage(socket: Socket, inputStream: InputStream, messageEndChar: Char? = null): SocketResult {
        var countBytesSend = -1L

        try {
            val sink = Okio.buffer(Okio.sink(socket))
            val source = Okio.buffer(Okio.source(BufferedInputStream(inputStream)))

            countBytesSend = sink.writeAll(source)

            messageEndChar?.let {
                sink.writeUtf8(it.toString()) // to signal receiver that message ends here
            }

            sink.flush()

            source.close()

            return SocketResult(true, countBytesSend = countBytesSend)
        } catch(e: Exception) {
            log.error("Could not send message to ${socket.inetAddress}", e)

            return SocketResult(false, e, countBytesSend = countBytesSend)
        }
    }


    open fun <T> receiveMessage(socket: Socket, messageClass: Class<T>, messageMaxSize: Int): T? {
        val receiveResult = receiveMessage(socket)

        if (receiveResult.isSuccessful) {
            receiveResult.receivedMessage?.let { serializedMessage ->
                return serializer.deserializeObject(serializedMessage, messageClass)
            }
        }

        return null
    }

    open fun receiveMessage(socket: Socket): SocketResult {
        try {
            val inputStream = BufferedInputStream(socket.getInputStream())

            // do not close inputStream, otherwise socket gets closed
            return receiveMessage(inputStream)
        } catch (e: Exception) {
            log.error("Could not receive response", e)

            return SocketResult(false, e)
        }

    }

    @Throws(IOException::class)
    open fun receiveMessage(inputStream: InputStream): SocketResult {
        val buffer = ByteArray(BufferSize)
        val receivedMessageBytes = ArrayList<Byte>()

        var receivedChunkSize: Int
        var receivedMessageSize: Int = 0

        do {
            receivedChunkSize = inputStream.read(buffer, 0, buffer.size)
            receivedMessageSize += receivedChunkSize

            if(receivedChunkSize > 0) {
                receivedMessageBytes.addAll(buffer.take(receivedChunkSize))

                if(buffer[receivedChunkSize - 1] == MessageEndChar.toByte()) {
                    break
                }
            }
        } while(receivedChunkSize > -1)

        if(receivedMessageSize > 0 && receivedMessageSize < MaxMessageSize) {
            val receivedMessage = String(receivedMessageBytes.toByteArray(), MessageCharset)
            return SocketResult(true, receivedMessage = receivedMessage)
        }
        else {
            if(receivedMessageSize <= 0) {
                return SocketResult(false, Exception("Could not receive any bytes"))
            }
            else {
                return SocketResult(false, Exception("Received message exceeds max message length of " + MaxMessageSize))
            }
        }
    }

}