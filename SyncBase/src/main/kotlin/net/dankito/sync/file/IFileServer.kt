package net.dankito.sync.file

import java.io.File


interface IFileServer {

    var fileForIdCallback: GetFileForIdCallback?

    /**
     * Convenience method for Kotlin users
     */
    fun setGetFileForIdCallback(callback: (fileId: String) -> File?) {
        fileForIdCallback = object : GetFileForIdCallback {

            override fun getFileForId(fileId: String): File? {
                return callback(fileId)
            }

        }
    }


    fun start(port: Int): Boolean

    fun stop(): Boolean

}