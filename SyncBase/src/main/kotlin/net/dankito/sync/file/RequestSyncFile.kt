package net.dankito.sync.file


open class RequestSyncFile(val fileId: String) {

    protected constructor() : this("")


    override fun toString(): String {
        return fileId
    }

}