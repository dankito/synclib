package net.dankito.sync.file

import java.io.File


interface GetFileForIdCallback {

    fun getFileForId(fileId: String): File?

}