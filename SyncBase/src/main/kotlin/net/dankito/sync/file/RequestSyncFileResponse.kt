package net.dankito.sync.file


open class RequestSyncFileResponse(val permitted: Boolean, val fileSize: Long) {

    companion object {
        const val FileSizeUnknown = -1L
    }


    protected constructor() : this(false, FileSizeUnknown)


    override fun toString(): String {
        return "Is permitted $permitted to synchronize file of size $fileSize"
    }

}