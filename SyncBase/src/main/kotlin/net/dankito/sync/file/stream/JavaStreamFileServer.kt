package net.dankito.sync.file.stream

import net.dankito.sync.file.GetFileForIdCallback
import net.dankito.sync.file.IFileServer
import net.dankito.sync.file.RequestSyncFile
import net.dankito.sync.file.RequestSyncFileResponse
import net.dankito.sync.file.RequestSyncFileResponse.Companion.FileSizeUnknown
import net.dankito.utils.ThreadPool
import org.slf4j.LoggerFactory
import java.io.*
import java.net.ServerSocket
import java.net.Socket
import kotlin.concurrent.thread


open class JavaStreamFileServer(protected val socketHandler: SocketHandler) : IFileServer {

    companion object {
        private val log = LoggerFactory.getLogger(JavaStreamFileServer::class.java)
    }


    protected var serverSocket: ServerSocket? = null

    protected var receiverThread: Thread? = null

    protected var threadPool = ThreadPool()

    override var fileForIdCallback: GetFileForIdCallback? = null


    override fun start(port: Int): Boolean {
        serverSocket = ServerSocket(port)

        serverSocket?.let { serverSocket ->
            serverSocket.reuseAddress = true

            startReceiverThread(serverSocket)

            return true
        }

        return false
    }

    override fun stop(): Boolean {
        serverSocket?.close()

        receiverThread?.join(200)

        serverSocket = null
        receiverThread = null

        return true
    }


    protected open fun startReceiverThread(serverSocket: ServerSocket) {
        receiverThread = thread {
            while (serverSocket.isClosed == false) {
                val clientSocket = serverSocket.accept()

                sendFileToClientAsync(clientSocket)
            }
        }
    }

    protected open fun sendFileToClientAsync(clientSocket: Socket) {
        threadPool.runAsync {
            sendFileToClient(clientSocket)

            clientSocket.close()
        }
    }

    protected open fun sendFileToClient(clientSocket: Socket) {
        val requestFileSync = socketHandler.receiveMessage(clientSocket, RequestSyncFile::class.java, 300)

        if (requestFileSync != null) {
            val file = fileForIdCallback?.getFileForId(requestFileSync.fileId)

            if (file != null) {
                socketHandler.sendMessage(clientSocket, RequestSyncFileResponse(true, file.length()))

                val waitForClientReady = socketHandler.receiveMessage(clientSocket) // wait till client is ready to receive file, otherwise we may would send bytes but the client is not in receiving state yet

                if (waitForClientReady.isSuccessful) {
                    sendFileToClient(clientSocket, file)
                }
            }
            else {
                socketHandler.sendMessage(clientSocket, RequestSyncFileResponse(false, FileSizeUnknown))
            }
        }
    }

    protected open fun sendFileToClient(clientSocket: Socket, file: File) {try {
        val inputStream = BufferedInputStream(FileInputStream(file))
        val outputStream = DataOutputStream(BufferedOutputStream(clientSocket.getOutputStream()))

        val buffer = ByteArray(SocketHandler.BufferSize)

        var sentChunkSize: Int
        var sentMessageSize = 0L

        do {
            sentChunkSize = inputStream.read(buffer, 0, buffer.size)
            sentMessageSize += sentChunkSize

            if(sentChunkSize > 0) {
                outputStream.write(buffer, 0, sentChunkSize)
            }
        } while (sentChunkSize > -1)

        outputStream.flush()
        outputStream.close()
        inputStream.close()

        log.info("Result of sending file $file = $sentMessageSize bytes sent")
    } catch (e: Exception) {
        log.error("Could not send file $file to client ${clientSocket.inetAddress}", e)
    }
    }

}