package net.dankito.sync.file.nio

import net.dankito.sync.Endpoint
import net.dankito.sync.file.IFileSyncService
import net.dankito.sync.file.RequestSyncFile
import net.dankito.sync.file.RequestSyncFileResponse
import org.slf4j.LoggerFactory
import java.io.File
import java.io.FileOutputStream
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.SocketChannel


/**
 * Even if using blocking (file operations are always blocking by the way) and not non-blocking mode,
 * Java NIO should be preferred to Java Stream due to its zero copy approach:
 * https://www.ibm.com/developerworks/library/j-zerocopy/
 *
 * Also this benchmark shows that NIO is faster:
 * https://baptiste-wicht.com/posts/2010/08/file-copy-in-java-benchmark.html
 *
 * More tips for improving speed / load:
 * https://stackoverflow.com/questions/17556901/java-high-load-nio-tcp-server
 */
open class JavaNioFileSyncService(protected val socketChannelHandler: SocketChannelHandler) : IFileSyncService {

    companion object {
        private val log = LoggerFactory.getLogger(JavaNioFileSyncService::class.java)
    }


    override fun retrieveFile(endpoint: Endpoint, fileId: String, destinationFile: File) {
        var totalBytesRead = 0L

        SocketChannel.open().use { socketChannel ->
            val address = InetSocketAddress(endpoint.address, endpoint.fileSyncPort)

            socketChannel.connect(address)
            socketChannel.configureBlocking(true)

            socketChannelHandler.sendMessage(socketChannel, RequestSyncFile(fileId))

            val requestFileSyncResponse = socketChannelHandler.receiveMessage(socketChannel, RequestSyncFileResponse::class.java, 100)

            if (requestFileSyncResponse != null && requestFileSyncResponse.permitted) {

                if (requestFileSyncResponse.permitted) {
                    val synchronizeBuffer = ByteBuffer.allocate(1)
                    synchronizeBuffer.put(0)
                    synchronizeBuffer.flip()
                    socketChannel.write(synchronizeBuffer)

                    FileOutputStream(destinationFile).channel.use { fileChannel ->
//                        totalBytesRead = fileChannel.transferFrom(socketChannel, 0, requestFileSyncResponse.fileSize)

                        val buffer = ByteBuffer.allocateDirect(8 * 1024)
                        while (socketChannel.read(buffer) != -1) {
                            buffer.flip()

                            while (buffer.hasRemaining()) {
                                totalBytesRead += fileChannel.write(buffer)
                            }

                            buffer.clear()
                        }

                        fileChannel.force(true)
                    }
                }
            }
        }

        log.info("Read $totalBytesRead from ${endpoint.address}")
    }
}