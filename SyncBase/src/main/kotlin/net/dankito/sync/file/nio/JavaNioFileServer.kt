package net.dankito.sync.file.nio

import net.dankito.sync.file.GetFileForIdCallback
import net.dankito.sync.file.IFileServer
import net.dankito.sync.file.RequestSyncFile
import net.dankito.sync.file.RequestSyncFileResponse
import net.dankito.sync.file.RequestSyncFileResponse.Companion.FileSizeUnknown
import net.dankito.utils.ThreadPool
import java.io.File
import java.io.FileInputStream
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.ServerSocketChannel
import java.nio.channels.SocketChannel
import kotlin.concurrent.thread


open class JavaNioFileServer(protected val socketChannelHandler: SocketChannelHandler) : IFileServer {

    protected var serverSocketChannel: ServerSocketChannel? = null

    protected var receiverThread: Thread? = null

    protected var threadPool = ThreadPool()

    override var fileForIdCallback: GetFileForIdCallback? = null


    override fun start(port: Int): Boolean {
        val listenAddr = InetSocketAddress(port)
        serverSocketChannel = ServerSocketChannel.open()

        serverSocketChannel?.let { listener ->
            val serverSocket = listener.socket()
            serverSocket.reuseAddress = true
            serverSocket.bind(listenAddr)

            startReceiverThread(listener)
        }

        return true
    }

    override fun stop(): Boolean {
        serverSocketChannel?.close()

        receiverThread?.join(200)

        serverSocketChannel = null
        receiverThread = null

        return true
    }


    protected open fun startReceiverThread(listener: ServerSocketChannel) {
        receiverThread = thread {
            while (listener.isOpen) {
                val clientSocket = listener.accept()

                sendFileToClientAsync(clientSocket)
            }
        }
    }

    protected open fun sendFileToClientAsync(socketChannel: SocketChannel) {
        threadPool.runAsync {
            sendFileToClient(socketChannel)

            socketChannel.close()
        }
    }

    protected open fun sendFileToClient(socketChannel: SocketChannel) {
        val requestFileSync = socketChannelHandler.receiveMessage(socketChannel, RequestSyncFile::class.java, 300)

        if (requestFileSync != null) {
            val file = fileForIdCallback?.getFileForId(requestFileSync.fileId)

            if (file != null) {
                socketChannelHandler.sendMessage(socketChannel, RequestSyncFileResponse(true, file.length()))

                val synchronizeBuffer = ByteBuffer.allocate(1)
                val waitForClientReady = socketChannel.read(synchronizeBuffer) // wait till client is ready to receive file, otherwise we may would send bytes but the client is not in
                // receiving state yet

                sendFileToClient(socketChannel, file)
            }
            else {
                socketChannelHandler.sendMessage(socketChannel, RequestSyncFileResponse(false, FileSizeUnknown))
            }
        }
    }

    protected open fun sendFileToClient(socketChannel: SocketChannel, file: File) {
        socketChannel.configureBlocking(true)

        FileInputStream(file).use { inputStream ->
            inputStream.channel.use { fileChannel ->
                fileChannel.transferTo(0, file.length(), socketChannel)
            }
        }
    }

}