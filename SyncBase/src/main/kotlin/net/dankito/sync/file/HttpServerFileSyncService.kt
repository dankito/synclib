package net.dankito.sync.file

import net.dankito.sync.Endpoint
import net.dankito.utils.web.client.OkHttpWebClient
import net.dankito.utils.web.client.RequestParameters
import net.dankito.utils.web.client.ResponseType
import org.slf4j.LoggerFactory
import java.io.DataInputStream
import java.io.File


open class HttpServerFileSyncService : IFileSyncService {

    companion object {
        const val FilesUrlStart = "files/"

        private val log = LoggerFactory.getLogger(HttpServerFileSyncService::class.java)
    }


    protected val client = OkHttpWebClient()


    override fun retrieveFile(endpoint: Endpoint, fileId: String, destinationFile: File) {

        val response = client.get(RequestParameters(
                "http:${endpoint.address}:${endpoint.fileSyncPort}/$FilesUrlStart$fileId",
                responseType = ResponseType.Stream))

        log.info("Retrieve file $fileId response: isSuccessful = ${response.isSuccessful}, error = ${response.error}," +
                " body = ${response.body}")

        if (response.isSuccessful) {
            try {
                response.responseStream?.let { input ->
                    DataInputStream(input.buffered()).use { bufferedInput ->
                        destinationFile.outputStream().buffered().use { fileOut ->
                            bufferedInput.copyTo(fileOut)
                        }
                    }
                }
            } catch (e: Exception) {
                log.error("Error while receiving response stream for file with id $fileId", e)
            }
        }
        else {
            log.error("Could not get file with id $fileId", response.error)
        }
    }

}