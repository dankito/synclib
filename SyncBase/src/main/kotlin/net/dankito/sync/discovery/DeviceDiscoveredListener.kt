package net.dankito.sync.discovery


interface DeviceDiscoveredListener {

    fun discoveredDevice(discoveredDevice: DiscoveredDevice)

}