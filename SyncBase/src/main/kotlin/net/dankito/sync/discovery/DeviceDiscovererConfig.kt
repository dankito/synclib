package net.dankito.sync.discovery


open class DeviceDiscovererConfig(
                                  /**
                                   * Only needed for mDNS bases discoverers, ignored in UdpDeviceDiscoverer
                                   */
                                  val serviceType: String,
                                  /**
                                   * Name of the service. In most times the app name
                                   */
                                  val serviceName: String,
                                  /**
                                   * The via which other devices can connect to this service.
                                   */
                                  val servicePortToPromote: Int,
                                  /**
                                   * A description of the service.
                                   * For UdpDeviceDiscoverer the device identifier.
                                   */
                                  val serviceDescription: String,
                                  /**
                                   * Port on which discoverer should run.
                                   * Not needed for mDNS bases discoverers
                                   */
                                  val discovererPort: Int = 0
) {

    override fun toString(): String {
        return "$servicePortToPromote: $serviceDescription"
    }

}