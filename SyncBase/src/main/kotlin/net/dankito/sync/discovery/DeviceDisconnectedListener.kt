package net.dankito.sync.discovery


interface DeviceDisconnectedListener {

    fun deviceDisconnected(discoveredDevice: DiscoveredDevice)

}