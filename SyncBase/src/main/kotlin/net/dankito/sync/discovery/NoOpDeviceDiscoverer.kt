package net.dankito.sync.discovery


class NoOpDeviceDiscoverer : IDeviceDiscoverer {

    override fun start(config: DeviceDiscovererConfig): Boolean {
        return false
    }

    override fun stop() {
    }


    override fun addDeviceDiscoveredListener(listener: DeviceDiscoveredListener) {
    }

    override fun removeDeviceDiscoveredListener(listener: DeviceDiscoveredListener) {
    }

    override fun addDeviceDisconnectedListener(listener: DeviceDisconnectedListener) {
    }

    override fun removeDeviceDisconnectedListener(listener: DeviceDisconnectedListener) {
    }

}