package net.dankito.sync.discovery

import kotlin.concurrent.thread


interface IDeviceDiscoverer {

    companion object {
        const val DefaultServiceType = "_sync._tcp.local."
    }


    fun startAsync(config: DeviceDiscovererConfig) {
        thread {
            start(config)
        }
    }

    fun start(config: DeviceDiscovererConfig): Boolean

    fun stop()


    fun addDeviceDiscoveredListener(listener: (DiscoveredDevice) -> Unit) {
        addDeviceDiscoveredListener(object : DeviceDiscoveredListener {

            override fun discoveredDevice(discoveredDevice: DiscoveredDevice) {
                listener(discoveredDevice)
            }

        })
    }

    fun addDeviceDiscoveredListener(listener: DeviceDiscoveredListener)

    fun removeDeviceDiscoveredListener(listener: DeviceDiscoveredListener)


    fun addDeviceDisconnectedListener(listener: (DiscoveredDevice) -> Unit) {
        addDeviceDisconnectedListener(object : DeviceDisconnectedListener {

            override fun deviceDisconnected(discoveredDevice: DiscoveredDevice) {
                listener(discoveredDevice)
            }

        })
    }

    fun addDeviceDisconnectedListener(listener: DeviceDisconnectedListener)

    fun removeDeviceDisconnectedListener(listener: DeviceDisconnectedListener)

}