package net.dankito.sync.discovery


abstract class DeviceDiscovererBase : IDeviceDiscoverer {

    protected val deviceDiscoveredListeners = mutableListOf<DeviceDiscoveredListener>()

    protected val deviceDisconnectedListeners = mutableListOf<DeviceDisconnectedListener>()


    override fun addDeviceDiscoveredListener(listener: DeviceDiscoveredListener) {
        deviceDiscoveredListeners.add(listener)
    }

    override fun removeDeviceDiscoveredListener(listener: DeviceDiscoveredListener) {
        deviceDiscoveredListeners.remove(listener)
    }

    protected open fun callDeviceDiscoveredListeners(discoveredDevice: DiscoveredDevice) {
        ArrayList(deviceDiscoveredListeners).forEach { listener ->
            listener.discoveredDevice(discoveredDevice)
        }
    }


    override fun addDeviceDisconnectedListener(listener: DeviceDisconnectedListener) {
        deviceDisconnectedListeners.add(listener)
    }

    override fun removeDeviceDisconnectedListener(listener: DeviceDisconnectedListener) {
        deviceDisconnectedListeners.remove(listener)
    }

    protected open fun callDeviceDisconnectedListeners(discoveredDevice: DiscoveredDevice) {
        ArrayList(deviceDisconnectedListeners).forEach { listener ->
            listener.deviceDisconnected(discoveredDevice)
        }
    }

}