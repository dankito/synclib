package net.dankito.sync.discovery


open class DiscoveredDevice(val address: String, val servicePort: Int, val serviceDescription: String, val synchronizationPort: Int = servicePort) {


    override fun toString(): String {
        return "$address:$servicePort"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is DiscoveredDevice) return false

        if (address != other.address) return false
        if (servicePort != other.servicePort) return false
        if (serviceDescription != other.serviceDescription) return false

        return true
    }


    override fun hashCode(): Int {
        var result = address.hashCode()
        result = 31 * result + servicePort
        result = 31 * result + serviceDescription.hashCode()
        return result
    }

}