package net.dankito.sync.android.test.di

import android.content.Context
import dagger.Module
import dagger.Provides
import net.dankito.jpa.entitymanager.IEntityManager
import net.dankito.sync.android.test.files.AndroidFolderShortcutsService
import net.dankito.sync.data.CouchbaseLiteEntityManagerAndroid
import net.dankito.sync.discovery.IDeviceDiscoverer
import net.dankito.sync.discovery.UdpDeviceDiscovererAndroid
import net.dankito.sync.security.ConscryptSecurityProviderAndroid
import net.dankito.sync.security.ISecurityProvider
import net.dankito.sync.test.files.IFolderShortcutsService
import net.dankito.utils.IThreadPool
import net.dankito.utils.android.io.AndroidFolderUtils
import net.dankito.utils.network.INetworkConnectivityManager
import net.dankito.utils.network.INetworkHelper
import net.dankito.utils.network.PeriodicCheckingNetworkConnectivityManager
import javax.inject.Singleton


@Module
class AndroidModule(private val applicationContext: Context) {

    @Provides
    @Singleton
    fun provideApplicationContext() : Context {
        return applicationContext
    }

    @Provides
    @Singleton
    fun provideEntityManager(applicationContext: Context) : IEntityManager {
        return CouchbaseLiteEntityManagerAndroid(applicationContext)
    }


    @Provides
    @Singleton
    fun provideNetworkConnectivityManager(networkHelper: INetworkHelper) : INetworkConnectivityManager {
        return PeriodicCheckingNetworkConnectivityManager(networkHelper) // TODO: use Android version
    }

    @Provides
    @Singleton
    fun provideDeviceDiscoverer(context: Context, connectivityManager: INetworkConnectivityManager,
                                threadPool: IThreadPool) : IDeviceDiscoverer {

//        return JmDnsDeviceDiscovererAndroid(this.applicationContext)
//        return NsdServiceDeviceDiscoverer(this.applicationContext)

        return UdpDeviceDiscovererAndroid(context, connectivityManager, threadPool)
    }


    @Provides
    @Singleton
    fun provideSecurityProvider() : ISecurityProvider {
        return ConscryptSecurityProviderAndroid()
    }


    @Provides
    @Singleton
    fun provideFolderUtils(context: Context) : AndroidFolderUtils {
        return AndroidFolderUtils(context)
    }

    @Provides
    @Singleton
    fun provideFolderShortcutsService(folderUtils: AndroidFolderUtils) : IFolderShortcutsService {
        return AndroidFolderShortcutsService(folderUtils)
    }

}