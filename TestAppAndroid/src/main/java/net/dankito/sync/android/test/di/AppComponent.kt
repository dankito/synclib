package net.dankito.sync.android.test.di

import dagger.Component
import net.dankito.sync.android.test.MainActivity
import net.dankito.sync.test.di.CommonComponent
import net.dankito.sync.test.di.CommonUtilsModule
import net.dankito.sync.test.di.DaoModule
import net.dankito.sync.test.di.NetworkModule
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AndroidModule::class,
    CommonUtilsModule::class, NetworkModule::class, DaoModule::class
])
interface AppComponent : CommonComponent {

    companion object {
        lateinit var component: AppComponent
    }


    fun inject(mainActivity: MainActivity)

}