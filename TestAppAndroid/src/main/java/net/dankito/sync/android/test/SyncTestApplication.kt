package net.dankito.sync.android.test

import android.support.multidex.MultiDexApplication
import net.dankito.sync.android.test.di.AndroidModule
import net.dankito.sync.android.test.di.AppComponent
import net.dankito.sync.android.test.di.DaggerAppComponent
import net.dankito.sync.test.di.CommonComponent


class SyncTestApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        setupDI()
    }


    private fun setupDI() {
        val component = DaggerAppComponent.builder()
                .androidModule(AndroidModule(this))
                .build()

        CommonComponent.component = component
        AppComponent.component = component
    }

}