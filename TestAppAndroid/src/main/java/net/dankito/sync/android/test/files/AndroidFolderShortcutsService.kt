package net.dankito.sync.android.test.files

import android.os.Build
import android.os.Environment
import net.dankito.sync.test.files.IFolderShortcutsService
import net.dankito.utils.android.io.AndroidFolderUtils
import net.dankito.utils.io.FileInfo


open class AndroidFolderShortcutsService(protected val folderUtils: AndroidFolderUtils) : IFolderShortcutsService {


    override fun getFolderShortcuts(): List<FileInfo> {
        val folderShortcuts = mutableListOf<FileInfo>()

        folderShortcuts.add(FileInfo.from(Environment.getExternalStorageDirectory(), "Internal Storage")) // TODO: translate

        folderUtils.findSdCardDirectory()?.let {
            folderShortcuts.add(FileInfo.from((it)))
        }

        folderShortcuts.add(fromPublicDir(Environment.DIRECTORY_DOWNLOADS))

        folderShortcuts.add(FileInfo.from(folderUtils.getCameraPhotosDirectory()))

        folderShortcuts.add(fromPublicDir(Environment.DIRECTORY_PICTURES))

        folderShortcuts.add(fromPublicDir(Environment.DIRECTORY_MUSIC))

        folderShortcuts.add(fromPublicDir(Environment.DIRECTORY_MOVIES))

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) { // Documents folder is only available on KitKat and // newer)
            val docs = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS)

            if (docs.exists()) {
                folderShortcuts.add(FileInfo.from(docs))
            }
        }


        return folderShortcuts
    }

    protected open fun fromPublicDir(environmentFolderString: String): FileInfo {
        return FileInfo.from(Environment.getExternalStoragePublicDirectory(environmentFolderString))
    }

}