package net.dankito.sync.android.test.files

import android.os.FileObserver
import net.dankito.sync.test.files.FileChange
import net.dankito.sync.test.files.FileChangeInfo
import net.dankito.sync.test.files.IFileSystemWatcher
import net.dankito.utils.io.FileInfo
import org.slf4j.LoggerFactory
import java.io.File
import java.util.concurrent.ConcurrentHashMap


open class FileSystemWatcherAndroid : IFileSystemWatcher {

    companion object {
        private val log = LoggerFactory.getLogger(FileSystemWatcherAndroid::class.java)
    }


    protected val watchers = ConcurrentHashMap<File, RecursiveFileObserver>()


    override fun watchFolderRecursively(folder: File, changeListener: (FileChangeInfo) -> Unit) {
        val watcher = object : RecursiveFileObserver(folder.absolutePath) {
            override fun onEvent(event: Int, path: String) {
                super.onEvent(event, path)

                if (path.endsWith("/null", true) == false) { // filter out paths that end on /null
                    fileChanged(folder, event, path, changeListener)
                }
            }
        }

        watchers.put(folder, watcher)

        watcher.startWatching()
    }

    protected open fun fileChanged(folder: File, event: Int, path: String, changeListener: (FileChangeInfo) -> Unit) {
        mapChange(event)?.let { change ->
            val file = File(path)
            changeListener(FileChangeInfo(FileInfo(file.name, file.absolutePath, file.isDirectory, file.length()),
                    file.toRelativeString(folder), change))
        }

        if (event == FileObserver.DELETE_SELF) {
            stopWatching(folder)
        }
    }

    protected open fun mapChange(event: Int): FileChange? {
        if (isBitSet(event, FileObserver.CREATE)) {
            return FileChange.Created
        }

        val modified = isBitSet(event, FileObserver.MODIFY)
        val moveSelf = isBitSet(event, FileObserver.MOVE_SELF)
        val movedTo = isBitSet(event, FileObserver.MOVED_TO) // avoid
        val movedFrom = isBitSet(event, FileObserver.MOVED_FROM)
        val attrib = isBitSet(event, FileObserver.ATTRIB)
        val closeWrite = isBitSet(event, FileObserver.CLOSE_WRITE)
        log.info("modified = $modified, moveSelf = $moveSelf, movedTo = $movedTo, movedFrom = $movedFrom, attrib = $attrib, closeWrite = $closeWrite")

        if (isBitSet(event, FileObserver.MODIFY) || isBitSet(event, FileObserver.ATTRIB) || /*isBitSet(event, FileObserver.CLOSE_WRITE) || */
                isBitSet(event, FileObserver.MOVE_SELF) || /*isBitSet(event, FileObserver.MOVED_TO) ||*/ isBitSet(event, FileObserver.MOVED_FROM)) {

            return FileChange.Modified
        }

        if (isBitSet(event, FileObserver.DELETE) || isBitSet(event, FileObserver.DELETE_SELF)) {
            return FileChange.Deleted
        }

        return null
    }

    private fun isBitSet(bitMask: Int, bit: Int): Boolean {
        return bitMask.and(bit) == bit
    }

    override fun stopWatching(folder: File) {
        watchers.remove(folder)?.stopWatching()
    }

    override fun stopAllWatchers() {
        ArrayList(watchers.keys).forEach { stopWatching(it) }
    }

}