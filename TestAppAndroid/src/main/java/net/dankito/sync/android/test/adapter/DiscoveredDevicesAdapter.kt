package net.dankito.sync.android.test.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.list_item_discovered_device.view.*
import net.dankito.sync.android.test.R
import net.dankito.sync.test.messenger.http.messages.DeviceInfo


class DiscoveredDevicesAdapter(private val downloadFilesListener: (DeviceInfo) -> Unit,
                               private val downloadFolderListener: (DeviceInfo) -> Unit,
                               private val testTransferSpeedListener: (DeviceInfo) -> Unit) : BaseAdapter() {

    private val discoveredDevices = mutableListOf<DeviceInfo>()


    override fun getCount(): Int {
        return discoveredDevices.size
    }

    override fun getItem(position: Int): Any {
        return discoveredDevices.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val device = getItem(position) as DeviceInfo

        val view = convertView ?: LayoutInflater.from(parent?.context).inflate(R.layout.list_item_discovered_device, parent, false)

        view.txtvwDeviceName.text = device.deviceName

        view.txtvwDeviceAddress.text = "${device.address} (Port ${device.messagesPort})"

        view.btnDownloadFiles.setOnClickListener { downloadFilesListener(device) }

        view.btnDownloadFolder.setOnClickListener { downloadFolderListener(device) }

        view.btnTransferSpeed.setOnClickListener { testTransferSpeedListener(device) }

        return view
    }


    fun deviceDiscovered(discoveredDevice: DeviceInfo) {
        discoveredDevices.add(discoveredDevice)

        notifyDataSetChanged()
    }

    fun deviceDisconnected(discoveredDevice: DeviceInfo) {
        discoveredDevices.remove(discoveredDevice)

        notifyDataSetChanged()
    }

}