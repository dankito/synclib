package net.dankito.sync.android.test.utils

import android.view.View.MeasureSpec
import android.widget.ListView


class ListViewUtils {

    /**
     * Thanks to Nirmal, https://stackoverflow.com/a/17693628
     */
    fun setListViewHeightBasedOnChildren(listView: ListView) {
        val listAdapter = listView.getAdapter()
                ?: // pre-condition
                return

        var totalHeight = 0
        val desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST)
        for (i in 0 until listAdapter.getCount()) {
            val listItem = listAdapter.getView(i, null, listView)
            listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED)
            totalHeight += listItem.getMeasuredHeight()
        }

        val params = listView.getLayoutParams()
        params.height = totalHeight + listView.getDividerHeight() * (listAdapter.getCount() - 1)
        listView.setLayoutParams(params)
        listView.requestLayout()
    }

}