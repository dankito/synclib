package net.dankito.sync.android.test.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.list_item_log_item.view.*
import net.dankito.sync.android.test.R
import net.dankito.sync.test.ui.model.LogItem
import net.dankito.sync.test.ui.presenter.MainWindowPresenter


class ActivityLogAdapter : BaseAdapter() {

    private val logItems = mutableListOf<LogItem>()


    override fun getCount(): Int {
        return logItems.size
    }

    override fun getItem(position: Int): Any {
        return logItems.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val logItem = getItem(position) as LogItem

        val view = convertView ?: LayoutInflater.from(parent?.context).inflate(R.layout.list_item_log_item, parent, false)

        view.txtvwTimestamp.text = MainWindowPresenter.LogItemTimestampFormat.format(logItem.timestamp)

        view.txtvwLogMessage.text = logItem.logMessage

        return view
    }


    fun addLogMessage(message: LogItem) {
        logItems.add(message)

        notifyDataSetChanged()
    }

}