package net.dankito.sync.android.test

import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import net.dankito.filechooserdialog.FileChooserDialog
import net.dankito.filechooserdialog.model.FileChooserDialogConfig
import net.dankito.jpa.entitymanager.IEntityManager
import net.dankito.sync.android.test.adapter.ActivityLogAdapter
import net.dankito.sync.android.test.adapter.DiscoveredDevicesAdapter
import net.dankito.sync.android.test.clipboard.ClipboardWatcherAndroid
import net.dankito.sync.android.test.di.AppComponent
import net.dankito.sync.android.test.utils.ListViewUtils
import net.dankito.sync.android.test.utils.RemoteFilesystemDirectoryContentRetriever
import net.dankito.sync.discovery.IDeviceDiscoverer
import net.dankito.sync.security.ISecurityProvider
import net.dankito.sync.test.files.IFolderShortcutsService
import net.dankito.sync.test.messenger.http.messages.DeviceInfo
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentMessage
import net.dankito.sync.test.ui.model.LogItem
import net.dankito.sync.test.ui.presenter.MainWindowPresenter
import net.dankito.utils.IThreadPool
import net.dankito.utils.android.permissions.PermissionsService
import net.dankito.utils.events.IEventBus
import net.dankito.utils.extensions.toFileInfo
import net.dankito.utils.io.FileInfo
import org.slf4j.LoggerFactory
import java.io.File
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    companion object {
        private val log = LoggerFactory.getLogger(MainActivity::class.java)
    }


    @Inject
    lateinit var entityManager: IEntityManager

    @Inject
    lateinit var deviceDiscoverer: IDeviceDiscoverer

    @Inject
    lateinit var securityProvider: ISecurityProvider

    @Inject
    lateinit var folderShortcutsService: IFolderShortcutsService

    @Inject
    lateinit var eventBus: IEventBus

    @Inject
    lateinit var threadPool: IThreadPool


    private val permissionsService = PermissionsService(this)

    private val presenter: MainWindowPresenter

    private val discoveredDevicesAdapter = DiscoveredDevicesAdapter( { downloadFiles(it) }, { downloadFolder(it) },
            { doTransferSpeedTest(it) } )

    private val activityLogAdapter = ActivityLogAdapter()


    init {
        AppComponent.component.inject(this)

        presenter = MainWindowPresenter(eventBus, threadPool) { addLogMessage(it) }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupUi()

        setupLogic()
    }

    private fun setupUi() {
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        lstvwDiscoveredDevices.adapter = discoveredDevicesAdapter

        lstvwActivityLog.adapter = activityLogAdapter

        val listViewUtils = ListViewUtils()
        listViewUtils.setListViewHeightBasedOnChildren(lstvwDiscoveredDevices)
        listViewUtils.setListViewHeightBasedOnChildren(lstvwActivityLog)
    }

    private fun setupLogic() {
        presenter.start(Build.MODEL, entityManager, deviceDiscoverer, securityProvider,
                folderShortcutsService)

        presenter.syncTestClient.addDeviceDiscoveredListener { handleDeviceDiscovered(it) }
        presenter.syncTestClient.addDeviceDisconnectedListener { handleDeviceDisconnected(it) }

        // TODO: use implementation from AndroidUtils
        ClipboardWatcherAndroid(this.applicationContext).addClipboardContentChangedListener {
            log.info("Clipboard content changed to $it")
            entityManager.persistEntity(it)
        }
    }

    override fun onDestroy() {
        presenter.destroy()

        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        permissionsService.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    private fun downloadFolder(device: DeviceInfo) {
        val config = createRemoteFileChooserDialogConfig(device)

        FileChooserDialog().showSelectFolderFullscreenDialog(this, permissionsService, config) { _, remoteFolder ->

            remoteFolder?.let {
                FileChooserDialog().showSelectFolderFullscreenDialog(this, permissionsService,
                        FileChooserDialogConfig()) { _, destinationFolder ->

                    destinationFolder?.let {

                        presenter.downloadFolderAsync(device, remoteFolder, destinationFolder)
                    }
                }
            }
        }
    }

    private fun downloadFiles(device: DeviceInfo) {
        val config = createRemoteFileChooserDialogConfig(device)

        FileChooserDialog().showOpenMultipleFilesFullscreenDialog(this, permissionsService, config) { _, files ->

            files?.let {
                FileChooserDialog().showSelectFolderFullscreenDialog(this, permissionsService,
                        FileChooserDialogConfig(initialDirectory = Environment.getExternalStoragePublicDirectory
                        (Environment.DIRECTORY_DOWNLOADS))) { _, destinationFolder ->

                    destinationFolder?.let {
                        val fileInfos: List<FileInfo> = files.map { it.toFileInfo() }

                        presenter.downloadSelectedFilesAsync(device, destinationFolder, fileInfos) // get off UI thread
                    }
                }
            }
        }
    }

    private fun createRemoteFileChooserDialogConfig(device: DeviceInfo): FileChooserDialogConfig {
        return FileChooserDialogConfig(initialDirectory = File(GetDirectoryContentMessage.RootFolder),
                directoryContentRetriever = RemoteFilesystemDirectoryContentRetriever
                (this, presenter.syncTestClient.messenger, presenter.syncTestClient.messageBuilder,
                        device.endpoint, threadPool),
                showFolderShortcutsView = false)
    }

    private fun doTransferSpeedTest(device: DeviceInfo) {
        presenter.doTransferSpeedTest(device)
    }


    private fun handleDeviceDiscovered(discoveredDevice: DeviceInfo) {
        runOnUiThread {
            discoveredDevicesAdapter.deviceDiscovered(discoveredDevice)

            addLogMessageOnUiThread(LogItem("Discovered device $discoveredDevice"))
        }
    }

    private fun handleDeviceDisconnected(discoveredDevice: DeviceInfo) {
        runOnUiThread {
            discoveredDevicesAdapter.deviceDisconnected(discoveredDevice)

            addLogMessageOnUiThread(LogItem("Device disconnected $discoveredDevice"))
        }
    }

    private fun addLogMessage(message: LogItem) {
        runOnUiThread { addLogMessageOnUiThread(message) }
    }

    private fun addLogMessageOnUiThread(message: LogItem) {
        log.info(message.logMessage)

        activityLogAdapter.addLogMessage(message)
    }

}
