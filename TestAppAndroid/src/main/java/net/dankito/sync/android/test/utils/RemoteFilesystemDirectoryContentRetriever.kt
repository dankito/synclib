package net.dankito.sync.android.test.utils

import android.app.Activity
import net.dankito.filechooserdialog.service.IDirectoryContentRetriever
import net.dankito.sync.Endpoint
import net.dankito.sync.messenger.IMessenger
import net.dankito.sync.test.messenger.http.HttpMessageBuilder
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentMessage
import net.dankito.sync.test.messenger.http.messages.GetDirectoryContentResponse
import net.dankito.utils.IThreadPool
import net.dankito.utils.io.FileFileInfoWrapper
import net.dankito.utils.io.ListDirectory
import org.slf4j.LoggerFactory
import java.io.File


open class RemoteFilesystemDirectoryContentRetriever(protected val activity: Activity,
                                                     protected val messenger: IMessenger,
                                                     protected val messageBuilder: HttpMessageBuilder,
                                                     protected val endpoint: Endpoint,
                                                     protected val threadPool: IThreadPool)
    : IDirectoryContentRetriever {

    companion object {
        private val log = LoggerFactory.getLogger(RemoteFilesystemDirectoryContentRetriever::class.java)
    }


    override fun getFilesOfDirectorySorted(directory: File, listDirectory: ListDirectory, folderDepth: Int,
                                           extensionsFilters: List<String>, callback: (List<File>?) -> Unit) {

        threadPool.runAsync { // this method gets called on UI thread, so get off UI thread for network call
            val adjustedDirectory =
                    if (directory.absolutePath == "/" + GetDirectoryContentMessage.RootFolder)
                        GetDirectoryContentMessage.RootFolder
                    else
                        directory.absolutePath

            val response = messenger.sendMessage(endpoint,
                    messageBuilder.getContentDirectoryMessage(adjustedDirectory, listDirectory),
                    GetDirectoryContentResponse::class.java)

            log.info("Response: error = ${response.error}, files = ${response.body?.files}")

            response.body?.files?.let { files ->
                activity.runOnUiThread {
                    callback(files.map { FileFileInfoWrapper(it) })
                }
            }

            // TODO: show error

        }
    }

}