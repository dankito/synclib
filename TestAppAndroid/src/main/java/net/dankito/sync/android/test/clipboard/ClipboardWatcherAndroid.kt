package net.dankito.sync.android.test.clipboard

import android.content.ClipboardManager
import android.content.Context
import net.dankito.sync.test.clipboard.ClipboardContent
import net.dankito.sync.test.clipboard.ClipboardWatcherBase


open class ClipboardWatcherAndroid(protected val clipboardManager: ClipboardManager) : ClipboardWatcherBase() {

    constructor(context: Context) : this(context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager)


    protected val clipChangedListener = object : ClipboardManager.OnPrimaryClipChangedListener {

        override fun onPrimaryClipChanged() {
            if (clipboardManager.hasPrimaryClip() && clipboardManager.primaryClip.itemCount > 0) {
                val clipItem = clipboardManager.primaryClip.getItemAt(0)

                clipboardContentChanged(ClipboardContent(clipItem.text as? String ?: "", clipItem.htmlText ?: ""))
            }
        }

    }


    override fun startListeningToClipboard() {
        clipboardManager.addPrimaryClipChangedListener(clipChangedListener)
    }

    override fun stopListeningToClipboard() {
        clipboardManager.removePrimaryClipChangedListener(clipChangedListener)
    }

}