package net.dankito.sync.android.test.files

import android.support.test.runner.AndroidJUnit4
import net.dankito.sync.test.files.FileChange
import net.dankito.sync.test.files.FileChangeInfo
import net.dankito.utils.io.FileUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import java.io.File


@RunWith(AndroidJUnit4::class)
class FileSystemWatcherAndroidTest {

    companion object {
        private val log = LoggerFactory.getLogger(FileSystemWatcherAndroid::class.java)
    }


    private val fileUtils = FileUtils()

    private lateinit var testFolder: File

    private val underTest = FileSystemWatcherAndroid()


    @Before
    @Throws(Exception::class)
    fun setUp() {
        val tempFile = File.createTempFile("JustWantToKnowTempDir", ".tmp")

        testFolder = File(tempFile.parent, "FileSystemWatcherTest")
        testFolder.mkdirs()

        tempFile.delete()
    }

    @After
    fun tearDown() {
        underTest.stopAllWatchers()

        fileUtils.deleteFolderRecursively(testFolder)
    }


    @Test
    fun createFile() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFile = createTestFile()

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos.get(0).relativePath).isEqualTo(testFile.name)
        assertThat(raisedFileChangeInfos.get(0).change).isEqualTo(FileChange.Created)
    }

    @Test
    fun modifyFile() {
        // given
        val testFile = createTestFile()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        modifyFile(testFile)

        // then
        assertThat(raisedFileChangeInfos.size).isIn(1, 2) // on Android Modified is fired twice
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFile.name)
        assertThat(raisedFileChangeInfos).extracting("change").containsOnly(FileChange.Modified)
    }

    @Test
    fun createAndModifyFile() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFile = createTestFile()
        modifyFile(testFile)

        // then
        assertThat(raisedFileChangeInfos.size).isIn(2, 3) // on Android Modified is fired twice
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFile.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsOnly(FileChange.Created, FileChange.Modified)
    }

    @Test
    fun deleteFile() {
        // given
        val testFile = createTestFile()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        deleteFile(testFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos.get(0).relativePath).isEqualTo(testFile.name)
        assertThat(raisedFileChangeInfos.get(0).change).isEqualTo(FileChange.Deleted)
    }

    @Test
    fun createAndDeleteFile() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFile = createTestFile()
        deleteFile(testFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFile.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted)
    }

    @Test
    fun modifyAndDeleteFile() {
        // given
        val testFile = createTestFile()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        modifyFile(testFile)
        deleteFile(testFile)

        // then
        assertThat(raisedFileChangeInfos.size).isIn(2, 3) // on Android Modified is fired twice
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFile.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsOnly(FileChange.Modified, FileChange.Deleted)
    }

    @Test
    fun createModifyAndDeleteFile() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFile = createTestFile()
        modifyFile(testFile)
        deleteFile(testFile)

        // then
        assertThat(raisedFileChangeInfos.size).isIn(3, 4) // on Android Modified is fired twice
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFile.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsOnly(FileChange.Created, FileChange.Modified, FileChange.Deleted)
    }


    @Test
    fun createFolder() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFolder = createTestFolder()

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsExactly(testFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Created)
    }

    @Test
    fun renameFolder() {
        // given
        val testFolder = createTestFolder()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val renamedFolder = rename(testFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsExactly(testFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Modified)
    }

    @Test
    fun createAndRenameFolder() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFolder = createTestFolder()
        val renamedFolder = rename(testFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Modified)
    }

    @Test
    fun deleteFolder() {
        // given
        val testFolder = createTestFolder()
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        deleteFile(testFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change").containsOnly(FileChange.Deleted)
    }

    @Test
    fun createAndDeleteFolder() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFolder = createTestFolder()
        deleteFile(testFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted)
    }

    @Test
    fun createRenameAndDeleteFolder() {
        // given
        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val testFolder = createTestFolder()
        val renamedFolder = rename(testFolder)
        deleteFile(renamedFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(3)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(testFolder.name, renamedFolder.name)
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Modified, FileChange.Deleted)
    }


    @Test
    fun createSubFolder() {
        // given
        val testFolder = createTestFolder()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolder = createTestFolder(testFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsExactly(subPath(subFolder))
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Created)
    }

    @Test
    fun renameSubFolder() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val renamedSubFolder = rename(subFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsExactly(subPath(subFolder))
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Modified)
    }

    @Test
    fun createAndRenameSubFolder() {
        // given
        val testFolder = createTestFolder()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolder = createTestFolder(testFolder)
        val renamedSubFolder = rename(subFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsOnly(subPath(subFolder))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Modified)
    }

    @Test
    fun deleteSubFolder() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        deleteFile(subFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(subPath(subFolder))
        assertThat(raisedFileChangeInfos).extracting("change").containsOnly(FileChange.Deleted)
    }

    @Test
    fun createAndDeleteSubFolder() {
        // given
        val testFolder = createTestFolder()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolder = createTestFolder(testFolder)
        deleteFile(subFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(subPath(subFolder))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted)
    }

    @Test
    fun createRenameAndDeleteSubFolder() {
        // given
        val testFolder = createTestFolder()

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolder = createTestFolder(testFolder)
        val renamedSubFolder = rename(subFolder)
        deleteFile(renamedSubFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(3)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsOnly(subPath(subFolder), subPath(renamedSubFolder))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Modified, FileChange.Deleted)
    }


    @Test
    fun createSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolderFile = createTestFile(subFolder)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsExactly(subPath(subFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Created)
    }

    @Test
    fun renameSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)
        val subFolderFile = createTestFile(subFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val renamedSubFolderFile = rename(subFolderFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsExactly(subPath(subFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change").containsExactly(FileChange.Modified)
    }

    @Test
    fun createAndRenameSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolderFile = createTestFile(subFolder)
        val renamedSubFolderFile = rename(subFolderFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsOnly(subPath(subFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Modified)
    }

    @Test
    fun deleteSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)
        val subFolderFile = createTestFile(subFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        deleteFile(subFolderFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(1)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(subPath(subFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change").containsOnly(FileChange.Deleted)
    }

    @Test
    fun createAndDeleteSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolderFile = createTestFile(subFolder)
        deleteFile(subFolderFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(2)
        assertThat(raisedFileChangeInfos).extracting("relativePath").containsOnly(subPath(subFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Deleted)
    }

    @Test
    fun createRenameAndDeleteSubFolderFile() {
        // given
        val testFolder = createTestFolder()
        val subFolder = createTestFolder(testFolder)

        val raisedFileChangeInfos = detectFileSystemChanges()

        // when
        val subFolderFile = createTestFile(subFolder)
        val renamedSubFolderFile = rename(subFolderFile)
        deleteFile(renamedSubFolderFile)

        // then
        assertThat(raisedFileChangeInfos).hasSize(3)
        assertThat(raisedFileChangeInfos).extracting("relativePath")
                .containsOnly(subPath(subFolderFile), subPath(renamedSubFolderFile))
        assertThat(raisedFileChangeInfos).extracting("change")
                .containsExactly(FileChange.Created, FileChange.Modified, FileChange.Deleted)
    }


    protected open fun detectFileSystemChanges(): MutableList<FileChangeInfo> {
        val raisedFileChangeInfos = mutableListOf<FileChangeInfo>()

        underTest.watchFolderRecursively(testFolder) { fileChangeInfo ->
            raisedFileChangeInfos.add(fileChangeInfo)
        }

        return raisedFileChangeInfos
    }

    protected open fun createTestFile(parentFolder: File? = null, fileName: String = "test.txt"): File {
        val parent = parentFolder ?: testFolder

        val testFile = File(parent, fileName)

        testFile.createNewFile()

        giveFileSystemWatcherTimeToDetectChanges("Created file $testFile")

        return testFile
    }

    protected open fun createTestFolder(parentFolder: File? = null, folderName: String = "folder"): File {
        val parent = parentFolder ?: testFolder

        val folder = File(parent, folderName)

        val successful = folder.mkdirs()

        giveFileSystemWatcherTimeToDetectChanges("Created folder $folder. Successful = $successful")

        return folder
    }

    protected open fun rename(fileOrFolder: File, newName: String = "NewName"): File {
        val destination = File(fileOrFolder.parent, newName)

        fileOrFolder.renameTo(destination)

        giveFileSystemWatcherTimeToDetectChanges("Renamed $fileOrFolder to $destination")

        return destination
    }

    protected open fun modifyFile(file: File, newFileContent: String = "test2") {
        file.writeText(newFileContent)

        giveFileSystemWatcherTimeToDetectChanges("Wrote text to file $file")
    }

    protected open fun deleteFile(file: File) {
        file.delete()

        giveFileSystemWatcherTimeToDetectChanges("Deleted file $file")
    }

    protected open fun giveFileSystemWatcherTimeToDetectChanges(logMessage: String? = null) {
        logMessage?.let {
            log.info(logMessage)
        }

        Thread.sleep(1250)
    }

    protected open fun subPath(fileOrFolder: File): String {
        return fileOrFolder.toRelativeString(this.testFolder)
    }


}